package cerere.datareader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import cerere.datareader.utils.JSONUtils;
import javafx.util.Pair;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileFilter;

public class Application extends JPanel {

  private static final String EXCEL_EXTENSION = ".xlsx";
  private static final String SHEET_NAME = "reports";
  private static final Integer KEYROW_NUM = 0;
  private JTextArea textFile;

  public Application() {
    super(new BorderLayout());
    createPanel();
  }

  private void createPanel() {
    JButton openFileButton = new JButton("Seleziona Data");
    JButton elabButton = new JButton("Elabora");
    
    textFile = new JTextArea(5, 40);
    textFile.setEnabled(false);
    
    add(new JScrollPane(textFile), BorderLayout.CENTER);
    JPanel panelButton = new JPanel();
    panelButton.add(openFileButton);
    panelButton.add(elabButton);
    add(panelButton, BorderLayout.SOUTH);
    
    openFileButton.addActionListener(new OpenFileListener());
    elabButton.addActionListener(new ElaborateListener());
  }

  private class OpenFileListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
      try {
        textFile.setText("");
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new TxtFileFilter());
        int n = fileChooser.showOpenDialog(Application.this);
        if (n == JFileChooser.APPROVE_OPTION) {
          File f = fileChooser.getSelectedFile();
          BufferedReader read = new BufferedReader(new FileReader(f));
          String line = read.readLine();
          while(line != null) {
            textFile.append(line + System.lineSeparator());
            line = read.readLine();
          }
          read.close();
        }
      } catch (Exception ex) {
    	 JOptionPane.showMessageDialog(getParent(), ex.getStackTrace().toString(), "ERRORE", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  private class ElaborateListener implements ActionListener {

	  private File file;
	  
    public void actionPerformed(ActionEvent e) {
      try {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new XlsxFileFilter());
        int n = fileChooser.showSaveDialog(Application.this);
        if (n == JFileChooser.APPROVE_OPTION) {
          file = fileChooser.getSelectedFile();
          
          checkExtention();
          
          elaborateData(JSONUtils.readString(textFile.getText()));
          
          JOptionPane.showMessageDialog(getParent(),"Dati esportati correttamente nel file " + file.getName());
        }
      } catch (Exception ex) {
    	  JOptionPane.showMessageDialog(getParent(), ex.getStackTrace().toString(), "ERRORE", JOptionPane.ERROR_MESSAGE);
      }
    }
    
    private void checkExtention() {
        if(!file.getName().endsWith(EXCEL_EXTENSION)) {
      	  String path = file.getAbsolutePath();
      	  if(path.contains(".")) {
      		  path.substring(0, path.lastIndexOf("."));
      	  }
      	  path = path.concat(EXCEL_EXTENSION);
      	  file = new File(path);
        }
    }
    
    private void elaborateData(Map<Integer, Map<String, String>> map) throws InvalidFormatException, IOException {
    	
	        XSSFWorkbook workbook; 
	        XSSFSheet sheet; 
	        int startingrow = KEYROW_NUM;
	        Row keyrow;
	        
	        if(file.exists()) {
	        	workbook = new XSSFWorkbook(new FileInputStream(file));
	        	sheet = workbook.getSheet(SHEET_NAME);
	        	keyrow = sheet.getRow(startingrow);
	        	startingrow = sheet.getLastRowNum() + 1;
	        	
	        } else {
		        // Create a blank workbook and sheet 
	        	workbook = new XSSFWorkbook();
	        	sheet = workbook.createSheet(SHEET_NAME); 
		        int cellnum = 0;
		        
		        List<String> keyset = new ArrayList<>(map.get(startingrow).keySet());
		        Collections.sort(keyset);
		        
		        keyrow = sheet.createRow(startingrow++);
		        
		        for (String key : keyset) {
	                Cell cell = keyrow.createCell(cellnum++); 
	                cell.setCellValue(key);
		        }
	        }       
	        
	        
	        for(int rownum = 0; rownum < map.size(); rownum++) {
	        	Iterator<Cell> iterator = keyrow.cellIterator();
	        	Row row = sheet.createRow(startingrow + rownum);
	        	Map<String, String> obj = map.get(rownum);
	        	
	        	while(iterator.hasNext()) {
	        		Cell keycell = iterator.next();
	        		if(obj.containsKey(keycell.getStringCellValue())) {
	        			Cell cell = row.createCell(keycell.getColumnIndex());
	        			cell.setCellValue(obj.get(keycell.getStringCellValue()));
	        		}
	        	}
	        }
	        
	        try { 
	            // this Writes the workbook
	            FileOutputStream out = new FileOutputStream(file); 
	            workbook.write(out); 
	            out.close(); 
	        	workbook.close();
	        } 
	        catch (Exception e) { 
	            e.printStackTrace(); 
	        }
    }
  }

  private class TxtFileFilter extends FileFilter {

    public boolean accept(File file) {
      if (file.isDirectory()) return true;
        String fname = file.getName().toLowerCase();
        return fname.endsWith("txt");
    }

    public String getDescription() {
      return "File di testo";
    }
  }
  
  private class XlsxFileFilter extends FileFilter {

	    public boolean accept(File file) {
	      if (file.isDirectory()) return true;
	        String fname = file.getName().toLowerCase();
	        return fname.endsWith(EXCEL_EXTENSION);
	    }

	    public String getDescription() {
	      return "File Excel";
	    }
	  }

  public static void main(String[] argv) {
    JFrame frame = new JFrame("Cerere Data Reader");
    Application demo = new Application();
    frame.getContentPane().add(demo);
    frame.pack();
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
}