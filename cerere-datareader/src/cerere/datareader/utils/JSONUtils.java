package cerere.datareader.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONUtils {
	
	public static Map<Integer, Map<String, String>> readString(String jsonString) {
		Map<Integer, Map<String, String>> result = new HashMap<>();
		try {
			JSONArray array = new JSONArray(jsonString);
			Map<String, String> keymap = new HashMap<>();
			for(int i = 1; i < array.length(); i++) {
				JSONObject obj = array.getJSONObject(i);
				Map<String, String> formatted_Obj = recursiveKeySearch("", "", obj);
				result.put(i, formatted_Obj);
				for(String key: formatted_Obj.keySet()) {
					if(!keymap.containsKey(key)) {
						keymap.put(key, "");
					}
				}
			}
			result.put(0, keymap);
		} catch (Exception e) {
			
		}
		return result;
	}
	
	private static Map<String, String> recursiveKeySearch(String relative_path, String absolute_path, Object object) {
		Map<String, String> result = new HashMap<>();
		
		try {
			if(object instanceof JSONObject) {
				JSONObject json_obj = (JSONObject) object;
				Iterator iterator = json_obj.keys();
				while(iterator.hasNext()) {
					String partial_key = (String) iterator.next();
					String complete_key = absolute_path.concat("." + partial_key);
					Object temp = json_obj.get(partial_key);
					result.putAll(recursiveKeySearch(partial_key, complete_key, temp));
				}
			} else if (object instanceof JSONArray) {
				JSONArray array = (JSONArray) object;
				for(int i = 0; i < array.length(); i++) {
					Object temp = array.get(i);
					String partial_key = relative_path + i;
					String complete_key = absolute_path.concat("." + partial_key);			
					result.putAll(recursiveKeySearch(partial_key, complete_key, temp));
				}
			} else {
				result.put(absolute_path, String.valueOf(object));
			}
		
		} catch (Exception e) {
			
		}
		return result;
	}
}


