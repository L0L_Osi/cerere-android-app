package com.app.cerere.cerereapp.api;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.json.JSONException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class DataSenderTest {

    Context context;
    String idReport;

    @BeforeClass
    public void setUp() {
        idReport = "5c2e171fcd3f9014c59565b5";
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    /**
     * Check on the server if the data has been received.
     * @throws JSONException
     */
    @Test
    public void testUploaderStatic() throws JSONException {
//        JSONObject json = new JSONObject();
//        json.put("operator", "A");
//        BDataSender.startService(context, idReport, json);
    }

    @Test
    public void testUploaderIntent() throws JSONException {
//        JSONObject json = new JSONObject();
//        json.put("operator", "A");
//
//        Intent i = new Intent(context, BDataSender.class);
//        i.putExtra(C.REPORT_ID, idReport);
//        i.putExtra(C.DATA_REQUEST, json.toString());
//        context.startService(i);
    }

}
