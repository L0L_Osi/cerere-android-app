package com.app.cerere.cerereapp.data;

import android.support.test.runner.AndroidJUnit4;

import com.app.cerere.cerereapp.domain.model.event.Labor;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class LaborTest {

    @Test
    public void testParsing() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("tipology", "spontaneo");
        Labor labor = new Gson().fromJson(json.toString(), Labor.class);
        assertEquals(Labor.Type.SPONTANEO, labor.getType());
    }

}
