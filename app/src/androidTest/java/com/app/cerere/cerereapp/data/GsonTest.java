package com.app.cerere.cerereapp.data;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.service.RemoteDataProvider;
import com.app.cerere.cerereapp.domain.model.Report;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith (AndroidJUnit4.class)
public class GsonTest {

    Context context;
    String reportId;

    @Before
    public void setUp() {
        reportId = "5c2e171fcd3f9014c59565b5";
        context = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void jsonArrayToString() {
        RemoteDataProvider.getRemoteActiveReportsList(context, success -> {
            LocalDataCacher.saveActiveReportList(context, success);
            List<Report> list = LocalDataCacher.getActiveReportList(context);
            assertEquals(list.size(), 2);
            assertEquals(list.get(0).getOperator(), "A");
        }, error -> {});
    }
}
