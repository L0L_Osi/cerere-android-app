package com.app.cerere.cerereapp;

import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.types.Drugs;

import org.junit.Test;

public class DrugsPrintTest {

    @Test
    public void testPrint() {
        final Anesthetic a = new Anesthetic();
        a.setDrug(Drugs.BUPIVACAINA);
        a.setMg(4);
        a.setMl(4);

        final Anesthetic aa = new Anesthetic();
        aa.setDrug(Drugs.ROPIVACAINA);
        aa.setPercent(5);
        aa.setMl(7);

        System.out.println(a.toString());
        System.out.println(aa.toString());
    }
}
