package com.app.cerere.cerereapp;

import com.app.cerere.cerereapp.domain.util.BmiCalculator;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class BmiTest {

    @Test
    public void testInteger() {
        double w = 65.0f;
        int h = 170;

        assertEquals(22.49d, BmiCalculator.getBmiByCm(w, h));
        assertEquals(22.49d, BmiCalculator.getBmiByM(w, 1.70));
    }
}
