package com.app.cerere.cerereapp;

import com.app.cerere.cerereapp.domain.model.event.Event;
import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.domain.model.event.Patient;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EventStatusTest {

    @Test
    public void isEmptyTest() {
        Patient p = new Patient();
        assertEquals(Event.Status.EMPTY, p.isComplete());
    }

    @Test
    public void isPartialTest() {
        Patient p = new Patient();
        p.setName("abc");
        assertEquals(Event.Status.PARTIAL, p.isComplete());
    }

    @Test
    public void isPartialStringsTest() {
        InfoReport i = new InfoReport();
        assertEquals(Event.Status.PARTIAL, i.isComplete());
    }

}
