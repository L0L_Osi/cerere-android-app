package com.app.cerere.cerereapp;

import com.app.cerere.cerereapp.domain.model.event.Patient;
import com.app.cerere.cerereapp.domain.types.Genders;
import com.app.cerere.cerereapp.domain.util.EnumUtils;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class EnumUtilsTest {

    @Test
    public void testEnumToStrings() {
        List<String> list = EnumUtils.enumToStrings(Patient.BirthsHistory.class);
        System.out.println(list);
        System.out.println(list.get(0));
    }

    @Test
    public void testStringToEnum() {
        Patient.BirthsHistory b = EnumUtils.stringToEnum(Patient.BirthsHistory.class,
                                    Patient.BirthsHistory.PLURIPARA.getPrintName());
        assertEquals(Patient.BirthsHistory.PLURIPARA, b);
    }

    @Test
    public void testEnumList() {
        List<Genders> genders = EnumUtils.getEnumAsList(Genders.class);
        assertEquals(genders.get(0), Genders.MALE);
        assertEquals(genders.get(1), Genders.FEMALE);
    }
}
