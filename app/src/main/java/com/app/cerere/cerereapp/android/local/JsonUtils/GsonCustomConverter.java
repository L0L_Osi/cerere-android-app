package com.app.cerere.cerereapp.android.local.JsonUtils;

import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;

public class GsonCustomConverter {

    private Gson gson;

    public GsonCustomConverter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(new LinkedHashMap<Anesthetic.DrugDetails, Double>().getClass() , new CustomAnestheticDetailsSerializer());
        builder.registerTypeAdapter(new LinkedHashMap<Anesthetic.DrugDetails, Double>().getClass(), new CustomAnestheticDetailsDeserializer());
        builder.registerTypeAdapter(InfoReport.class, new CustomInfoReportSerializer());
        builder.registerTypeAdapter(InfoReport.class, new CustomInfoReportDeserializer());
        builder.registerTypeAdapter(Report.Status.class, new JsonSerializer<Report.Status>() {
            @Override
            public JsonElement serialize(Report.Status src, Type typeOfSrc, JsonSerializationContext context) {
                return new JsonPrimitive(src.getName());
            }
        });
        builder.registerTypeAdapter(Report.Status.class, new JsonDeserializer<Report.Status>() {
            @Override
            public Report.Status deserialize(JsonElement src, Type typeOfSrc, JsonDeserializationContext context) {
                return Report.Status.valueOf(src.getAsString().toUpperCase());
            }
        });
        gson = builder.create();
    }

    public Gson getCustomGson() {
        return this.gson;
    }
}
