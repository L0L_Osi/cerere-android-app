package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class Secondamento extends AbstractEvent {

    public enum Type {

        @SerializedName("spontaneo")
        SPONTANEO("spontaneo"),

        @SerializedName("manuale")
        MANUALE("manuale"),

        @SerializedName("RCU")
        RCU("RCU");

        @Getter private final String printName;

        Type(final String name) {
            this.printName = name;
        }
    }

    public static final String NAME = "secondamento";

    @Getter @Setter private Type type;

    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }
}