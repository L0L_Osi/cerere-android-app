package com.app.cerere.cerereapp.android;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.service.RemoteDataProvider;
import com.app.cerere.cerereapp.domain.model.User;
import com.app.cerere.cerereapp.android.local.JsonUtils.JsonUtils;

import org.json.JSONObject;

public class SessionManager {

    private User currentUser;
    private final Context ctx;
    private static final SessionManager INSTANCE = new SessionManager();

    private SessionManager() {
        this.ctx = App.get().getApplicationContext();
        this.currentUser = LocalDataCacher.get().getCurrentUser(ctx);
    }

    public static SessionManager get() {
        return INSTANCE;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void logIn(Context context, User user, Runnable success, Runnable error) {
        final JSONObject json = JsonUtils.objToJSON(user);
        RemoteDataProvider.remoteLogin(context, json, resultOk -> {
            this.currentUser = JsonUtils.jsonStringToObj(resultOk.toString(), User.class);
            LocalDataCacher.get().saveUser(context, currentUser);
            success.run();
        }, resultNotOk -> error.run());
    }

    /*
        Test only
     */
    @VisibleForTesting
    public void fakeLogIn(Context context, User user, Runnable success) {
        this.currentUser = user;
        LocalDataCacher.get().saveUser(context,user);
        success.run();
    }

    public void logOut(Context context) {
        this.currentUser = null;
        LocalDataCacher.get().deleteCurrentUser(context);
    }

    public boolean isLogged() {
        if (currentUser != null) {
            return true;
        } else {
            this.currentUser = LocalDataCacher.get().getCurrentUser(ctx);
        }
        return currentUser != null;
    }

}
