package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Recognition;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.List;

public class TableInputVODialog extends AbstractDialog {

        private Button btnSubmit;
        private Button btnCancel;
        private EditText etTime;
        private Button btnTime;
        private EditText etDate;
        private Button btnDate;
        private Spinner spinnerNeck;
        private EditText etDilation;
        private EditText etLvhead;

        private Report report;
        private Recognition.NeckType neckType;
        private Recognition recognition;

        public TableInputVODialog(@NonNull final Context context, final Report report, final Recognition recognition) {
                super(context);
                this.report = report;
                this.recognition = recognition;
                }

        public TableInputVODialog(@NonNull final Context context, final Report report) {
                super(context);
                this.report = report;
                }

        @Override
        protected void onCreate(final Bundle savedInstanceState) {
                super.onCreate(savedInstanceState, R.layout.dialog_table_input_vo, "Visita Ostetrica");

                initView();
                initViewData();
                btnTime.setOnClickListener(e -> etTime.setText(DateUtil.getCurrentTime()));
                btnDate.setOnClickListener(e -> etDate.setText(DateUtil.getCurrentDate()));
                btnSubmit.setOnClickListener(e -> onSubmit());
                btnCancel.setOnClickListener(e -> dismiss());
                }

        private void initViewData() {
                spinnerNeck.setOnItemSelectedListener(new NeckListener());
                if (recognition == null) {
                        return;
                }
                etTime.setText(UtilsView.emptyIfNull(recognition.getTime()));
                etDate.setText(UtilsView.emptyIfNull(recognition.getDate()));
                spinnerNeck.setSelection(getNeckSpinnerPosition(recognition.getNeck()));
                etDilation.setText(UtilsView.emptyIfNull(recognition.getDilation()));
                etLvhead.setText(UtilsView.emptyIfNull(recognition.getLvHead()));
        }

        private void onSubmit() {
                final Recognition r = validateData();
                if (r == null) {
                        return;
                }
                if (recognition != null) {
                        editColumn(report.getRecognitions(), r);
                        r.setDate(this.recognition.getDate());
                } else {
                        addNewColumn(report.getRecognitions(), r);
                        r.setDate(DateUtil.getCurrentDate());
                }
                DataRepository.get().updateReport(report, r, Recognition.NAME);
                this.dismiss();
        }

        private void addNewColumn(final List<Recognition> list, final Recognition r) {
                list.add(r);
        }

        private void editColumn(final List<Recognition> list, final Recognition r) {
                list.set(list.indexOf(recognition), r);
        }

        private Recognition validateData() {
                final Recognition r = new Recognition();
                if (UtilsView.isEmpty(etTime)) {
                        showError("Ora mancante");
                        return null;
                } else if (UtilsView.isEmpty(etDate)) {
                        showError("Data mancante");
                        return null;
                } else if (!DateUtil.validateTime(UtilsView.getValueAsString(etTime))) {
                        showError("Formato dell'ora errato");
                        return null;
                } else if (!DateUtil.validateDate(UtilsView.getValueAsString(etDate))) {
                        showError("Formato della data errato");
                        return null;
                }

                if (!UtilsView.isEmpty(etDilation) && !Recognition.validateDilation(UtilsView.getValueAsInteger(etDilation))) {
                        showError("Dilatazione non valida");
                        return null;
                }
                if (!UtilsView.isEmpty(etLvhead) && !Recognition.validateLvHead(UtilsView.getValueAsInteger(etLvhead))) {
                showError("Livello impegno testa non valido");
                return null;
                }

                r.setLvHead(UtilsView.getValueAsInteger(etLvhead));
                r.setNeck(neckType);
                r.setDilation(UtilsView.getValueAsInteger(etDilation));
                r.setTime(UtilsView.getValueAsString(etTime));
                r.setDate(UtilsView.getValueAsString(etDate));

                final Integer position = recognition == null ? report.getRecognitions().size() :
                report.getRecognitions().indexOf(recognition);
                r.setRecognType(Recognition.RecognType.VO);
                r.setPosition(position);
                return r;
        }

        private void initView() {
                btnSubmit = findViewById(R.id.btn_submit);
                btnCancel = findViewById(R.id.btn_cancel);
                etDate = findViewById(R.id.table_et_date);
                btnDate = findViewById(R.id.table_btn_date);
                etTime = findViewById(R.id.table_et_time);
                btnTime = findViewById(R.id.table_btn_time);
                spinnerNeck = findViewById(R.id.table_neck);
                etDilation = findViewById(R.id.table_et_dilation);
                etLvhead = findViewById(R.id.table_et_lvhead);

                initSpinner(spinnerNeck, R.array.neck_types);
        }

        private int getNeckSpinnerPosition(final Recognition.NeckType neckType) {
                if (neckType == Recognition.NeckType.APPIANATO) return 1;
                        else if (neckType == Recognition.NeckType.RACCORCIATO) return 2;
                        else if (neckType == Recognition.NeckType.SCOMPARSO) return 3;
                        else return 0;
        }


        /**
         * Listener for the neck spinner input
         */
        private class NeckListener implements AdapterView.OnItemSelectedListener {

            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                if (position == 0) neckType = null;
                else if (position == 1) neckType = Recognition.NeckType.APPIANATO;
                else if (position == 2) neckType = Recognition.NeckType.RACCORCIATO;
                else if (position == 3) neckType = Recognition.NeckType.SCOMPARSO;
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {
                neckType = null;
            }
        }
}
