package com.app.cerere.cerereapp.domain.model.event;


import com.app.cerere.cerereapp.domain.types.DrugTipology;
import com.app.cerere.cerereapp.domain.types.Drugs;
import com.app.cerere.cerereapp.domain.types.DrugsVia;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

public class Anesthetic implements Serializable {

    public enum DrugDetails {

        PERCENT("percent", "%"),
        MG("mg", "mg"),
        ML("ml", "ml"),
        MGC("mgc", "mgc"),
        BASALSPEED("basalspeed", "vel bas"),
        BOLI("boli", "boli"),
        LOCKOUT("lockout", "lockout"),
        INTERVAL("interval", "interv"),
        MLH("mlh", "ml/h");

        @Getter private final String serializedName;
        @Getter private final String printName;

        DrugDetails(final String name, final String printName) {
            this.serializedName = name;
            this.printName = printName;
        }
    }

    public static final String NAME = "anesthetic";

    @Getter @Setter private DrugsVia via;
    @Getter @Setter private DrugTipology tipology;
    @Getter @Setter private Drugs drug;
    @Getter @Setter private String solution;
    @Getter @Setter private String notes;
    @Setter private LinkedHashMap<DrugDetails, Double> details;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if(via == DrugsVia.GENERAL || via == DrugsVia.SPINAL) {

            sb.append(via.getPrintName() + (notes != null?": " + notes: ""));
        } else if (drug == Drugs.OTHERS && solution != null) {
            sb.append("Sol: " + solution + ",");

            for(DrugDetails key: details.keySet()) {
                sb.append(" " + key.getPrintName() + " " + details.get(key) + ",");
            }
            sb.append(" " + DrugDetails.MLH.getPrintName());
        } else {
            sb.append(drug.getPrintName());
            for(DrugDetails key: details.keySet()) {
                sb.append(" " + details.get(key) + " " + key.getPrintName());
            }
        }

        return sb.toString();
    }

    public Map<DrugDetails, Double> getDetails() {
        return Collections.unmodifiableMap(details);
    }
}
