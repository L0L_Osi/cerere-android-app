package com.app.cerere.cerereapp.android.local;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.app.cerere.cerereapp.App;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class LocalFileHandler {

    private static final String TAG = "MSG";
    private final Context ctx;
    private final File file;

    public LocalFileHandler(String name) {
        this.ctx = App.get().getApplicationContext();
        File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "cerere-app");
        if (!root.exists()) {
            if (root.mkdirs()) {
                Log.d(TAG, "dir created");
            } else {
                Log.d(TAG, "dir not created");
            }
        }
        file = new File(root, name);
    }

    private boolean isESWritable() {
        return App.get().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public String readFile(){
        String line = null;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null )
            {
                stringBuilder.append(line + System.getProperty("line.separator"));
            }
            fileInputStream.close();
            line = stringBuilder.toString();

            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            Log.d(TAG, ex.getMessage());
        }
        catch(IOException ex) {
            Log.d(TAG, ex.getMessage());
        }
        return line;
    }

    public boolean writeFile(String data) {
        if (this.isESWritable()) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file, false);
                fileOutputStream.write((data).getBytes());
                return true;
            } catch (FileNotFoundException ex) {
                Log.d(TAG, ex.getMessage());
            } catch (IOException ex) {
                Log.d(TAG, ex.getMessage());
            }
        } else {
            Toast.makeText(App.get().getApplicationContext(), "Attivare esportazione nella schermata di Login", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public boolean deleteFile() {
        return file.delete();
    }

    public boolean isUsable() {
        return file.exists();
    }
}
