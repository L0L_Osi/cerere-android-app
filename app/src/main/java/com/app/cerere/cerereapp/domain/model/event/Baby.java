package com.app.cerere.cerereapp.domain.model.event;

import com.app.cerere.cerereapp.domain.types.BirthResult;
import com.app.cerere.cerereapp.domain.types.Genders;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;


public class Baby extends AbstractEvent {

    @SerializedName("babies")
    public static final String NAME = "babies";

    @Getter @Setter private Genders gender;
    @Getter @Setter private Integer weight;
    @Getter @Setter private Integer apgar1;
    @Getter @Setter private Integer apgar5;
    @Getter @Setter private Double ph;
    @Getter @Setter private boolean resuscitation;
    @Getter @Setter private BirthResult result;
    @Getter @Setter private Integer id;

    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }

    public static boolean validateWeight(final Integer weight) {
        return weight != null && (weight > 0 && weight <= 6000);
    }

    public static boolean validateApgar(final Integer apgar) {
        return apgar != null && (apgar >= 0 && apgar <= 10);
    }

    public String toString() {
        return gender.toString() + ", " + weight + " g";
    }

}
