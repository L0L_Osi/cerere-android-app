package com.app.cerere.cerereapp;

public class C {

    private C() {}

    public static final String SELECTED_SECTION = "selected_section";
    public static final String REPORT_ID = "report_id";
    public static final String BROADCAST_REFRESH_DATA = "refresh_data";
    public static final String START_FOREGROUND = "foreground";

}