package com.app.cerere.cerereapp.ui.dialogs;

public interface PartialDataListener<X> {

    void setData(X x);
}
