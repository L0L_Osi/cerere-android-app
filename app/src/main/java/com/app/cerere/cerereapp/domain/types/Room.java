package com.app.cerere.cerereapp.domain.types;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public enum Room {
    @SerializedName("iris")
    ONE(1, "iris", "Iris"),

    @SerializedName("giglio")
    TWO(2, "giglio", "Giglio"),

    @SerializedName("gardenia")
    THREE(3, "gardenia", "Gardenia"),

    @SerializedName("mimosa")
    FOUR(4, "mimosa", "Mimosa");

    @Getter private final Integer code;
    @Getter private final String name;
    @Getter private final String printName;

    Room(Integer code, String name, String printName) {
        this.code = code;
        this.name = name;
        this.printName = printName;
    }

    @Override
    public String toString() {
        return this.getPrintName();
    }
}