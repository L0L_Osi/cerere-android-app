package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class MembraneRupture extends AbstractEvent {

    public enum Type {
        @SerializedName("spontanea")
        SPONTANEA("spontanea"),

        @SerializedName("amnioressi")
        AMNIORESSI("amnioressi");

        @Getter private final String printName;

        Type(final String printName) {
            this.printName = printName;
        }
    }

    public enum FluidType {

        @SerializedName("limpido")
        LIMPIDO("Limpido"),

        @SerializedName("tinto_ns")
        TINTO_S("non significamente tinto"),

        @SerializedName("tinto_s")
        TINTO_NS("significamente tinto");

        @Getter private final String printName;

        FluidType(final String printName) {
            this.printName = printName;
        }
    }

    public static final String NAME = "membrane";

    @Getter @Setter private Type type;
    @Getter @Setter private FluidType fluidType;

    @Override
    public Status isComplete() {
        return null;
    }
}
