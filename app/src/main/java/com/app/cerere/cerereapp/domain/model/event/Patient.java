package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Patient extends AbstractEvent  {

    public enum BirthsHistory {
        @SerializedName("primipara")
        PRIMIPARA("Primipara"),

        @SerializedName("pluripara")
        PLURIPARA("Pluripara"),

        @SerializedName("nullipara")
        NULLIPARA("Nullipara");

        @Getter private final String printName;

        BirthsHistory(final String printName) {
            this.printName = printName;
        }

        @Override
        public String toString() {
            return getPrintName();
        }
    }

    public static final String NAME = "patient";

    @Getter @Setter private String name;
    @Getter @Setter private Integer age;
    @Getter @Setter private Integer height; /* centimeters */
    @Getter @Setter private Double weightPre;
    @Getter @Setter private Double weightCurrent;
    @Getter @Setter private Integer gestAge;
    @Getter @Setter private boolean pma;
    @Getter @Setter private BirthsHistory birthsHistory;
    @Getter @Setter private List<String> previousBirths = new ArrayList<>();


    public static boolean validateAge(final Integer age) {
        return age != null && (age >= 10 && age <= 60);
    }

    public static boolean validateHeight(final Integer height) {
        return height != null && (height >= 50 && height <= 250);
    }

    public static boolean validateWeight(final Double weight) {
        return weight != null && (weight >= 20 && weight <= 200);
    }

    public static boolean validateGestAge(final Integer gestAge) {
        return gestAge != null && (gestAge >= 0 && gestAge <= 55);
    }


    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }
}
