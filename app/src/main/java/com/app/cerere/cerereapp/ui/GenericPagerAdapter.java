package com.app.cerere.cerereapp.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class GenericPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> fList = new ArrayList<>();
    private final List<String> fTitleList = new ArrayList<>();

    public GenericPagerAdapter(final FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(final int position) {
        return fList.get(position);
    }

    @Override
    public int getCount() {
        return fList.size();
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return fTitleList.get(position);
    }

    public void addFragment(final Fragment fragment, String title) {
        fList.add(fragment);
        fTitleList.add(title);
    }
}
