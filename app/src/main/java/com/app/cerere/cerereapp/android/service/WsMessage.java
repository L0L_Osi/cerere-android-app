package com.app.cerere.cerereapp.android.service;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class WsMessage {

    public enum WsOperation {
        @SerializedName("update")
        UPDATE("update"),

        @SerializedName("none")
        NONE("none"),

        @SerializedName("delete")
        DELETE("delete"),

        @SerializedName("create")
        CREATE("create");

        private String printName;

        WsOperation(String s) {
            this.printName = s;
        }

        public String getPrintName() {
            return printName;
        }
    }

    @Getter @Setter private WsOperation operation;
    @Getter @Setter private String id;
}
