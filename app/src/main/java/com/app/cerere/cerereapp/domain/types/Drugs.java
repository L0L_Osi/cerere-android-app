package com.app.cerere.cerereapp.domain.types;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public enum Drugs {
    @SerializedName("bupivacaina")
    BUPIVACAINA("bupivacaina", "Bupi"),

    @SerializedName("ropivacaina")
    ROPIVACAINA("ropivacaina", "Ropi"),

    @SerializedName("sufentanil")
    SUFENTANIL("sufentanil", "Sufen"),

    @SerializedName("lidocaina")
    LIDOCAINA("lidocaina", "Lidoc"),

    @SerializedName("others")
    OTHERS("others", "");

    @Getter private final String name;
    @Getter private final String printName;

    Drugs(final String name, final String printName) {
        this.name = name;
        this.printName = printName;
    }
}
