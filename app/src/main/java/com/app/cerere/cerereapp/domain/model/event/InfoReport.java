package com.app.cerere.cerereapp.domain.model.event;

import android.util.Pair;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class InfoReport extends AbstractEvent {

    public static final String NAME = "info";

    public enum WorkerType {

        ANESTETISTA("Anestetista", "anesthetists"),
        OSTETRICO("Ostetrico", "obstetricians"),
        GINECOLOGO("Ginecologo", "gynecologists");

        @Getter private final String printName;
        @Getter private final String name;

        WorkerType(String printName, String name) {
            this.printName = printName;
            this.name = name;
        }
    }

    @Getter @Setter private transient List<Pair<String, String>> anesthetists;
    @Getter @Setter private transient List<Pair<String, String>> obstetricians;
    @Getter @Setter private transient List<Pair<String, String>> gynecologists;
    @Getter @Setter private String entranceDate;
    @Getter @Setter private String entranceTime;
    @Getter @Setter private boolean consensus;


    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }
}
