package com.app.cerere.cerereapp.ui.main.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Labor;
import com.app.cerere.cerereapp.domain.model.event.LaborComplications;
import com.app.cerere.cerereapp.domain.model.event.MembraneRupture;
import com.app.cerere.cerereapp.ui.dialogs.LaborComplicationsDialog;
import com.app.cerere.cerereapp.ui.dialogs.LaborDialog;
import com.app.cerere.cerereapp.ui.dialogs.MembraneRuptureDialog;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class FragmentDetailsLabor extends AbstractFragmentDetails {

    private Button fLaborLabor;
    private Button fLaborMembrane;
    private Button fLaborComplications;

    private TableLayout tableLabor;
    private TableLayout tableMembrane;
    private TableLayout tableComplications;


    private Report report;

    public static FragmentDetailsLabor newInstance(final Report report) {
        final FragmentDetailsLabor f = new FragmentDetailsLabor();
        final Bundle b = new Bundle();
        b.putSerializable(SELECTED_REPORT, report);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(SELECTED_REPORT)) {
            this.report = (Report) getArguments().getSerializable(SELECTED_REPORT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View v =  inflater.inflate(R.layout.fragment_details_labor, container, false);
        initView(v);

        fLaborLabor.setOnClickListener(e -> showDialogAndReload(this, new LaborDialog(getContext(), report)));
        fLaborMembrane.setOnClickListener(e -> showDialogAndReload(this, new MembraneRuptureDialog(getContext(), report)));
        fLaborComplications.setOnClickListener(e -> showDialogAndReload(this, new LaborComplicationsDialog(getContext(), report)));

        return v;
    }

    private void initView(final View v) {
        fLaborLabor = v.findViewById( R.id.f_labor_labor );
        fLaborMembrane = v.findViewById( R.id.f_labor_membrane);
        fLaborComplications = v.findViewById( R.id.f_labor_complications);

        tableLabor = v.findViewById(R.id.labor_table_travaglio);
        tableMembrane = v.findViewById(R.id.labor_table_membrane);
        tableComplications = v.findViewById( R.id.labor_table_complications);

        reloadData();
    }

    @Override
    public void reloadData() {
        tableLabor.removeAllViews();
        tableMembrane.removeAllViews();
        tableComplications.removeAllViews();

        updateTableLabor(report.getLabor());
        updateTableMembrane(report.getMembraneRupture());
        updateTableComplications(report.getLaborComplications());
    }

    private void updateTableLabor(Optional<Labor> labor) {
        if(labor.isPresent()) {
            Labor l = labor.get();
            Map<String, String> values = new LinkedHashMap<>();
            if(l.getType() != null) {
                values.put("Tipo Travaglio", l.getType().getPrintName());
            }
            if(l.getIndottoType() != null) {
                values.put("Tipo Indotto", l.getIndottoTypeToString());
            }
            values.put("Tolac", l.isTolac() ? "Si": "No");
            updateTable(values, tableLabor);
        }
    }

    private void updateTableMembrane(Optional<MembraneRupture> membrane) {
        if(membrane.isPresent()) {
            MembraneRupture m = membrane.get();
            Map<String, String> values = new LinkedHashMap<>();
            if(m.getDate() != null) {
                values.put("Data", m.getDate());
            }
            if(m.getTime() != null) {
                values.put("Ora", m.getTime());
            }
            if(m.getType() != null) {
                values.put("Tipo", m.getType().getPrintName());
            }
            if(m.getFluidType() != null) {
                values.put("Liquido Amniotico", m.getFluidType().getPrintName());
            }
            updateTable(values, tableMembrane);
        }
    }

    private void updateTableComplications(Optional<LaborComplications> complications) {
        if(complications.isPresent()) {
            LaborComplications lc = complications.get();
            Map<String, String> values = new LinkedHashMap<>();
            if(lc.getType() != null) {
                StringBuilder builder = new StringBuilder();
                for(int i = 0; i < lc.getType().size(); i++) {
                    if(lc.getType().get(i) == LaborComplications.Type.OTHER) {
                        builder.append(lc.getOther());
                    } else {
                        builder.append(lc.getType().get(i));
                    }
                    if(i < lc.getType().size() - 1) {
                        builder.append(",\n");
                    }
                }
                values.put("Tipologia", builder.toString());
            }
            updateTable(values, tableComplications);
        }
    }



    private void updateTable(Map<String, String> values, TableLayout table) {
        for(String key: values.keySet()) {
            TableRow row = getRowWithParams(getContext());
            row.addView(getTvWithParams(getContext(), key));
            row.addView(getTvWithParams(getContext(), values.get(key)));
            row.setBackgroundColor((table.getChildCount() % 2) == 0 ? Color.WHITE : Color.LTGRAY);
            table.addView(row);
        }
    }

    private TextView getTvWithParams(final Context ctx, final String text) {
        final TextView tv = new TextView(ctx);
        tv.setText(text);
        tv.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 1));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER);
        return tv;
    }

    private TableRow getRowWithParams(final Context ctx) {
        final TableRow row = new TableRow(ctx);
        row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, 1));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setPadding(0, 10, 0, 10);
        return row;
    }
}

