package com.app.cerere.cerereapp.android.local.JsonUtils;

import android.icu.text.IDNA;
import android.util.Pair;

import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.domain.model.event.InfoReport.WorkerType;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class CustomInfoReportSerializer implements JsonSerializer<InfoReport> {


    public JsonElement serialize(InfoReport info, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        if(info != null) {
            try {

                if (info.getEntranceDate() != null)
                    obj.addProperty("entranceDate", info.getEntranceDate());
                if (info.getEntranceTime() != null)
                    obj.addProperty("entranceTime", info.getEntranceTime());
                obj.addProperty("consensus", info.isConsensus());

                if(info.getAnesthetists() != null)
                    obj.add("anesthetists", getWorkers(info.getAnesthetists()));
                if(info.getObstetricians() != null)
                    obj.add("obstetricians", getWorkers(info.getObstetricians()));
                if(info.getGynecologists() != null)
                    obj.add("gynecologists", getWorkers(info.getGynecologists()));

                return obj;
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
        return null;
    }

    private JsonArray getWorkers(List<Pair<String, String>> list) {
        JsonArray jsonWorkers = new JsonArray();

        if(list != null) {
            for(Pair<String, String> turn: list) {
                if(turn != null && turn.first != null && turn.second != null) {
                    JsonObject obj = new JsonObject();
                    obj.addProperty("first", turn.first);
                    obj.addProperty("second", turn.second);
                    jsonWorkers.add(obj);
                }
            }
            if(!list.isEmpty())
                return jsonWorkers;
        }
        return null;
    }

}
