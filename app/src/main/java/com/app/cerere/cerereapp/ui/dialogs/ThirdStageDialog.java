package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.ThirdStage;

public class ThirdStageDialog extends AbstractDialog {

    private final Report report;
    private Switch switchTherapy;
    private Button btnSubmit;
    private Button btnCancel;

    public ThirdStageDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_thirdstage, "Terzo stadio");
        initView();
        initViewData();

        btnCancel.setOnClickListener(e -> this.dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private void initView() {
        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnCancel = (Button) findViewById(R.id.btn_cancel);
        switchTherapy = findViewById(R.id.thirdstage_switch_therapy);
    }

    private void initViewData() {
        if (report.getThirdStage().isPresent()) {
            switchTherapy.setChecked(report.getThirdStage().get().isTherapy());
        }
    }

    private void onSubmit() {
        final ThirdStage ts = new ThirdStage();
        ts.setTherapy(switchTherapy.isChecked());
        report.setThirdStage(ts);
        DataRepository.get().updateReport(report, ts, ThirdStage.NAME);
        this.dismiss();
    }
}
