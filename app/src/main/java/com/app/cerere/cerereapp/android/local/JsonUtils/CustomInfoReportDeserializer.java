package com.app.cerere.cerereapp.android.local.JsonUtils;

import android.util.Pair;

import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.domain.model.event.InfoReport.WorkerType;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class CustomInfoReportDeserializer implements JsonDeserializer<InfoReport> {

    private JsonObject obj;

    public InfoReport deserialize(JsonElement e, Type typeOfSrc, JsonDeserializationContext context) {
        InfoReport info = new InfoReport();
        try {
            this.obj = e.getAsJsonObject();

            info.setAnesthetists(getWorkers(WorkerType.ANESTETISTA));
            info.setObstetricians(getWorkers(WorkerType.OSTETRICO));
            info.setGynecologists(getWorkers(WorkerType.GINECOLOGO));

            if(this.obj.has("entranceDate")) {
                info.setEntranceDate(this.obj.get("entranceDate").getAsString());
            }
            if(this.obj.has("entranceTime")) {
                info.setEntranceTime(this.obj.get("entranceTime").getAsString());
            }
            if(this.obj.has("consensus")) {
                info.setConsensus(this.obj.get("consensus").getAsBoolean());
            }

            return info;
        } catch(Exception err) {
            err.printStackTrace();
            return null;
        }
    }

    private List<Pair<String, String>> getWorkers(WorkerType type) {
        JsonArray jsonWorkers = this.obj.getAsJsonArray(type.getName());

        if(jsonWorkers != null) {
            List<Pair<String, String>> list = new LinkedList<>();
            for(JsonElement turn: jsonWorkers) {
                JsonObject temp = turn.getAsJsonObject();
                list.add(new Pair<>(temp.get("first").getAsString(), temp.get("second").getAsString()));
            }
            if(!list.isEmpty())
                return list;
        }
        return null;
    }

}
