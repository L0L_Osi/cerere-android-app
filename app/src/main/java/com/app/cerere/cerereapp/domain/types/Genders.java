package com.app.cerere.cerereapp.domain.types;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public enum Genders {
    @SerializedName("male")
    MALE("Maschio"),

    @SerializedName("female")
    FEMALE("Femmina");

    @Getter
    private final String printName;

    Genders(final String printName) {
        this.printName = printName;
    }

    @Override
    public String toString() {
        return this.getPrintName();
    }
}
