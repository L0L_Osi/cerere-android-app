package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Pph;
import com.app.cerere.cerereapp.domain.model.event.Pph.PatientResult;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedList;
import java.util.List;

public class PphDialog extends AbstractDialog {

    private final Report report;

    private Button btnSubmit;
    private Button btnCancel;

    private Switch switchHaemodynamic;
    private EditText etVenousAccess;
    private EditText etVenousDiameter;
    private Switch switchCatheter;
    private EditText etCrystalloid;

    private RadioButton radioPretrasfusionalTestYes;
    private RadioButton radioPretrasfusionalTestNo;
    private RadioButton radioPretrasfusionalTestBleeding;

    private Switch switchClotting;
    private Switch switchEga;
    private Switch switchRotem;
    private Switch switchEmazie;

    private CheckBox radioUterotonicTherapyOxytocin;
    private CheckBox radioUterotonicTherapyErgometrine;
    private CheckBox radioUterotonicTherapySulprostone;

    private Switch switchSommAcTranexamic;
    private Switch switchRepetitionAcTranexamic;

    private Switch switchTrasEC;
    private EditText etTrasECNum;
    private Switch switchTrasPFC;
    private EditText etTrasPFCNum;
    private Switch switchTrasPoolPlatelet;
    private EditText etTrasPoolPlateletNum;

    private Switch switchTrasGuidedRotem;
    private Switch switchTrasFixedPackages;
    private RadioButton radioSommConcFibrinogenNone;
    private RadioButton radioSommConcFibrinogen2g;
    private RadioButton radioSommConcFibrinogen4g;
    private Switch switchBakriBaloon;
    private Switch switchConservativeSurgery;
    private Switch switchInterventionalRadiology;
    private Switch switchRecombinantFactorVII;
    private RadioButton radioHysterectomyNo;
    private RadioButton radioHysterectomyTotal;
    private RadioButton radioHysterectomySubtotal;
    private RadioButton radioResultDepartment;
    private RadioButton radioResultTIN;
    private RadioButton radioResultDeceased;
    private RadioGroup radiogroupResult;
    private EditText etDeceaseDate;
    private EditText etDeceaseTime;
    private Button btnDate;
    private Button btnTime;

    private Group groupTrasECNum;
    private Group groupTrasPFCNum;
    private Group groupTrasPoolPlateletNum;
    private Group groupDeceased;


    public PphDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_pph, "Emorraggia Postparto");

        initView();
        initViewData();
        setUpButtons();
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);

        switchHaemodynamic = findViewById(R.id.pph_switch_haemodynamic);
        etVenousAccess = findViewById(R.id.pph_venousAccess);
        etVenousDiameter = findViewById(R.id.pph_venousDiameter);
        switchCatheter = findViewById(R.id.pph_switch_catheter);
        etCrystalloid = findViewById(R.id.pph_crystalloid);

        radioPretrasfusionalTestYes = findViewById(R.id.pph_radio_pretrasfusionalTest_yes);
        radioPretrasfusionalTestNo = findViewById(R.id.pph_radio_pretrasfusionalTest_no);
        radioPretrasfusionalTestBleeding = findViewById(R.id.pph_radio_pretrasfusionalTest_bleeding);

        switchClotting = findViewById(R.id.pph_switch_clotting);
        switchEga = findViewById(R.id.pph_switch_ega);
        switchRotem = findViewById(R.id.pph_switch_rotem);
        switchEmazie = findViewById(R.id.pph_switch_emazie);

        radioUterotonicTherapyOxytocin = findViewById(R.id.pph_radio_uterotonicTherapy_oxytocin);
        radioUterotonicTherapyErgometrine = findViewById(R.id.pph_radio_uterotonicTherapy_ergometrine);
        radioUterotonicTherapySulprostone = findViewById(R.id.pph_radio_uterotonicTherapy_sulprostone);

        switchSommAcTranexamic = findViewById(R.id.pph_switch_sommAcTranexamic);
        switchRepetitionAcTranexamic = findViewById(R.id.pph_switch_repetitionAcTranexamic);

        switchTrasEC = findViewById(R.id.pph_switch_trasEC);
        etTrasECNum = findViewById(R.id.pph_trasECnum);
        switchTrasPFC = findViewById(R.id.pph_switch_trasPFC);
        etTrasPFCNum = findViewById(R.id.pph_trasPFCnum);
        switchTrasPoolPlatelet = findViewById(R.id.pph_switch_trasPoolPlatelet);
        etTrasPoolPlateletNum = findViewById(R.id.pph_trasPoolPlateletnum);

        switchTrasGuidedRotem = findViewById(R.id.pph_switch_trasGuidedRotem);
        switchTrasFixedPackages = findViewById(R.id.pph_switch_trasFixedPackages);
        radioSommConcFibrinogenNone = findViewById(R.id.pph_radio_sommConcFibrinogen_none);
        radioSommConcFibrinogen2g = findViewById(R.id.pph_radio_sommConcFibrinogen_2g);
        radioSommConcFibrinogen4g = findViewById(R.id.pph_radio_sommConcFibrinogen_4g);
        switchBakriBaloon = findViewById(R.id.pph_switch_barkiBaloon);
        switchConservativeSurgery = findViewById(R.id.pph_switch_conservativeSurgery);
        switchInterventionalRadiology = findViewById(R.id.pph_switch_interventionalRadiology);
        switchRecombinantFactorVII = findViewById(R.id.pph_switch_recombinantFactorVII);
        radioHysterectomyNo = findViewById(R.id.pph_radio_hysterectomy_no);
        radioHysterectomyTotal = findViewById(R.id.pph_radio_hysterectomy_total);
        radioHysterectomySubtotal = findViewById(R.id.pph_radio_hysterectomy_subtotal);
        radioResultDepartment = findViewById(R.id.pph_radio_result_department);
        radioResultTIN = findViewById(R.id.pph_radio_result_tin);
        radioResultDeceased = findViewById(R.id.pph_radio_result_deceased);
        radiogroupResult = findViewById(R.id.pph_radiogroup_result);
        etDeceaseDate = findViewById(R.id.pph_deceaseDate);
        etDeceaseTime = findViewById(R.id.pph_deceaseTime);
        btnDate = findViewById(R.id.pph_btn_date);
        btnTime = findViewById(R.id.pph_btn_time);

        groupDeceased = findViewById(R.id.pph_group_deceased);
        groupTrasECNum = findViewById(R.id.pph_group_trasECNum);
        groupTrasPFCNum = findViewById(R.id.pph_group_traPFCNum);
        groupTrasPoolPlateletNum = findViewById(R.id.pph_group_trasPoolPlateletNum);

    }

    private void setUpButtons() {
        radiogroupResult.setOnCheckedChangeListener((e, id) -> {
            if(radioResultDeceased.isChecked()) {
                groupDeceased.setVisibility(View.VISIBLE);
            } else {
                groupDeceased.setVisibility(View.GONE);
            }
        });

        switchTrasEC.setOnCheckedChangeListener((e, x)-> {
            if(switchTrasEC.isChecked()) {
                groupTrasECNum.setVisibility(View.VISIBLE);
            } else {
                groupTrasECNum.setVisibility(View.GONE);
            }
        });

        switchTrasPFC.setOnCheckedChangeListener((e, x)-> {
            if(switchTrasPFC.isChecked()) {
                groupTrasPFCNum.setVisibility(View.VISIBLE);
            } else {
                groupTrasPFCNum.setVisibility(View.GONE);
            }
        });

        switchTrasPoolPlatelet.setOnCheckedChangeListener((e, x) -> {
            if(switchTrasPoolPlatelet.isChecked()) {
                groupTrasPoolPlateletNum.setVisibility(View.VISIBLE);
            } else {
                groupTrasPoolPlateletNum.setVisibility(View.GONE);
            }
        });

        btnCancel.setOnClickListener(e -> dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
        btnDate.setOnClickListener(e -> etDeceaseDate.setText(DateUtil.getCurrentDate()));
        btnTime.setOnClickListener(e -> etDeceaseTime.setText(DateUtil.getCurrentTime()));
    }

    private void initViewData() {
        groupDeceased.setVisibility(View.GONE);
        groupTrasECNum.setVisibility(View.GONE);
        groupTrasPFCNum.setVisibility(View.GONE);
        groupTrasPoolPlateletNum.setVisibility(View.GONE);

        if(report == null || !report.getPph().isPresent()) return;
        else {
            Pph pph = report.getPph().get();

            switchHaemodynamic.setChecked(pph.isHaemodynamic());
            etVenousAccess.setText(UtilsView.emptyIfNull(pph.getVenousCount()));
            etVenousDiameter.setText(UtilsView.emptyIfNull(pph.getVenousDiameter()));
            switchCatheter.setChecked(pph.isCatheter());
            etCrystalloid.setText(UtilsView.emptyIfNull(pph.getCrystalloid()));

            if(pph.getPretrasfusional() == Pph.PretrasfusionalTest.YES) {
                radioPretrasfusionalTestYes.setChecked(true);
            } else if (pph.getPretrasfusional() == Pph.PretrasfusionalTest.NO){
                radioPretrasfusionalTestNo.setChecked(true);
            } else if (pph.getPretrasfusional() == Pph.PretrasfusionalTest.BLEEDING) {
                radioPretrasfusionalTestBleeding.setChecked(true);
            }

            switchClotting.setChecked(pph.isClotting());
            switchEga.setChecked(pph.isEga());
            switchRotem.setChecked(pph.isRotem());
            switchEmazie.setChecked(pph.isEmazie());


            if(pph.getUterotonicTherapy().contains(Pph.UterotonicTherapy.OXYTOCIN)) {
                radioUterotonicTherapyOxytocin.setChecked(true);
            }
            if (pph.getUterotonicTherapy().contains(Pph.UterotonicTherapy.ERGOMETRINE)){
                radioUterotonicTherapyErgometrine.setChecked(true);
            }
            if (pph.getUterotonicTherapy().contains(Pph.UterotonicTherapy.SULPROSTONE)) {
                radioUterotonicTherapySulprostone.setChecked(true);
            }

            switchSommAcTranexamic.setChecked(pph.isSommAcTranexamic());
            switchRepetitionAcTranexamic.setChecked(pph.isRepetitionAcTranexamic());

            if(pph.getTransEcBags() > 0) {
                switchTrasEC.setChecked(true);
                etTrasECNum.setText(String.valueOf(pph.getTransEcBags()));
            }
            if(pph.getTransPFCBags() > 0) {
                switchTrasPFC.setChecked(true);
                etTrasPFCNum.setText(String.valueOf(pph.getTransPFCBags()));
            }
            if(pph.getTransPoolPlateletBags() > 0) {
                switchTrasPoolPlatelet.setChecked(true);
                etTrasPoolPlateletNum.setText(String.valueOf(pph.getTransPoolPlateletBags()));
            }

            switchTrasGuidedRotem.setChecked(pph.isTransGuidedRotem());
            switchTrasFixedPackages.setChecked(pph.isTransFixedPackages());

            if(pph.getSommConcFibrinogen() == Pph.SommConcFibrinogen.NONE) {
                radioSommConcFibrinogenNone.setChecked(true);
            } else if (pph.getSommConcFibrinogen() == Pph.SommConcFibrinogen.TWOG){
                radioSommConcFibrinogen2g.setChecked(true);
            } else if (pph.getSommConcFibrinogen() == Pph.SommConcFibrinogen.FOURG) {
                radioSommConcFibrinogen4g.setChecked(true);
            }

            switchBakriBaloon.setChecked(pph.isBakriBalloon());
            switchConservativeSurgery.setChecked(pph.isConservativeSurgery());
            switchInterventionalRadiology.setChecked(pph.isInterventionalRadiology());
            switchRecombinantFactorVII.setChecked(pph.isRecombinantFactorVII());

            if(pph.getHysterectomy() == Pph.Hysterectomy.NONE) {
                radioHysterectomyNo.setChecked(true);
            } else if (pph.getHysterectomy() == Pph.Hysterectomy.TOTAL){
                radioHysterectomyTotal.setChecked(true);
            } else if (pph.getHysterectomy() == Pph.Hysterectomy.SUBTOTAL) {
                radioHysterectomySubtotal.setChecked(true);
            }

            if(pph.getResult() == PatientResult.REPARTO) {
                radioResultDepartment.setChecked(true);
            } else if (pph.getResult() == PatientResult.TI){
                radioResultTIN.setChecked(true);
            } else if (pph.getResult() == PatientResult.DECEDUTO) {
                radioResultDeceased.setChecked(true);
                etDeceaseDate.setText(pph.getDeceaseDate());
                etDeceaseTime.setText(pph.getDeceaseTime());
                groupDeceased.setVisibility(View.VISIBLE);
            }



            groupTrasECNum.setVisibility(switchTrasEC.isChecked()? View.VISIBLE: View.GONE);
            groupTrasPFCNum.setVisibility(switchTrasPFC.isChecked()? View.VISIBLE: View.GONE);
            groupTrasPoolPlateletNum.setVisibility(switchTrasPoolPlatelet.isChecked()? View.VISIBLE: View.GONE);
        }
    }

    private void onSubmit() {
        final Pph pph = validateData();
        if (pph == null) {
            showDefaultError();
        } else {
            report.setPph(pph);
            DataRepository.get().updateReport(report, pph, Pph.NAME);
            this.dismiss();
        }
    }

    private Pph validateData() {
        final Pph pph = new Pph();
        pph.setHaemodynamic(switchHaemodynamic.isChecked());
        pph.setVenousCount(UtilsView.getValueAsInteger(etVenousAccess));
        pph.setVenousDiameter(UtilsView.getValueAsDouble(etVenousDiameter));
        pph.setCatheter(switchCatheter.isChecked());
        pph.setCrystalloid(UtilsView.getValueAsString(etCrystalloid));

        if (radioPretrasfusionalTestYes.isChecked()) {
            pph.setPretrasfusional(Pph.PretrasfusionalTest.YES);
        } else if (radioPretrasfusionalTestNo.isChecked()) {
            pph.setPretrasfusional(Pph.PretrasfusionalTest.NO);
        } else if (radioPretrasfusionalTestBleeding.isChecked()) {
            pph.setPretrasfusional(Pph.PretrasfusionalTest.BLEEDING);
        }

        pph.setClotting(switchClotting.isChecked());
        pph.setEga(switchEga.isChecked());
        pph.setRotem(switchRotem.isChecked());
        pph.setEmazie(switchEmazie.isChecked());

        List<Pph.UterotonicTherapy> list = new LinkedList<>();
        if (radioUterotonicTherapyOxytocin.isChecked()) {
            list.add(Pph.UterotonicTherapy.OXYTOCIN);
        }
        if (radioUterotonicTherapyErgometrine.isChecked()) {
            list.add(Pph.UterotonicTherapy.ERGOMETRINE);
        }
        if (radioUterotonicTherapySulprostone.isChecked()) {
            list.add(Pph.UterotonicTherapy.SULPROSTONE);
        }
        pph.setUterotonicTherapy(list.size() > 0? list: null);

        pph.setSommAcTranexamic(switchSommAcTranexamic.isChecked());
        pph.setRepetitionAcTranexamic(switchRepetitionAcTranexamic.isChecked());


        if(switchTrasEC.isChecked() && UtilsView.isEmpty(etTrasECNum) ||
                switchTrasPFC.isChecked() && UtilsView.isEmpty(etTrasPFCNum) ||
                switchTrasPoolPlatelet.isChecked() && UtilsView.isEmpty(etTrasPoolPlateletNum)) {
            showDefaultError();
            return null;
        } else {
            pph.setTransEcBags(switchTrasEC.isChecked() ?
                    UtilsView.getValueAsInteger(etTrasECNum) : 0);
            pph.setTransPFCBags(switchTrasPFC.isChecked() ?
                    UtilsView.getValueAsInteger(etTrasPFCNum) : 0);
            pph.setTransPoolPlateletBags(switchTrasPoolPlatelet.isChecked() ?
                    UtilsView.getValueAsInteger(etTrasPoolPlateletNum) : 0);
        }

        pph.setTransGuidedRotem(switchTrasGuidedRotem.isChecked());
        pph.setTransFixedPackages(switchTrasFixedPackages.isChecked());

        if (radioSommConcFibrinogenNone.isChecked()) {
            pph.setSommConcFibrinogen(Pph.SommConcFibrinogen.NONE);
        } else if (radioSommConcFibrinogen2g.isChecked()) {
            pph.setSommConcFibrinogen(Pph.SommConcFibrinogen.TWOG);
        } else if (radioSommConcFibrinogen4g.isChecked()) {
            pph.setSommConcFibrinogen(Pph.SommConcFibrinogen.FOURG);
        }

        pph.setBakriBalloon(switchBakriBaloon.isChecked());
        pph.setConservativeSurgery(switchConservativeSurgery.isChecked());
        pph.setInterventionalRadiology(switchInterventionalRadiology.isChecked());
        pph.setRecombinantFactorVII(switchRecombinantFactorVII.isChecked());

        if (radioHysterectomyNo.isChecked()) {
            pph.setHysterectomy(Pph.Hysterectomy.NONE);
        } else if (radioHysterectomyTotal.isChecked()) {
            pph.setHysterectomy(Pph.Hysterectomy.TOTAL);
        } else if (radioHysterectomySubtotal.isChecked()) {
            pph.setHysterectomy(Pph.Hysterectomy.SUBTOTAL);
        }

        if (radioResultDepartment.isChecked()) {
            pph.setResult(PatientResult.REPARTO);
        } else if (radioResultTIN.isChecked()) {
            pph.setResult(PatientResult.TI);
        } else if (radioResultDeceased.isChecked()) {
            if(UtilsView.isEmpty(etDeceaseDate) ||
                    !DateUtil.validateDate(UtilsView.getValueAsString(etDeceaseDate))) {
                showError("Data decesso non valida");
                return null;
            } else if(UtilsView.isEmpty(etDeceaseTime) ||
                    !DateUtil.validateTime(UtilsView.getValueAsString(etDeceaseTime))) {
                showError("Orario decesso non valido");
                return null;
            } else {
                pph.setResult(PatientResult.DECEDUTO);
                pph.setDeceaseDate(UtilsView.getValueAsString(etDeceaseDate));
                pph.setDeceaseTime(UtilsView.getValueAsString(etDeceaseTime));
            }

        }
        return pph;
    }
}
