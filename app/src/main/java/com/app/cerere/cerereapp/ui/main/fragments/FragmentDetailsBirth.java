package com.app.cerere.cerereapp.ui.main.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Baby;
import com.app.cerere.cerereapp.domain.model.event.Birth;
import com.app.cerere.cerereapp.domain.model.event.BloodLoss;
import com.app.cerere.cerereapp.domain.model.event.OperatingRoom;
import com.app.cerere.cerereapp.domain.model.event.PostoperativeAnalgesia;
import com.app.cerere.cerereapp.domain.model.event.Pph;
import com.app.cerere.cerereapp.domain.model.event.Secondamento;
import com.app.cerere.cerereapp.domain.model.event.ThirdStage;
import com.app.cerere.cerereapp.ui.dialogs.BloodLossDialog;
import com.app.cerere.cerereapp.ui.dialogs.InsertBirthsDialog;
import com.app.cerere.cerereapp.ui.dialogs.OperatingRoomDialog;
import com.app.cerere.cerereapp.ui.dialogs.PphDialog;
import com.app.cerere.cerereapp.ui.dialogs.SecondamentoDialog;
import com.app.cerere.cerereapp.ui.dialogs.ThirdStageDialog;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class FragmentDetailsBirth extends AbstractFragmentDetails {

    private Button fBirthBtnBirth;
    private Button fBirthBtnBloodLoss;
    private Button fBirthBtnSecondamento;
    private Button fBirthBtnPPH;
    private Button fBirthBtnOpRoom;
    private Button fBirthBtnThirdStage;
    private TableLayout tableBirth;
    private TableLayout tableBaby;
    private TableLayout tableBloodLoss;
    private TableLayout tableSecondamento;
    private TableLayout tablePPH;
    private TableLayout tableOpRoom;
    private TableLayout tableThirdStage;

    private Report report;

    public static FragmentDetailsBirth newInstance(final Report report) {
        final FragmentDetailsBirth f = new FragmentDetailsBirth();
        final Bundle b = new Bundle();
        b.putSerializable(SELECTED_REPORT, report);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().containsKey(SELECTED_REPORT)) {
            this.report = (Report) getArguments().getSerializable(SELECTED_REPORT);
        }

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View v =  inflater.inflate(R.layout.fragment_details_birth, container, false);
        initView(v);

        fBirthBtnBirth.setOnClickListener(e -> showDialogAndReload(this, new InsertBirthsDialog(getContext(), report)));
        fBirthBtnBloodLoss.setOnClickListener(e -> showDialogAndReload(this, new BloodLossDialog(getContext(), report)));
        fBirthBtnSecondamento.setOnClickListener(e -> showDialogAndReload(this, new SecondamentoDialog(getContext(), report)));
        fBirthBtnPPH.setOnClickListener(e -> showDialogAndReload(this, new PphDialog(getContext(), report)));
        fBirthBtnOpRoom.setOnClickListener(e -> showDialogAndReload(this, new OperatingRoomDialog(getContext(), report)));
        fBirthBtnThirdStage.setOnClickListener(e -> showDialogAndReload(this, new ThirdStageDialog(getContext(), report)));
        return v;
    }

    private void initView(final View v) {
        fBirthBtnBirth = v.findViewById( R.id.f_birth_btn_birth );
        fBirthBtnBloodLoss = v.findViewById(R.id.f_birth_btn_bloodloss);
        fBirthBtnSecondamento = v.findViewById(R.id.f_birth_btn_secondamento);
        fBirthBtnPPH = v.findViewById(R.id.f_birth_btn_pph);
        fBirthBtnOpRoom = v.findViewById(R.id.f_birth_btn_op);
        fBirthBtnThirdStage = v.findViewById(R.id.f_birth_btn_thirdstage);

        tableBirth = v.findViewById(R.id.birth_table_birth);
        tableBaby = v.findViewById(R.id.birth_table_baby);
        tableBloodLoss = v.findViewById(R.id.birth_table_bloodloss);
        tableSecondamento = v.findViewById(R.id.birth_table_secondamento);
        tablePPH = v.findViewById(R.id.birth_table_pph);
        tableOpRoom = v.findViewById(R.id.birth_table_op);
        tableThirdStage = v.findViewById(R.id.birth_table_terzostadio);

        reloadData();
    }

    @Override
    public void reloadData() {
        tableBirth.removeAllViews();
        tableBaby.removeAllViews();
        tableBloodLoss.removeAllViews();
        tableSecondamento.removeAllViews();
        tablePPH.removeAllViews();
        tableOpRoom.removeAllViews();
        tableThirdStage.removeAllViews();

        updateBirthTable(report.getBirths());
        updateBabyTable(report.getBabies());
        updateBloodLossTable(report.getBloodLoss());
        updateSecondamentoTable(report.getSecondamento());
        updatePPHTable(report.getPph());
        updateOpRoomTable(report.getOperatingRoom());
        updateTableThirdStage(report.getThirdStage());
    }

    private void updateBirthTable(Optional<List<Birth>> birth) {
        if(birth.isPresent()) {
            Map<String, String> values = new LinkedHashMap<>();
            List<Birth> list = birth.get();
            for (Birth b: list) {
                values.put("Parto N." + b.getId().toString(), "");
                if(b.getType() != null) {
                    StringBuilder builder = new StringBuilder();
                    for(int i = 0; i < b.getType().size(); i++) {
                        builder.append(b.getType().get(i));
                        if(i < b.getType().size() - 1) {
                            builder.append("\n");
                        }
                    }
                    values.put("Tipologia " + b.getId().toString(), builder.toString());
                }
                values.put("Episiotomia " + b.getId().toString(), b.isEpisiotomia()? "Si": "No");
                if(b.getNote() != null) {
                    values.put("Note " + b.getId().toString(), b.getNote());
                }
            }

            updateTable(values, tableBirth);
        }
    }

    private void updateBabyTable(Optional<List<Baby>> baby) {
        if(baby.isPresent()) {
            List<Baby> list = baby.get();
            Map<String, String> values = new LinkedHashMap<>();
            for (Baby b: list) {
                values.put("Neonato N." + b.getId().toString(), "");
                if(b.getGender() != null) {
                    values.put("Sesso " + b.getId().toString(), b.getGender().getPrintName());
                }
                if(b.getWeight() != null) {
                    values.put("Peso " + b.getId().toString(), b.getWeight().toString());
                }
                if(b.getApgar1() != null) {
                    values.put("Apgar1 " + b.getId().toString(), b.getApgar1().toString());
                }
                if(b.getApgar5() != null) {
                    values.put("Apgar5 " + b.getId().toString(), b.getApgar5().toString());
                }
                if(b.getPh() != null) {
                    values.put("Ph " + b.getId().toString(), b.getPh().toString());
                }
                values.put("Rianimazione\nNeonatale " + b.getId().toString(), b.isResuscitation()? "Si": "No");
                if(b.getResult() != null) {
                    values.put("Esito " + b.getId().toString(), b.getResult().getPrintName());
                }
            }
            updateTable(values, tableBaby);
        }
    }

    private void updateBloodLossTable(Optional<BloodLoss> bloodLoss) {
        if(bloodLoss.isPresent()) {
            BloodLoss b = bloodLoss.get();
            Map<String, String> values = new LinkedHashMap<>();
            if(b.getQuantity() != null) {
                values.put("Quantità", b.getQuantity().toString());
            }
            updateTable(values, tableBloodLoss);
        }
    }

    private void updateSecondamentoTable(Optional<Secondamento> secondamento) {
        if(secondamento.isPresent()) {
            Secondamento s = secondamento.get();
            Map<String, String> values = new LinkedHashMap<>();
            if(s.getType() != null) {
                values.put("Tipo", s.getType().getPrintName());
            }
            updateTable(values, tableSecondamento);
        }
    }

    private void updatePPHTable(Optional<Pph> pph) {
        if(pph.isPresent()) {
            Pph p = pph.get();
            Map<String, String> values = new LinkedHashMap<>();

            values.put("Instabilità Emodinamica", p.isHaemodynamic()? "Si": "No");
            if(p.getVenousCount() != null) {
                values.put("Numero Accessi Venosi", p.getVenousCount().toString());
            }
            if(p.getVenousDiameter() != null) {
                values.put("Diametro Accessi Venosi", p.getVenousDiameter().toString());
            }
            values.put("Catetere", p.isCatheter()? "Si": "No");
            if(p.getCrystalloid() != null) {
                values.put("Volume Cristalloidi", p.getCrystalloid());
            }
            if(p.getPretrasfusional() != null) {
                values.put("Test Pretrasfuzionali", p.getPretrasfusional().getName());
            }
            values.put("Clotting", p.isClotting()? "Si": "No");
            values.put("Ega", p.isEga()? "Si": "No");
            values.put("Rotem", p.isRotem()? "Si": "No");
            values.put("Emazie", p.isEmazie()? "Si": "No");

            if(p.getUterotonicTherapy() != null) {
                List<Pph.UterotonicTherapy> list = p.getUterotonicTherapy();
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    builder.append(list.get(i).getName());
                    if (i < list.size() - 1) {
                        builder.append("\n");
                    }
                }
                values.put("Terapia Uterotonica", builder.toString());
            }


            values.put("SommAc Tranexamico", p.isSommAcTranexamic()? "Si": "No");
            values.put("Ripetizione AcTranexamico", p.isRepetitionAcTranexamic()? "Si": "No");
            if(p.getTransEcBags() != null) {
                values.put("Sacche EC", p.getTransEcBags() > 0?  p.getTransEcBags().toString(): "No");
            }
            if(p.getTransPFCBags() != null) {
                values.put("Sacche PFC", p.getTransPFCBags() > 0?  p.getTransPFCBags().toString(): "No");
            }
            if(p.getTransPoolPlateletBags() != null) {
                values.put("Sacche Pool Piastrinico", p.getTransPoolPlateletBags() > 0?  p.getTransPoolPlateletBags().toString(): "No");
            }
            values.put("Transf con Rotem Guidato", p.isTransGuidedRotem()? "Si": "No");
            values.put("Pacchetti Fissi", p.isTransFixedPackages()? "Si": "No");
            if(p.getSommConcFibrinogen() != null) {
                values.put("Somm Fibrinogeno Conc", p.getSommConcFibrinogen().getName());
            }
            values.put("Barki Baloon", p.isBakriBalloon()? "Si": "No");
            values.put("Chirurgia Conservativa", p.isConservativeSurgery()? "Si": "No");
            values.put("Radiologia Interventistica", p.isInterventionalRadiology()? "Si": "No");
            values.put("Fattore VII Ricombinante", p.isRecombinantFactorVII()? "Si": "No");
            if(p.getHysterectomy() != null) {
                values.put("Isterectomia", p.getHysterectomy().getName());
            }
            if(p.getResult() != null) {
                values.put("Esito Paziente", p.getResult().getPrintName());
                if(p.getResult() == Pph.PatientResult.DECEDUTO) {
                    values.put("Data Decesso", p.getDeceaseDate());
                    values.put("Ora Decesso", p.getDeceaseTime());
                }
            }
            updateTable(values, tablePPH);
        }
    }

    private void updateOpRoomTable(Optional<OperatingRoom> operatingRoom) {
        if(operatingRoom.isPresent()) {
            OperatingRoom o = operatingRoom.get();
            Map<String, String> values = new LinkedHashMap<>();

            if(o.getOperation() != null) {
                values.put("Operazione", o.getOperation().getName());
            }
            if(o.getReason() != null) {
                values.put("Motivo", o.getReasonToString());
            }
            if(o.getAnesthetist() != null) {
                values.put("Anestetista", o.getAnesthetist());
            }
            if(o.getGynecologist() != null) {
                values.put("Ginecologo", o.getGynecologist());
            }
            values.put("Codice Rosso", o.isCodeRed()? "Si": "No");
            values.put("Digiuna", o.isFasting()? "Si": "No");
            if(o.getLastPeriduralBolusTC() != null) {
                values.put("Intervallo ultimo bolo peridurale-TC", o.getLastPeriduralBolusTC().toString());
            }
            if(o.getAnesthetics() != null) {

                final StringBuilder cell = new StringBuilder();
                o.getAnesthetics().stream().filter(x -> x != null )
                        .forEach(x -> cell.append(x.toString() + " "));
                values.put("Anestesia", cell.toString());
            }
            if(o.getAnestheticTime() != null) {
                values.put("Ora Anestesia", o.getAnestheticTime());
            }
            if(o.getIncisionTime() != null) {
                values.put("Ora Incisione", o.getIncisionTime());
            }
            values.put("Peridurale Efficace", o.isEffectivePeridural()? "Si": "No");
            values.put("IOT Difficile", o.isHardIOT()? "Si": "No");
            if(o.getBloodloss() != null) {
                values.put("Perdite Ematiche", o.getBloodloss().toString());
            }
            values.put("Trattamento PDPH", o.isPdphTreatment()? "Si": "No");
            updateTable(values, tableOpRoom);

            if(o.getPostoperativeAnalgesia() != null) {
                PostoperativeAnalgesia post = o.getPostoperativeAnalgesia();
                values.clear();
                TableRow postOperativeAnalgesiaRow = getRowWithParams(getContext());
                TextView title = getTvWithParams(getContext(), "Analgesia PostOperativa");
                title.setTypeface(null, Typeface.BOLD);
                postOperativeAnalgesiaRow.addView(title);
                postOperativeAnalgesiaRow.setBackgroundColor((tableOpRoom.getChildCount() % 2) == 0 ? Color.WHITE : Color.LTGRAY);
                tableOpRoom.addView(postOperativeAnalgesiaRow);
                if(post.getProtocolType() != null) {
                    values.put("Tipo Protocollo", post.getProtocolType());
                }
                if(post.getVariation() != null) {
                    values.put("Variazioni", post.getVariation());
                }
                if(post.getNotes() != null) {
                    values.put("Notes", post.getNotes());
                }
                updateTable(values, tableOpRoom);
            }
        }
    }

    private void updateTableThirdStage(Optional<ThirdStage> thirdStage) {
        if(thirdStage.isPresent()) {
            ThirdStage t = thirdStage.get();
            Map<String, String> values = new LinkedHashMap<>();
            values.put("Trattamento Attivo", t.isTherapy() ? "Si": "No");
            updateTable(values, tableThirdStage);
        }
    }

    private void updateTable(Map<String, String> values, TableLayout table) {
        for(String key: values.keySet()) {
            TableRow row = getRowWithParams(getContext());
            row.addView(getTvWithParams(getContext(), key));
            row.addView(getTvWithParams(getContext(), values.get(key)));
            row.setBackgroundColor((table.getChildCount() % 2) == 0 ? Color.WHITE : Color.LTGRAY);
            table.addView(row);
        }
    }

    private TextView getTvWithParams(final Context ctx, final String text) {
        final TextView tv = new TextView(ctx);
        tv.setText(text);
        tv.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 1));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER);
        return tv;
    }

    private TableRow getRowWithParams(final Context ctx) {
        final TableRow row = new TableRow(ctx);
        row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, 1));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setPadding(0, 10, 0, 10);
        return row;
    }
}
