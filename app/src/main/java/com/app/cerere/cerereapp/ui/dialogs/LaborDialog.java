package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Labor;
import com.app.cerere.cerereapp.domain.util.EnumUtils;
import com.app.cerere.cerereapp.ui.component.MySpinner;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedList;
import java.util.List;

public class LaborDialog extends AbstractDialog {

    private Button btnSubmit;
    private Button btnCancel;
    private RadioGroup laborRg;
    private RadioButton radioSpontaneo;
    private RadioButton radioIndotto;
    private Group groupIndotto;

    private CheckBox cbCRB;
    private CheckBox cbPropess;
    private CheckBox cbCytotec;
    private CheckBox cbOxytocin;

    private Switch tolac;

    private final Report report;

    public LaborDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_labor, "Travaglio");

        initView();
        initViewData();
        radioSpontaneo.setOnClickListener(e-> groupIndotto.setVisibility(View.GONE));
        radioIndotto.setOnClickListener(e-> groupIndotto.setVisibility(View.VISIBLE));
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }

    private void initView() {
        laborRg = findViewById( R.id.labor_radiogroup );
        radioSpontaneo = findViewById( R.id.labor_radio_spontaneo );
        radioIndotto = findViewById( R.id.labor_radio_indotto );
        groupIndotto = findViewById(R.id.labor_group_indotto);
        tolac = findViewById(R.id.labor_switch_tolac);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        cbCRB = findViewById(R.id.labor_indotto_crb);
        cbPropess = findViewById(R.id.labor_indotto_propess);
        cbCytotec = findViewById(R.id.labor_indotto_cytotec);
        cbOxytocin = findViewById(R.id.labor_indotto_oxytocin);
    }

    private void initViewData() {
        groupIndotto.setVisibility(View.GONE);
        if (report == null || !report.getLabor().isPresent()) {
            return;
        }
        final Labor l = report.getLabor().get();
        if (l.getType() == Labor.Type.SPONTANEO) {
            radioSpontaneo.setChecked(true);
        } else if (l.getType() == Labor.Type.INDOTTO) {
            radioIndotto.setChecked(true);
            groupIndotto.setVisibility(View.VISIBLE);
            cbCRB.setChecked(l.getIndottoType().contains(Labor.IndottoType.CRB));
            cbPropess.setChecked(l.getIndottoType().contains(Labor.IndottoType.PROPESS));
            cbCytotec.setChecked(l.getIndottoType().contains(Labor.IndottoType.CYTOTEC));
            cbOxytocin .setChecked(l.getIndottoType().contains(Labor.IndottoType.OSSITOCINA));
        }
        tolac.setChecked(l.isTolac());
    }

    private void onSubmit() {
        final Labor labor = validateData();
        if(labor != null) {
            report.setLabor(labor);
            DataRepository.get().updateReport(report, labor, Labor.NAME);
            this.dismiss();
        }
    }

    private Labor validateData() {
        final Labor labor = new Labor();
        int i = laborRg.getCheckedRadioButtonId();
        if (i == R.id.labor_radio_spontaneo) {
            labor.setType(Labor.Type.SPONTANEO);
            labor.setIndottoType(null);
        } else if (i == R.id.labor_radio_indotto && UtilsView.hasOneTrue(cbCRB, cbCytotec, cbOxytocin, cbPropess)) {
            List<Labor.IndottoType> list = new LinkedList<>();
            if (cbCRB.isChecked())
                list.add(Labor.IndottoType.CRB);
            if (cbCytotec.isChecked())
                list.add(Labor.IndottoType.CYTOTEC);
            if (cbOxytocin.isChecked())
                list.add(Labor.IndottoType.OSSITOCINA);
            if (cbPropess.isChecked())
                list.add(Labor.IndottoType.PROPESS);
            labor.setType(Labor.Type.INDOTTO);
            labor.setIndottoType(list);
        } else {
            showError("Tipologia di travaglio non valida");
            return null;
        }
        labor.setTolac(tolac.isChecked());
        return labor;
    }

}
