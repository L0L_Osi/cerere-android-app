package com.app.cerere.cerereapp.domain.model.event;

import java.io.Serializable;

public interface Event extends Serializable  {


    enum Status {
        COMPLETE, EMPTY, PARTIAL
    }

    Status isComplete();
}