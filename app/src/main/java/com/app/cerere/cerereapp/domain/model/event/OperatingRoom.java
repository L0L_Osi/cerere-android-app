package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

    public class OperatingRoom extends AbstractEvent {

    public enum Operation {
        @SerializedName("TC")
        TC("TC"),

        @SerializedName("Sutura Lacerazione")
        LACERATIONSUTURE("Sutura Lacerazione"),

        @SerializedName("RCU")
        RCU("RCU"),

        @SerializedName("Posizionamento Bakri Balloon")
        BAKRIBALOONPLACEMENT("Posizionamento Bakri Balloon");

        @Getter private final String name;

        Operation(final String name) {this.name = name; }

        @Override
        public String toString() {
            return this.getName();
        }
    }

    public enum Reason {
        @SerializedName("Distocia Meccanica")
        MECHANICALDYSTOCIA("Distocia Meccanica"),

        @SerializedName("Distocia Dinamica")
        DYNAMICDYSTOCIA("Distocia Dinamica"),

        @SerializedName("Alterazioni CTG")
        CTGALTERATION("Alterazioni CTG"),

        @SerializedName("Fallimento Induzione")
        INDUCTIONFAILURE("Fallimento Induzione"),

        @SerializedName("Bradicardia Fetale")
        FETALBRADYCARDIA("Bradicardia Fetale"),

        @SerializedName("Anomalia Placentazione")
        PLACENTATIONANOMALY("Anomalia Placentazione"),

        @SerializedName("Liquido Tinto 3")
        LIQUIDOTINTO3("Liquido Tinto 3"),

        @SerializedName("Altro")
        OTHER("Altro");

        @Getter private final String name;

        Reason(final String name) {this.name = name; }

        @Override
        public String toString() {
            return this.getName();
        }
    }


    public static final String NAME = "operatingRoom";

    @Getter @Setter private Operation operation;
    @Getter @Setter private List<Reason> reason;
    @Getter @Setter private String otherReason;
    @Getter @Setter private String anesthetist; //Operator
    @Getter @Setter private String gynecologist; //Operator
    @Getter @Setter private boolean codeRed;
    @Getter @Setter private Integer lastPeriduralBolusTC;
    @Getter @Setter private boolean fasting;
    @Getter @Setter private List<Anesthetic> anesthetics;
    @Getter @Setter private String anestheticTime;
    @Getter @Setter private String incisionTime;
    @Getter @Setter private boolean effectivePeridural;
    @Getter @Setter private boolean hardIOT;
    @Getter @Setter private Integer bloodloss;
    @Getter @Setter private PostoperativeAnalgesia postoperativeAnalgesia;
    @Getter @Setter private boolean pdphTreatment;

    @Override
    public Status isComplete() {
        return null;
    }

    public String getReasonToString() {
        String s = "";
        for(Reason type: reason) {
            if(type == Reason.OTHER) {
                s = s.concat(otherReason + ", ");
            } else {
                s = s.concat(type.getName() + ", ");
            }
        }
        if(s.length() > 0) {
            return s.substring(0, s.length() - 2);
        } else return s;
    }
}

