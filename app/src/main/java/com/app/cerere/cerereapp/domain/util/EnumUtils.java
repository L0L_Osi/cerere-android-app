package com.app.cerere.cerereapp.domain.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

public class EnumUtils {

    public static<X extends Enum> List<String> enumToStrings(final Class<X> clazz) {
        final List<X> list = Arrays.asList(clazz.getEnumConstants());
        return list.stream().map(Enum::toString).collect(Collectors.toList());
    }

    public static<X extends Enum> X stringToEnum(final Class<X> clazz, final String name) {
        List<Enum> enumValues = new ArrayList<>(EnumSet.allOf(clazz));
        for (Enum e : enumValues) {
            if (e.toString().equals(name))
                return (X) e;
        }
        return null;
    }

    public static<X extends Enum> List<X> getEnumAsList(final Class<X> clazz) {
        return new ArrayList<>(EnumSet.allOf(clazz));
    }

    public static<X extends Enum> List<String> enumListToStringList(final List<X> list) {
        return list.stream()
                .map(Enum::toString)
                .collect(Collectors.toList());
    }

    public static<X extends Enum> String[] enumListToStringArray(final List<X> list) {
        final List<String> strings = enumListToStringList(list);
        final String[] array = new String[strings.size()];
        return strings.toArray(array);
    }
}
