package com.app.cerere.cerereapp.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Recognition;

import java.util.LinkedList;
import java.util.List;

public class AnestheticsRecognitionsDialog extends AnestheticsDialog implements PartialDataListener<Object>{

    private Button btnEpidural;
    private Button btnSubarachnoid;
    private Button btnPieb;
    private TableLayout tableEpidural;
    private TableLayout tableSubarachnoid;
    private TableLayout tablePieb;

    private Button btnSubmit;
    private Button btnCancel;

    public AnestheticsRecognitionsDialog(@NonNull final Context context, final List<Anesthetic> anesthetics, final PartialDataListener<Object> listener) {
        super(context, anesthetics, listener);
    }

    public void setData(final Object obj) {
        if(obj instanceof Anesthetic) {
            Anesthetic a = (Anesthetic) obj;
            anesthetics.add(a);
            switch (a.getVia()) {
                case EPIDURAL: {
                    insertRow(a, tableEpidural);
                    break;
                }
                case SUBARACHNOID: {
                    insertRow(a, tableSubarachnoid);
                    break;
                }
                case PIEB: {
                    insertRow(a, tablePieb);
                    break;
                }
                default:
                    break;
            }
        }
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_anestheticsrecognition, "Anestetici");
        btnEpidural.setOnClickListener(e -> {
            EpiduralDialog dialog = new EpiduralDialog(getContext(), this);
            dialog.show();
        });
        btnSubarachnoid.setOnClickListener(e -> {
            SubArachnoidDialog dialog = new SubArachnoidDialog(getContext(), this);
            dialog.show();
        });
        btnPieb.setOnClickListener(e -> {
            PiebDialog dialog = new PiebDialog(getContext(), this);
            dialog.show();
        });
    }

    @Override
    protected void initViewData() {
        if(anesthetics == null || anesthetics.stream().allMatch(x -> x == null)) {
            return;
        }
        for(Anesthetic a: anesthetics) {
            switch(a.getVia()) {
                case EPIDURAL: {
                    insertRow(a, tableEpidural);
                    break;
                }
                case SUBARACHNOID: {
                    insertRow(a, tableSubarachnoid);
                    break;
                }
                case PIEB: {
                    insertRow(a, tablePieb);
                    break;
                }
                default: break;
            }
        }
    }

    protected void initView() {
        super.initView();

        btnEpidural = findViewById(R.id.an_btn_epidural);
        btnSubarachnoid = findViewById(R.id.an_btn_subarach);
        btnPieb = findViewById(R.id.an_btn_pieb);
        tableEpidural = findViewById(R.id.an_epidural_table);
        tableSubarachnoid = findViewById(R.id.an_subarach_table);
        tablePieb = findViewById(R.id.an_pieb_table);
    }
}
