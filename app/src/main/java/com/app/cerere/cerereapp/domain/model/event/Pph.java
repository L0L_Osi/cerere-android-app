package com.app.cerere.cerereapp.domain.model.event;

import android.support.annotation.NonNull;

import com.app.cerere.cerereapp.domain.types.BirthResult;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Pph extends AbstractEvent {

    public enum PretrasfusionalTest {
        @SerializedName("yes")
        YES("yes"),

        @SerializedName("no")
        NO("no"),

        @SerializedName("bleeding")
        BLEEDING("bleeding");

        @Getter private final String name;

        PretrasfusionalTest(final String name) {
            this.name = name;
        }
    }

    public enum UterotonicTherapy {
        @SerializedName("oxytocin")
        OXYTOCIN("Ossitocina"),

        @SerializedName("ergometrine")
        ERGOMETRINE("Ergometrina"),

        @SerializedName("sulprostone")
        SULPROSTONE("Sulprostone");

        @Getter private final String name;

        UterotonicTherapy(final String name) {
            this.name = name;
        }
    }

    public enum SommConcFibrinogen {
        @SerializedName("none")
        NONE("none"),

        @SerializedName("2g")
        TWOG("2g"),

        @SerializedName("4g")
        FOURG("4g");

        @Getter private final String name;

        SommConcFibrinogen(final String name) {
            this.name = name;
        }
    }

    public enum Hysterectomy {
        @SerializedName("none")
        NONE("none"),

        @SerializedName("total")
        TOTAL("total"),

        @SerializedName("subtotal")
        SUBTOTAL("subtotal");

        @Getter private final String name;

        Hysterectomy(final String name) {
            this.name = name;
        }
    }

    public enum PatientResult {

        @SerializedName ("reparto")
        REPARTO("Reparto"),

        @SerializedName("ti")
        TI("Ti"),

        @SerializedName("deceduto")
        DECEDUTO("Deceduto");

        @Getter
        private final String printName;

        PatientResult(final String printName) {
            this.printName = printName;
        }

        @NonNull
        @Override
        public String toString() {
            return getPrintName();
        }
    }

    public static final String NAME = "pph";

    @Getter @Setter private boolean haemodynamic;
    @Getter @Setter private Integer venousCount;
    @Getter @Setter private Double venousDiameter;
    @Getter @Setter private boolean catheter;
    @Getter @Setter private String crystalloid;
    @Getter @Setter private PretrasfusionalTest pretrasfusional;
    @Getter @Setter private boolean clotting;
    @Getter @Setter private boolean ega;
    @Getter @Setter private boolean rotem;
    @Getter @Setter private boolean emazie;

    @Getter @Setter private List<UterotonicTherapy> uterotonicTherapy;
    @Getter @Setter private boolean sommAcTranexamic;
    @Getter @Setter private boolean repetitionAcTranexamic;
    @Getter @Setter private Integer transEcBags; //if 0, boolean is false
    @Getter @Setter private Integer transPFCBags;
    @Getter @Setter private Integer transPoolPlateletBags;
    @Getter @Setter private boolean transGuidedRotem;
    @Getter @Setter private boolean transFixedPackages;
    @Getter @Setter private SommConcFibrinogen sommConcFibrinogen;
    @Getter @Setter private boolean bakriBalloon;
    @Getter @Setter private boolean conservativeSurgery;
    @Getter @Setter private boolean interventionalRadiology;
    @Getter @Setter private boolean recombinantFactorVII;
    @Getter @Setter private Hysterectomy hysterectomy;

    //PatientOutcome
    @Getter @Setter private PatientResult result;
    @Getter @Setter private String deceaseDate;
    @Getter @Setter private String deceaseTime;

    @Override
    public Status isComplete() {
        return null;
    }

}
