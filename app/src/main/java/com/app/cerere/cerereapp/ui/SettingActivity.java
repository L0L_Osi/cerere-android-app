package com.app.cerere.cerereapp.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.ui.dialogs.SettingDialog;

public class SettingActivity extends AppCompatActivity {

    private static final String TAG = "MSG";

    protected void onCreate(@Nullable final Bundle savedInstanceState, @LayoutRes int layout) {
        super.onCreate(savedInstanceState);
        setContentView(layout);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if(item.getItemId() == R.id.menu_settings) {
            new SettingDialog(this).show();
            return true;
        } else if (item.getItemId() == R.id.enable_export) {
            enableExport();
            return true;
        }else {
            return super.onOptionsItemSelected(item);
        }
    }

    private  boolean enableExport() {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Esportazione già abilitata", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            return false;
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && permissions[0].equals("android.permission.WRITE_EXTERNAL_STORAGE") && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Log.d(TAG,"Permission: "+permissions[0]+ "was "+grantResults[0]);
        }
    }
}
