package com.app.cerere.cerereapp.android.local.JsonUtils;

import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;

public class CustomAnestheticDetailsSerializer implements JsonSerializer<LinkedHashMap<Anesthetic.DrugDetails, Double>> {

    public JsonObject serialize(LinkedHashMap<Anesthetic.DrugDetails, Double> details, Type typeOfSrc, JsonSerializationContext context) {
        try {
            JsonObject temp = new JsonObject();
            if(details != null) {

                for(Anesthetic.DrugDetails key: details.keySet()) {
                    temp.addProperty(key.getSerializedName(), details.get(key));
                }
            }
            return temp;
        } catch(Exception err) {
            err.printStackTrace();
            return null;
        }
    }
}
