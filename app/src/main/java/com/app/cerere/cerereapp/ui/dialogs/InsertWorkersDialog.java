package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.text.InputType;
import android.util.Pair;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.InfoReport.WorkerType;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InsertWorkersDialog extends AbstractDialog {

    private Button btnSubmit;
    private Button btnCancel;
    private TableLayout tableWorkers;
    private Button btnAddWorker;
    private Button btnRemoveWorker;
    private Group groupBtnsTable;

    private final PartialDataListener<Pair<WorkerType, List<Pair<String, String>>>> listener;

    private final List<Pair<String, String>> workers;
    private int workerCount = 0;
    private final List<TableRow> rows = new ArrayList<>();
    private final WorkerType type;

    public InsertWorkersDialog(@NonNull final Context context,
                               final List<Pair<String, String>> workers, WorkerType type,
                               final PartialDataListener<Pair<WorkerType, List<Pair<String, String>>>> listener) {
        super(context);
        this.workers = workers;
        this.listener = listener;
        this.type = type;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        String title = "";
        switch (type) {
            case ANESTETISTA: {
                title = "Anestetisti";
                break;
            }
            case OSTETRICO: {
                title = "Ostetrici";
                break;
            }
            case GINECOLOGO: {
                title = "Ginecologi";
                break;
            }
        }
        super.onCreate(savedInstanceState, R.layout.dialog_insertworkers, title);

        initView();
        initViewData();
        btnAddWorker.setOnClickListener(e -> addWorker());
        btnRemoveWorker.setOnClickListener(e -> removeLastWorker());
        btnSubmit.setOnClickListener(e -> this.onSubmit());
        btnCancel.setOnClickListener(e -> this.dismiss());
    }

    private void initView() {
        tableWorkers = findViewById(R.id.iw_table);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        btnAddWorker =  findViewById(R.id.iw_btn_addworker);
        btnRemoveWorker =  findViewById(R.id.iw_btn_removeworker);
        groupBtnsTable = findViewById(R.id.iw_group_btntable);
    }

    private void initViewData() {
        if (workers == null) {
            return;
        }

        for (int i = 0; i < workers.size(); i++) {
            addWorker();
            TableRow new_row = rows.get(i);
            Pair<String, String> worker = workers.get(i);
            ((EditText)
                    ((TableRow)
                            ((TableLayout) new_row.getChildAt(0)).getChildAt(1))
                            .getChildAt(1))
                    .setText(worker.first);
            ((EditText)
                    ((TableRow)
                            ((TableLayout) new_row.getChildAt(0)).getChildAt(2))
                            .getChildAt(1))
                    .setText(worker.second);
        }
    }

    private void onSubmit() {
        final Pair<WorkerType,List<Pair<String, String>>> result = validateData();
        if (result != null) {
            listener.setData(result);
            this.dismiss();
        }
    }

    private Pair<WorkerType,List<Pair<String, String>>> validateData() {
        final List<Pair<String, String>> result = new LinkedList<>();

        for(TableRow row : rows) {
            String name = UtilsView.getValueAsString((EditText)
                    ((TableRow)
                            ((TableLayout) row.getChildAt(0)).getChildAt(1))
                            .getChildAt(1));
            String time = UtilsView.getValueAsString((EditText)
                    ((TableRow)
                            ((TableLayout) row.getChildAt(0)).getChildAt(2))
                            .getChildAt(1));

            if(name == null) {
                showError("Nome mancante");
                return null;
            } else if(time == null || !DateUtil.validateTime(time)) {
                showError("Orario non valido");
                return null;
            } else {
                result.add(new Pair<>(name, time));
            }
        }
        return new Pair<>(type, result);
    }

    private void addWorker() {
        final Context ctx = getContext();
        final TableRow row = getRowWithParams(ctx, 4);
        final TableLayout inner_layout = getTableLayoutWithParams(ctx);

        final TableRow inner_row1 = getRowWithParams(ctx, 4);
        final TextView title = getTvWithParams(ctx, type.getPrintName() +  " n." + (workerCount + 1), 1);
        title.setTypeface(null, Typeface.BOLD);

        final TableRow inner_row2 = getRowWithParams(ctx, 4);
        final TextView tv1 = getTvWithParams(ctx, "Nome", 1);
        final EditText etworker = getEditTextWithParams(ctx, 3);

        final TableRow inner_row3 = getRowWithParams(ctx, 4);
        final TextView tv2 = getTvWithParams(ctx,"Ora Entrata", 1);
        final EditText ettime = getEditTextWithParams(ctx, 2);
        ettime.setHint("ore:minuti");
        ettime.setInputType(InputType.TYPE_CLASS_DATETIME);
        final Button btn = getButtonWithParams(ctx, "adesso");
        btn.setOnClickListener(e -> {
            ettime.setText(DateUtil.getCurrentTime());
        });

        inner_row1.addView(title);

        inner_row2.addView(tv1);
        inner_row2.addView(etworker);

        inner_row3.addView(tv2);
        inner_row3.addView(ettime);
        inner_row3.addView(btn);

        inner_layout.addView(inner_row1);
        inner_layout.addView(inner_row2);
        inner_layout.addView(inner_row3);

        row.addView(inner_layout);
        rows.add(row);
        tableWorkers.addView(row);
        workerCount++;
    }

    private void removeLastWorker() {
        if(workerCount > 0) {
            tableWorkers.removeView(rows.remove(rows.size() - 1));
            workerCount--;
        }
    }

    private TableRow getRowWithParams(final Context ctx, int weight) {
        final TableRow row = new TableRow(ctx);
        row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, weight));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setPadding(0, 10, 0, 10);
        return row;
    }

    private TextView getTvWithParams(final Context ctx, final String text, int weight) {
        final TextView tv = new TextView(ctx);
        tv.setText(text);
        tv.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, weight));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        return tv;
    }

    private EditText getEditTextWithParams(final Context ctx, int weight) {
        final EditText et = new EditText(ctx);
        et.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.MATCH_PARENT, weight));
        et.setGravity(Gravity.CENTER);
        et.setPadding(5, 0, 5, 0);
        return et;
    }

    private TableLayout getTableLayoutWithParams(final Context ctx) {
        final TableLayout layout = new TableLayout(ctx);
        layout.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 4.0f));
        layout.setPadding(5, 0, 5, 0);
        layout.setMeasureWithLargestChildEnabled(true);
        return layout;
    }

    private Button getButtonWithParams(final Context ctx, final String text) {
        final Button btn = new Button(ctx);
        btn.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
        btn.setText(text);
        btn.setGravity(Gravity.CENTER);
        btn.setPadding(5, 0, 5, 0);
        return btn;
    }

}

