package com.app.cerere.cerereapp.domain.model;

import com.google.gson.annotations.SerializedName;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
public class User {

    @SerializedName("_id")
    @Getter private String id;
    @Getter private String username;
    private String pswd;
    @Getter @Setter private String role;
    @Getter @Setter private String name;
    @Getter @Setter private String surname;

    public User() { }

    public User(final String username, final String pswd) {
        this.username = username;
        this.pswd = this.hashPswd(pswd);
    }

    private String hashPswd(final String pswd) {
        return String.valueOf(pswd.hashCode());
    }
}
