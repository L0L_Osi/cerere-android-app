package com.app.cerere.cerereapp.ui.rooms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.android.SessionManager;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.dialogs.AbstractDialog;
import com.app.cerere.cerereapp.ui.dialogs.PartialDataListener;
import com.app.cerere.cerereapp.ui.main.ReportMainActivity;
import com.app.cerere.cerereapp.ui.util.UtilsView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Optional;

public class OldReportDialog extends  AbstractDialog implements PartialDataListener<Object> {

        private EditText etSdo;
        private Button btnScan;
        private Button btnCancel;
        private Button btnSubmit;

        private RoomsActivity activity;

        private Room currentRoom;

    public OldReportDialog(@NonNull final Context context, final Room room, final RoomsActivity activity) {
        super(context);
        this.currentRoom = room;
        this.activity = activity;
        activity.setListener(this);
    }

        @Override
        protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_old_report, "Nuovo Report");

        initView();
        btnScan.setOnClickListener(e -> this.scanCode());
        btnCancel.setOnClickListener(e -> this.dismiss());
        btnSubmit.setOnClickListener(e -> this.findReport());
    }

        private void onSubmit(Report report) {
        //Resetting Report
        DataRepository.get().deleteReport(report);
        report.setId(null);
        report.setStatus(Report.Status.ACTIVE);
        report.setRoom(currentRoom);
        report.increaseVersion();

        DataRepository.get().createReport(report);
        this.dismiss();
        ReportMainActivity.startActivity(getContext(), currentRoom);
    }

        private void initView() {
        etSdo = findViewById(R.id.old_et_sdo);
        btnScan = findViewById(R.id.old_btn_sdo);
        btnCancel = findViewById(R.id.btn_cancel);
        btnSubmit = findViewById(R.id.btn_search);
    }

    private void findReport() {
        if(UtilsView.isEmpty(etSdo)) {
            showError("SDO non inserito");
        } else {
            DataRepository.get().getReportBySDO(this, UtilsView.getValueAsString(etSdo));
            showError("Caricamento...");
        }
    }

    private void reportFoundHandler(Report report) {
        if (report == null) {
            showError("Nessun Report trovato");
        } else if(report.getStatus() == Report.Status.ACTIVE) {
            showError("Il report è già attivo nella stanza " + report.getRoom().getPrintName());
        } else {
            onSubmit(report);
        }
    }


    private void scanCode() {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setOrientationLocked(true);
        integrator.initiateScan();
    }

    @Override
    public void setData(Object s) {
        if(s != null && s instanceof String) {
            etSdo.setText(s.toString());
        } else if(s == null || s instanceof Report) {
            this.reportFoundHandler((Report) s);
        }
    }
}
