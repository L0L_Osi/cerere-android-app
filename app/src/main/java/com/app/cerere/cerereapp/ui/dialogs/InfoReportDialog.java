package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Pair;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.domain.model.event.InfoReport.WorkerType;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.List;
import java.util.Optional;

public class InfoReportDialog extends AbstractDialog implements PartialDataListener<Pair<WorkerType, List<Pair<String, String>>>>{

    private EditText etDate;
    private EditText etTime;
    private Button btnAnestesists;
    private Button btnObstetrician;
    private Button btnGynecologists;
    private Button btnSubmit;
    private Button btnCancel;
    private Button btnSetDate;
    private Button btnSetTime;
    private Switch switchConsenso;

    private final Report report;
    private List<Pair<String, String>> anestesists;
    private List<Pair<String, String>> obstetricians;
    private List<Pair<String, String>> gynecologists;

    public InfoReportDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_info_report, "Informazioni report");
        initView();
        initViewData();

        btnSetDate.setOnClickListener(e-> etDate.setText(DateUtil.getCurrentDate()));
        btnSetTime.setOnClickListener(e -> etTime.setText(DateUtil.getCurrentTime()));
        btnAnestesists.setOnClickListener(e -> {
            InsertWorkersDialog dialog = new InsertWorkersDialog(getContext(), anestesists, WorkerType.ANESTETISTA, this);
            dialog.show();
        });
        btnGynecologists.setOnClickListener(e -> {
            InsertWorkersDialog dialog = new InsertWorkersDialog(getContext(), gynecologists, WorkerType.GINECOLOGO, this);
            dialog.show();
        });;
        btnObstetrician.setOnClickListener(e -> {
            InsertWorkersDialog dialog = new InsertWorkersDialog(getContext(), obstetricians, WorkerType.OSTETRICO, this);
            dialog.show();
        });;
        btnCancel.setOnClickListener(e -> dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());

    }

    private void initView() {
        etDate = findViewById(R.id.info_et_date);
        etTime = findViewById(R.id.info_et_time);
        btnAnestesists = findViewById(R.id.info_btn_anestesista);
        btnObstetrician = findViewById(R.id.info_btn_obstetrician);
        btnGynecologists = findViewById(R.id.info_btn_gynecologists);
        switchConsenso = findViewById(R.id.info_switch_consenso);
        btnSetDate = findViewById(R.id.info_btn_date);
        btnSetTime = findViewById(R.id.info_btn_time);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
    }

    private void initViewData() {
        if (report == null || !report.getInfo().isPresent()) {
            return;
        }

        final InfoReport i = report.getInfo().get();
        etDate.setText(UtilsView.emptyIfNull(i.getEntranceDate()));
        etTime.setText(UtilsView.emptyIfNull(i.getEntranceTime()));
        switchConsenso.setChecked(i.isConsensus());
        this.anestesists = i.getAnesthetists();
        this.gynecologists = i.getGynecologists();
        this.obstetricians = i.getObstetricians();
    }


    private void onSubmit() {
        final InfoReport rInfo = validateData(report.getInfo());
        if (rInfo != null) {
            report.setInfo(rInfo);
            DataRepository.get().updateReport(report, rInfo, InfoReport.NAME);
            this.dismiss();
        }
    }

    private InfoReport validateData(Optional<InfoReport> old) {


        if (UtilsView.isEmpty(etDate) || !DateUtil.validateDate(UtilsView.getValueAsString(etDate))) {
            showError("Data non valida");
            return null;
        }
        if (UtilsView.isEmpty(etTime) || !DateUtil.validateTime(UtilsView.getValueAsString(etTime))) {
            showError("Orario non valida");
            return null;
        }

        final InfoReport i = new InfoReport();
        i.setEntranceDate(UtilsView.getValueAsString(etDate));
        i.setEntranceTime(UtilsView.getValueAsString(etTime));
        i.setConsensus(switchConsenso.isChecked());
        i.setAnesthetists(this.anestesists);
        i.setObstetricians(this.obstetricians);
        i.setGynecologists(this.gynecologists);

        return i;
    }

    @Override
    public void setData(final Pair<WorkerType, List<Pair<String, String>>> workers) {
        switch (workers.first) {
            case ANESTETISTA: {
                anestesists = workers.second;
                break;
            }
            case OSTETRICO: {
                obstetricians = workers.second;
                break;
            }
            case GINECOLOGO: {
                gynecologists = workers.second;
                break;
            }
        }
    }

}
