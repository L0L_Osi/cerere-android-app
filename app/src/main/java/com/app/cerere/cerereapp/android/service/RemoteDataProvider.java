package com.app.cerere.cerereapp.android.service;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonRequest;
import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.service.customRequests.CustomJsonArrayRequest;
import com.app.cerere.cerereapp.android.service.customRequests.CustomJsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 *  Class that uses the API of the backend to get and send data
 */
public class RemoteDataProvider {

    public static final int RESPONSE_OK = 200;
    public static final int BAD_REQUEST = 400;
    public static final int NOT_FOUND = 404;
    public static final int SERVER_ERROR = 500;

    private static final int REQUEST_TIMEOUT = 5000;
    private static final String API_PATH = "/cerere/v1";

    private RemoteDataProvider() { }

    /**
     * Get all the active reports from the server
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void getRemoteActiveReportsList(Context context, Response.Listener<JSONArray> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/active";
        final CustomJsonArrayRequest request = new CustomJsonArrayRequest(Request.Method.GET, url, null, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Get a report from the server
     * @param reportSdo the sdo of the report
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void getRemoteReportFromSdo(Context context, String reportSdo, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/sdo/" + reportSdo;
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.GET, url, null, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Get a report from the server
     * @param reportId the id of the report
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void getRemoteReportFromId(Context context, String reportId, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/" + reportId;
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.GET, url, null, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }


    /**
     * Get a report's info from the server
     * @param reportId the id of the report
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void getRemoteReportInfo(Context context, String reportId, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/info/" + reportId;
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.GET, url, null, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Update the report on the server
     * @param body the local reports (active and completed)
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void getRemoteReportsUpdated(Context context, JSONObject body, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/check";
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.PUT, url, body, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Update the report on the server
     * @param reportId the id of the report
     * @param body the body of the request
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void putRemoteReportUpdate(Context context, String reportId, JSONObject body, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/update/" + reportId;
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.PUT, url, body, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Create a new report on the server
     * @param body the body of the request
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void createRemoteReport(Context context, JSONObject body, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/add";
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, body, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Delete a report on the server
     * @param reportId the id of the report
     * @param successListener operation to execute if the request succeed
     * @param errorListener operation to execute if the request failed
     */
    public static void deleteRemoteReport(Context context, String reportId, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/reports/delete/" + reportId;
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.DELETE, url, null, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    /**
     * Attempt to log in with user data
     * @param body the JSONObject with "username" and "pswd" keys filled
     * @param successListener
     * @param errorListener
     */
    public static void remoteLogin(Context context, JSONObject body, Response.Listener<JSONObject> successListener, Response.ErrorListener errorListener) {
        final String url = getApiAddress() + "/login";
        final JsonRequest request = new CustomJsonObjectRequest(Request.Method.POST, url, body, successListener, errorListener);
        request.setRetryPolicy(getDefRetryPolicy());
        SmartRequestQueue.getInstance(context).addToRequestQueue(request);
    }

    private static DefaultRetryPolicy getDefRetryPolicy() {
        return new DefaultRetryPolicy(REQUEST_TIMEOUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    }
    
    private static String getApiAddress() {
        return LocalDataCacher.get().getIpAddress(App.get().getApplicationContext()) + API_PATH;
    }
}

