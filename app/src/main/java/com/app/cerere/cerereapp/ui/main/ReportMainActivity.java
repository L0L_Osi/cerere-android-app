package com.app.cerere.cerereapp.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.ui.GenericPagerAdapter;
import com.app.cerere.cerereapp.ui.LogoutActivity;
import com.app.cerere.cerereapp.ui.util.UtilsView;

public class ReportMainActivity extends LogoutActivity {

    private static final String SELECTED_ROOM = "room";

    private Room selectedRoom;

    public static void startActivity(final Context context, final Room room) {
        final Intent i = new Intent(context, ReportMainActivity.class);
        i.putExtra(SELECTED_ROOM, room);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_main);

        final ViewPager mViewPager = findViewById(R.id.container);
        initViewPager(mViewPager);

        if (getIntent() != null) {
            this.selectedRoom = (Room) getIntent().getSerializableExtra(SELECTED_ROOM);
        }
        final TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(selectedRoom != null ? selectedRoom.getCode()-1 : 0);
    }

    private void initViewPager(final ViewPager vp) {
        final GenericPagerAdapter adapter = new GenericPagerAdapter(getSupportFragmentManager());
        if (DataRepository.get().getLocalReportByRoom(Room.ONE) != null) {
            adapter.addFragment(MainContainerFragment.newInstance(Room.ONE),
                                    UtilsView.getStringFromRes(this, R.string.room1));
        }
        if (DataRepository.get().getLocalReportByRoom(Room.TWO) != null) {
            adapter.addFragment(MainContainerFragment.newInstance(Room.TWO),
                                    UtilsView.getStringFromRes(this, R.string.room2));
        }
        if (DataRepository.get().getLocalReportByRoom(Room.THREE) != null) {
            adapter.addFragment(MainContainerFragment.newInstance(Room.THREE),
                                    UtilsView.getStringFromRes(this, R.string.room3));
        }
        if (DataRepository.get().getLocalReportByRoom(Room.FOUR) != null) {
            adapter.addFragment(MainContainerFragment.newInstance(Room.FOUR),
                                    UtilsView.getStringFromRes(this, R.string.room4));
        }
        vp.setAdapter(adapter);
    }
}
