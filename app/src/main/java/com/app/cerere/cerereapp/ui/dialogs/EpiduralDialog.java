package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic.DrugDetails;
import com.app.cerere.cerereapp.domain.types.DrugTipology;
import com.app.cerere.cerereapp.domain.types.Drugs;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedHashMap;


public class EpiduralDialog extends AbstractDialog {

    private EditText etSufentanil;
    private RadioButton radioLocal;
    private RadioButton radioOpioid;
    private EditText etRopivacainaPerc;
    private EditText etRopivacainaMl;
    private EditText etLidocainaPerc;
    private EditText etLidocainaMl;
    private Group groupLocal;
    private Group groupOpioid;
    private Button btnSubmit;
    private Button btnCancel;

    private PartialDataListener<Object> listener;
    private Anesthetic anesthetic;

    public EpiduralDialog(@NonNull final Context context, final PartialDataListener<Object> listener) {
        super(context);
        this.listener = listener;
    }

    public EpiduralDialog(@NonNull final Context context, Anesthetic a, final PartialDataListener<Object> listener) {
        super(context);
        this.listener = listener;
        this.anesthetic = a;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_epidural, "Via epidurale");
        initView();
        initViewData();
        setUpRadioButtons();
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }

    private void setUpRadioButtons() {
        radioLocal.setTag(DrugTipology.LOCAL);
        radioOpioid.setTag(DrugTipology.OPIOID);

        radioLocal.setOnClickListener(e-> {
            groupLocal.setVisibility(View.VISIBLE);
            groupOpioid.setVisibility(View.GONE);
        });
        radioOpioid.setOnClickListener(e-> {
            groupLocal.setVisibility(View.GONE);
            groupOpioid.setVisibility(View.VISIBLE);
        });
    }

    private void onSubmit() {
        anesthetic = validateData();
        if (anesthetic == null)
            return;
        listener.setData(anesthetic);
        this.dismiss();
    }

    private Anesthetic validateData() {
        final Anesthetic a = new Anesthetic();
        boolean result = false;
        if (radioLocal.isChecked())
            result = local(a);
        else if (radioOpioid.isChecked())
            result = opioid(a);
        if (!result) {
            sendError();
            return null;
        }
        a.setVia(DrugsVia.EPIDURAL);
        return a;
    }

    private boolean local(final Anesthetic a) {
        a.setTipology(DrugTipology.LOCAL);
        if (!UtilsView.isEmpty(etRopivacainaPerc) && !UtilsView.isEmpty(etRopivacainaMl) &&
                UtilsView.isEmpty(etLidocainaPerc) && UtilsView.isEmpty(etLidocainaMl)) {
            a.setDrug(Drugs.ROPIVACAINA);
            LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
            details.put(DrugDetails.PERCENT, UtilsView.getValueAsDouble(etRopivacainaPerc));
            details.put(DrugDetails.ML, UtilsView.getValueAsDouble(etRopivacainaMl));
            a.setDetails(details);
            return true;
        } else if (UtilsView.isEmpty(etRopivacainaPerc) && UtilsView.isEmpty(etRopivacainaMl) &&
                !UtilsView.isEmpty(etLidocainaPerc) && !UtilsView.isEmpty(etLidocainaMl)){
            a.setDrug(Drugs.LIDOCAINA);
            LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
            details.put(DrugDetails.PERCENT, UtilsView.getValueAsDouble(etLidocainaPerc));
            details.put(DrugDetails.ML, UtilsView.getValueAsDouble(etLidocainaMl));
            a.setDetails(details);
            return true;
        } else {
            return false;
        }
    }

    private boolean opioid(final Anesthetic a) {
        a.setTipology(DrugTipology.OPIOID);
        if (UtilsView.isEmpty(etSufentanil))
            return false;
        a.setDrug(Drugs.SUFENTANIL);
        LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
        details.put(DrugDetails.MGC, UtilsView.getValueAsDouble(etSufentanil));
        a.setDetails(details);
        return true;
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        etSufentanil = findViewById( R.id.epidural_et_sufentanil);
        radioLocal = findViewById( R.id.epop_radio_local );
        radioOpioid = findViewById( R.id.epop_radio_opioid );
        etRopivacainaPerc = findViewById( R.id.epidural_et_ropivacainaperc);
        etRopivacainaMl = findViewById( R.id.epidural_et_ropivacainaml);
        etLidocainaPerc = findViewById( R.id.epidural_et_lidocainaperc);
        etLidocainaMl = findViewById( R.id.epidural_et_lidocainaml);
        groupLocal = findViewById( R.id.epidural_group_local );
        groupOpioid = findViewById( R.id.epidural_group_opioid );

        groupLocal.setVisibility(View.GONE);
        groupOpioid.setVisibility(View.GONE);
    }

    private void initViewData() {
        if(anesthetic != null) {
            if(anesthetic.getTipology() == DrugTipology.LOCAL) {
                radioLocal.setChecked(true);
                groupLocal.setVisibility(View.VISIBLE);
                switch(anesthetic.getDrug()) {
                    case ROPIVACAINA: {
                        etRopivacainaPerc.setText(UtilsView.emptyIfNull(anesthetic.getDetails().get(DrugDetails.PERCENT)));
                        etRopivacainaMl.setText(UtilsView.emptyIfNull(anesthetic.getDetails().get(DrugDetails.ML)));
                        break;
                    }
                    case LIDOCAINA: {
                        etLidocainaPerc.setText(UtilsView.emptyIfNull(anesthetic.getDetails().get(DrugDetails.PERCENT)));
                        etLidocainaMl.setText(UtilsView.emptyIfNull(anesthetic.getDetails().get(DrugDetails.ML)));
                        break;
                    }
                }
            } else if(anesthetic.getTipology() == DrugTipology.OPIOID) {
                radioOpioid.setChecked(true);
                groupOpioid.setVisibility(View.VISIBLE);
                etSufentanil.setText(UtilsView.emptyIfNull(anesthetic.getDetails().get(DrugDetails.MGC)));
            }
        }
    }

    private void sendError() {
        if (radioLocal.isChecked() &&
                !UtilsView.isEmpty(etRopivacainaPerc) && !UtilsView.isEmpty(etRopivacainaMl) &&
                !UtilsView.isEmpty(etLidocainaPerc) && !UtilsView.isEmpty(etLidocainaMl)) {
            showError("Inserire dati per solo un anestetico!");
        } else {
            showDefaultError();
        }
    }
}
