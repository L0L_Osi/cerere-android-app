package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Baby;
import com.app.cerere.cerereapp.domain.model.event.Birth;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class InsertBirthsDialog extends AbstractDialog implements PartialDataListener<Object> {

    private Button btnSubmit;
    private Button btnCancel;
    private TableLayout tableBirths;
    private Button btnAddBirth;
    private Button btnRemoveBirth;
    private Group groupBtnsTable;

    private final Report report;
    private final List<Birth> births;
    private final List<Baby> babies;
    private int birthCount = 0;
    private final List<TableRow> rows = new ArrayList<>();

    public InsertBirthsDialog(@NonNull final Context context, Report report) {
        super(context);

        this.report = report;
        this.births =  new LinkedList<>();
        if(report.getBirths().isPresent()) births.addAll(report.getBirths().get());
        this.babies = new LinkedList<>();
        if(report.getBabies().isPresent()) babies.addAll(report.getBabies().get());
    }

    public void setData(final Object obj) {
        if(obj instanceof Baby) {
            Baby b = (Baby) obj;
            Optional<Baby> opt = babies.stream().filter(x -> x.getId() == b.getId()).findAny();
            if(opt.isPresent()) {
                babies.remove(opt.get());
            }
            babies.add(b);
            updateRow(b.getId());
        } else if (obj instanceof Birth) {
            Birth b = (Birth) obj;
            Optional<Birth> opt = births.stream().filter(x -> x.getId() == b.getId()).findAny();
            if(opt.isPresent()) {
                births.remove(opt.get());
            }
            births.add(b);
            updateRow(b.getId());
        }
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_births, "Parti effettuati");

        initView();
        initViewData();
        btnAddBirth.setOnClickListener(e -> addBirth());
        btnRemoveBirth.setOnClickListener(e -> removeLastBirth());
        btnSubmit.setOnClickListener(e -> this.onSubmit());
        btnCancel.setOnClickListener(e -> this.dismiss());
    }

    private void initView() {
        tableBirths = findViewById(R.id.b_table);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        btnAddBirth =  findViewById(R.id.b_btn_addbirth);
        btnRemoveBirth =  findViewById(R.id.b_btn_removebirth);
        groupBtnsTable = findViewById(R.id.b_group_btntable);
    }

    private void initViewData() {
        if (births == null || babies == null || (births.isEmpty() && babies.isEmpty())) {
            return;
        }

        for (int i = 0; i < Math.max(births.size(), babies.size()); i++) {
            addBirth();
            updateRow(i);
        }
    }

    private void onSubmit() {
        if ((babies.isEmpty() && births.isEmpty()) ||
            Math.max(babies.size(), births.size()) < rows.size()) {
            showError("Dati Mancanti");
        } else {
            report.setBirths(births);
            DataRepository.get().updateReport(report, births, Birth.NAME);
            report.setBabies(babies);
            DataRepository.get().updateReport(report, babies, Baby.NAME);
            this.dismiss();
        }
    }

    private void addBirth() {
        final Integer index = birthCount;
        final Context ctx = getContext();
        final TableRow row = getRowWithParams(ctx, 4);
        final TableLayout inner_layout = getTableLayoutWithParams(ctx);

        final TableRow inner_title = getRowWithParams(ctx, 4);
        final TextView title = getTvWithParams(ctx, "Parto n." + (++birthCount), 1);
        title.setTypeface(null, Typeface.BOLD);

        final TableRow inner_birth = getRowWithParams(ctx, 4);
        final Button btn1 = getButtonWithParams(ctx, "Parto");
        btn1.setOnClickListener(e -> {
            BirthDialog dialog = new BirthDialog(getContext(), this, index,
                    births.stream().filter(x -> x.getId() == index).findAny());
            dialog.show();
        });
        final TextView tv1 = getTvWithParams(ctx, "Non Compilato", 1);

        final TableRow inner_baby = getRowWithParams(ctx, 4);
        final Button btn2 = getButtonWithParams(ctx, "Neonato");
        btn2.setOnClickListener(e -> {
            BabyDialog dialog = new BabyDialog(getContext(), this, index,
                    babies.stream().filter(x -> x.getId() == index).findAny());
            dialog.show();
        });
        final TextView tv2 = getTvWithParams(ctx,"Non Compilato", 1);

        inner_title.addView(title);

        inner_birth.addView(btn1);
        inner_birth.addView(tv1);

        inner_baby.addView(btn2);
        inner_baby.addView(tv2);

//Insert rows
        inner_layout.addView(inner_title);
        inner_layout.addView(inner_birth);
        inner_layout.addView(inner_baby);

        row.addView(inner_layout);
        rows.add(row);
        tableBirths.addView(row);
    }

    private void updateRow(final Integer index) {
        Optional<Birth> birth = births.stream().filter(x -> x.getId() == index).findAny();
        Optional<Baby> baby = babies.stream().filter(x -> x.getId() == index).findAny();

        if(birth.isPresent() || baby.isPresent()) {
            TableRow new_row = rows.get(index);

            ((TextView)
                    ((TableRow)
                            ((TableLayout) new_row.getChildAt(0)).getChildAt(1))
                            .getChildAt(1))
                    //Implement
                    .setText(birth.isPresent()? birth.get().toString(): "Non Compilato");

            if(baby.isPresent()) {
                ((TextView)
                        ((TableRow)
                                ((TableLayout) new_row.getChildAt(0)).getChildAt(2))
                                .getChildAt(1))
                        //Implement
                        .setText(baby.isPresent()? baby.get().toString(): "Non Compilato");
            }
        }
    }

    private void removeLastBirth() {
        if(birthCount > 0) {
            tableBirths.removeView(rows.remove(rows.size() - 1));
            Optional<Baby> temp1 = babies.stream().max((x, y) -> x.getId() - y.getId());
            Optional<Birth> temp2 = births.stream().max((x, y) -> x.getId() - y.getId());
            if(temp1.isPresent() && temp1.get().getId() == birthCount - 1)
                babies.remove(temp1.get());
            if(temp2.isPresent() && temp2.get().getId() == birthCount - 1) {
                births.remove(temp2.get());
            }
            birthCount--;
        }
    }

    private TableRow getRowWithParams(final Context ctx, int weight) {
        final TableRow row = new TableRow(ctx);
        row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, weight));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setPadding(0, 10, 0, 10);
        return row;
    }

    private TextView getTvWithParams(final Context ctx, final String text, int weight) {
        final TextView tv = new TextView(ctx);
        tv.setText(text);
        tv.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, weight));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER_VERTICAL);
        return tv;
    }

    private TableLayout getTableLayoutWithParams(final Context ctx) {
        final TableLayout layout = new TableLayout(ctx);
        layout.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 4.0f));
        layout.setPadding(5, 0, 5, 0);
        layout.setMeasureWithLargestChildEnabled(true);
        return layout;
    }

    private Button getButtonWithParams(final Context ctx, final String text) {
        final Button btn = new Button(ctx);
        btn.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
        btn.setText(text);
        btn.setGravity(Gravity.CENTER);
        btn.setPadding(5, 0, 5, 0);
        return btn;
    }

}
