package com.app.cerere.cerereapp.android.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.app.cerere.cerereapp.App;

import java.util.Observable;

public class WifiListener extends Observable {

    private static final WifiListener INSTANCE = new WifiListener();
    private WifiManager wifiManager = (WifiManager) App.get().getSystemService(Context.WIFI_SERVICE);

    private WifiListener() {
        initBroadcast();
    }

    public static WifiListener get() {
        return INSTANCE;
    }


    /**
     * @return true if the device is connected to the requested wifi connection, false otherwise.
     * Requested wifi SSID is defined in {@link ServiceConfig}.
     */
    public boolean isConnectedToCerereWifi() {
        //return wifiManager.getConnectionInfo().getSupplicantState() != SupplicantState.DISCONNECTED
         //       && wifiManager.getConnectionInfo().getSSID().equals(ServiceConfig.WIFI_SSID);
        return true;
    }

    /**
     * @return true if the device is connected to a wifi connection, false otherwise.
     */
    public boolean isConnectedToWifi() {
        //return wifiManager.getConnectionInfo().getSupplicantState() != SupplicantState.DISCONNECTED;
        ConnectivityManager cm = (ConnectivityManager) App.get().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void initBroadcast() {
        ConnectivityManager cm = (ConnectivityManager) App.get().getSystemService(Context.CONNECTIVITY_SERVICE);
        cm.registerNetworkCallback(getNetworkRequest(), new ConnectivityManager.NetworkCallback() {

            @Override
            public void onAvailable(final Network network) {
                setChanged();
                notifyObservers();
            }
        });
    }

    private NetworkRequest getNetworkRequest() {
        return new NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build();
    }

}
