package com.app.cerere.cerereapp.android.service;

/**
 * This class contain the config to communicate with the backend
 */
public class ServiceConfig {

    /*
        Put here the service IP address
    */
//    public static final String SERVICE_ADDRESS = "http://10.0.2.2:8080";
    public static final String PORT = "8081";
    public static String DEFAULT_SERVICE_ADDRESS = "192.0.0.1";

    /* pslab test */
//    public static final String SERVICE_ADDRESS = "http://192.168.43.118:8080";

    //set request wifi name here
    public static final String WIFI_SSID = "cerere";

    public static String assembleServiceAddress(String address, String port) {
        return "http://" + address + ":" + port;
    }
}
