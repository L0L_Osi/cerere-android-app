package com.app.cerere.cerereapp.android;

import android.content.Context;
import android.provider.Telephony;
import android.util.Log;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.android.local.JsonUtils.JsonUtils;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.local.LocalDataBase;
import com.app.cerere.cerereapp.android.service.RemoteDataProvider;
import com.app.cerere.cerereapp.android.service.WifiListener;
import com.app.cerere.cerereapp.android.service.WsMessage;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.ui.dialogs.PartialDataListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DataRepository {

    private static final String TAG = "DataRepository";

    private final Context ctx;
    private static final DataRepository INSTANCE = new DataRepository();
    private List<Report> reports = new ArrayList<>();

    private DataRepository() {
        this.ctx = App.get().getApplicationContext();
    }

    public static DataRepository get() { return INSTANCE; }

    /**
     * @param room
     * @return the current report related to this room or null if the room is empty
     */
    public Report getLocalReportByRoom(final Room room) {
        if (reports == null || reports.isEmpty()) {
            return null;
        }
        final Optional<Report> optReport = reports.stream()
                .filter(r -> r.getRoom()!= null && r.getRoom() == room)
                .findAny();
        return optReport.orElse(null);
    }

    /**
     * @param reportId
     * @return the report related to this id or null if the id doesn't exist
     */
    public Report getLocalReportById(final String reportId) {
        if (reportId == null || reports == null || reports.isEmpty()) {
            return null;
        }

        final Optional<Report> optReport = reports.stream()
                .filter(rr -> rr.getId() != null)
                .filter(r -> r.getId().equals(reportId))
                .findAny();
        return optReport.orElse(null);
    }

    public Report getReportBySDO(PartialDataListener<Object> listener, final String reportSDO) {
        RemoteDataProvider.getRemoteReportFromSdo(ctx, reportSDO, s -> {
            final Report report = JsonUtils.jsonStringToObj(s.toString(), Report.class);
            if(report != null) {
                listener.setData(report);
            }
        }, e -> new Thread(() -> listener.setData(getLocalReportBySDO(reportSDO))).start());
        return null;
    }

    public Report getLocalReportBySDO(final String reportSDO) {
        final List<Report> list = LocalDataBase.get().loadReports();
        list.addAll(LocalDataCacher.get().getActiveReportList());
        if (reportSDO == null || list == null || list.isEmpty()) {
            return null;
        }

        final Optional<Report> optReport = list.stream()
                .filter(r -> r.getSdo() != null &&
                        r.getSdo().equals(reportSDO))
                .findAny();
        return optReport.orElse(null);
    }

    /**
     * Create a report on the server and on the local memory if active
     * @param report the report to save
     */
    public void createReport(final Report report) {
        if (report == null) return;

        final JSONObject jsonObj = JsonUtils.objToJSON(report);
        RemoteDataProvider.createRemoteReport(ctx, jsonObj, s -> {
            try {
                final String reportId = s.getString("_id");
                report.setId(reportId);
                putLocalReportInActiveList(report);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, e-> {});

        this.putLocalReportInActiveList(report);
    }

    /**
     * Fetch the report with this id from the server and save it on the local memory
     * @param reportId
     */
    public void reloadReport(final String reportId) {
        RemoteDataProvider.getRemoteReportFromId(ctx, reportId, s -> {
            final Report report = JsonUtils.jsonStringToObj(s.toString(), Report.class);

            if(report.getStatus() == Report.Status.ACTIVE) {
                putLocalReportInActiveList(report);
            } else {
                //lo rimuovo dai reports se è stato completato
                Optional<Report> searchResult = reports.stream()
                        .filter(x -> x.getRoom() == report.getRoom()).findAny();
                if (searchResult.isPresent()) {
                    reports.remove(reports.indexOf(searchResult.get()));
                }
            }
        }, e -> {});
    }

    /**
     * Update a report data locally and on the server
     * @param report the report to update
     * @param objToSend the object modified
     * @param name the container name of the JSONObject where to put the data
     */
    public void updateReport(final Report report, final Object objToSend, final String name) {
        if (report == null) return;
        report.increaseVersion();
        putLocalReportInActiveList(report);
        try {
            final JSONObject jsonObj = JsonUtils.objToJSON(name, objToSend);
            jsonObj.put("version", report.getVersion());
            RemoteDataProvider.putRemoteReportUpdate(ctx, report.getId(), jsonObj, s ->{}, e -> {
                if(e.networkResponse != null && e.networkResponse.statusCode == RemoteDataProvider.NOT_FOUND) {
                    this.createReport(report);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Complete the report related to this room, erase it from the cache
     * @param room
     */
    public void completeReport(final Room room) {
        final Report report = getLocalReportByRoom(room);
        if (report == null) {
            return;
        }

        try {
            report.setStatus(Report.Status.COMPLETED);
            report.increaseVersion();


            final JSONObject jsonObj = JsonUtils.objToJSON("status", Report.Status.COMPLETED.getName());
            jsonObj.put("version", report.getVersion());
            RemoteDataProvider.putRemoteReportUpdate(ctx, report.getId(), jsonObj, s -> {}, e -> {});

            LocalDataBase.get().completeReport(report);
            reports.remove(report);
            LocalDataCacher.get().removeReport(report);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete the report related to this room
     * @param room
     */
    public void deleteReport(final Room room) {
        final Report report = getLocalReportByRoom(room);
        if (report == null) {
            return;
        }
        reports.remove(report);
        RemoteDataProvider.deleteRemoteReport(ctx, report.getId(), success ->{}, error -> {});
        LocalDataCacher.get().removeReport(report);
    }

    /**
     *  Delete the given report
     * @param report
     */
    public void deleteReport(final Report report) {
        if (report == null) {
            return;
        }
        if(reports.contains(report)) {
            reports.remove(report);
            LocalDataCacher.get().removeReport(report);
        } else
            LocalDataBase.get().removeReport(report);
        RemoteDataProvider.deleteRemoteReport(ctx, report.getId(), success ->{}, error -> {});

    }

    /**
     * Init the data at runtime by reading the data on the server or from the local memory if the server
     * is unreachable
     */
    public void initRuntimeData() {
        if (WifiListener.get().isConnectedToWifi()) {
            new Thread(() -> uploadAndGetReports()).start();
        } else {
            //too many threads?
            new Thread(() -> reports = LocalDataCacher.get().getActiveReportList()).start();
        }
    }

    private void uploadAndGetReports() {
        List<Report> locals = this.reports = LocalDataCacher.get().getActiveReportList();
        locals.addAll(LocalDataBase.get().loadReports());
        RemoteDataProvider.getRemoteReportsUpdated(ctx, JsonUtils.objToJSON("reports", locals), s -> {
            try {
                this.reports.clear();
                LocalDataCacher.get().deleteActiveReports();
                Log.d(TAG, "LOAD FROM REMOTE SOURCE");

                if (s.has(WsMessage.WsOperation.CREATE.getPrintName()) &&
                            (s.getJSONArray(WsMessage.WsOperation.CREATE.getPrintName()) != null)) {

                        JSONArray reportsToCreate = s.getJSONArray(WsMessage.WsOperation.CREATE.getPrintName());
                        for(int i = 0; i < reportsToCreate.length(); i++) {
                            Report toCreate = JsonUtils.jsonStringToObj(reportsToCreate.getJSONObject(i).toString(), Report.class);
                            this.createReport(toCreate);
                        }
                }

                if (s.has(WsMessage.WsOperation.UPDATE.getPrintName()) &&
                        (s.getJSONArray(WsMessage.WsOperation.UPDATE.getPrintName()) != null)) {

                    JSONArray reportsToUpdate = s.getJSONArray(WsMessage.WsOperation.UPDATE.getPrintName());
                    for(int i = 0; i < reportsToUpdate.length(); i++) {
                        Report toUpdate = JsonUtils.jsonStringToObj(reportsToUpdate.getJSONObject(i).toString(), Report.class);
                        this.updateReport(toUpdate, toUpdate, "report");
                    }
                }

                if (s.has(WsMessage.WsOperation.NONE.getPrintName()) &&
                        (s.getJSONArray(WsMessage.WsOperation.NONE.getPrintName()) != null)) {

                    JSONArray reportsToNone = s.getJSONArray(WsMessage.WsOperation.NONE.getPrintName());
                    for(int i = 0; i < reportsToNone.length(); i++) {
                        Report toNone = JsonUtils.jsonStringToObj(reportsToNone.getJSONObject(i).toString(), Report.class);
                        this.putLocalReportInActiveList(toNone);
                    }
                }

            LocalDataBase.get().removeReportList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, e -> {
            Log.d(TAG, "LOAD FROM LOCAL SOURCE");
            reports = locals.stream()
                    .filter(x -> x.getStatus() == Report.Status.ACTIVE)
                    .collect(Collectors.toList());
        });
    }

    /**
     * Insert the new report in the active reports list,
     * also it saves te report in the cache
     *
     * @param report the report to save
     */
    private void putLocalReportInActiveList(final Report report) {
        if(report.getStatus() == Report.Status.ACTIVE) {
            Optional<Report> searchResult = reports.stream()
                    .filter(x -> x.getRoom() == report.getRoom()).findAny();
            if (searchResult.isPresent()) {
                reports.set(reports.indexOf(searchResult.get()), report);
            } else {
                reports.add(report);
            }
            LocalDataCacher.get().saveActiveReport(report);
        }
    }
}
