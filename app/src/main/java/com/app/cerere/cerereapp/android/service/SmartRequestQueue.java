package com.app.cerere.cerereapp.android.service;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class SmartRequestQueue implements Observer {

    private static SmartRequestQueue mInstance;
    private com.android.volley.RequestQueue mRequestQueue;
    private Context context;


    private List<Request<?>> offlineQueue = new ArrayList<>();

    private SmartRequestQueue(Context context) {
        this.context = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized SmartRequestQueue getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SmartRequestQueue(context);
        }
        return mInstance;
    }

    public com.android.volley.RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            WifiListener.get().addObserver(this);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        addToOfflineQueue(req);
        if (WifiListener.get().isConnectedToCerereWifi()) {
            executeRequests();
        }
    }

    private <T> void addToOfflineQueue(final Request<T> request) {
        if (offlineQueue.contains(request)) {
            offlineQueue.set(offlineQueue.indexOf(request), request);
        } else {
            offlineQueue.add(request);
        }
    }

    private void executeRequests() {
        for (Request<?> r : offlineQueue) {
            mRequestQueue.add(r);
            offlineQueue.remove(r);
        }
    }

    /**
     * Called when the
     * @param o
     * @param arg
     */
    @Override
    public void update(final Observable o, final Object arg) {
        executeRequests();
    }
}
