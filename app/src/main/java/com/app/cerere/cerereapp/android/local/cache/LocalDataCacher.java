package com.app.cerere.cerereapp.android.local.cache;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.android.local.JsonUtils.JsonUtils;
import com.app.cerere.cerereapp.android.local.LocalFileHandler;
import com.app.cerere.cerereapp.android.service.ServiceConfig;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.User;
import com.app.cerere.cerereapp.domain.types.Room;

import java.util.ArrayList;
import java.util.List;

public class LocalDataCacher {

    private static final String CURRENT_USER = "current_user";
    private static final String CURRENT_IPADDRESS = "ipaddress";
    private static final String ACTIVE_REPORTS_LIST = "activeReports";

    private static final String PREFERENCES = "localData";
    private static final String EMPTY_VALUE = "";

    private final Context context;
    private static final LocalDataCacher INSTANCE = new LocalDataCacher();

    private LocalDataCacher() {
        this.context = App.get().getApplicationContext();
    }

    public static LocalDataCacher get() { return INSTANCE; }


    private SharedPreferences getPreferences(final Context context) {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor(final Context context) {
        return getPreferences(context).edit();
    }


    /**
     * @return the current report list from the cache or an empty list
     */
    public List<Report> getActiveReportList() {
        final List<Report> reports = new ArrayList<>();
        for (Room room : Room.values()) {
            final String raw =  getPreferences(context).getString(room.getName(), EMPTY_VALUE);
            final Report tmpReport = JsonUtils.jsonStringToObj(raw, Report.class);
            if (tmpReport != null){
                reports.add(tmpReport);
            }
        }
        return reports;
    }

    /**
     * @return the current report active in that room, else null
     */
    public Report getActiveReport(final Room room) {
        final String raw = getPreferences(context).getString(room.getName(), EMPTY_VALUE);
        return JsonUtils.jsonStringToObj(raw, Report.class);
    }

    /**
     * @param reports save the report list to the cache
     */
    public void saveActiveReportList(final List<Report> reports) {
        final SharedPreferences.Editor editor = getEditor(context);
        reports.forEach(r -> editor.putString(r.getRoom().getName(), JsonUtils.objToJsonString(r)));
    }

    /**
     * @param report save the report to the cache
     */
    public void saveActiveReport(final Report report) {
        if (report == null || report.getRoom() == null) {
            throw new IllegalArgumentException("Report is null!");
        }
        final String data = JsonUtils.objToJsonString(report);
        getEditor(context).putString(report.getRoom().getName(), data).apply();
    }

    /**
     * @param report remove the report from the cache
     */
    public void removeReport(final Report report) {
        if (getPreferences(context).contains(report.getRoom().getName())) {
            getEditor(context).remove(report.getRoom().getName()).apply();
        }
    }

    /**
     * remove all the reports from the cache
     */
    public void deleteActiveReports() {
        if (getPreferences(context).contains(ACTIVE_REPORTS_LIST)) {
            getEditor(context).remove(ACTIVE_REPORTS_LIST).apply();
        }
    }

    public void saveUser(final Context context, final User user) {
        getEditor(context).putString(CURRENT_USER, JsonUtils.objToJsonString(user)).apply();
    }

    public User getCurrentUser(final Context context) {
        if (getPreferences(context).contains(CURRENT_USER)) {
            final String raw = getPreferences(context).getString(CURRENT_USER, EMPTY_VALUE);
            return JsonUtils.jsonStringToObj(raw, User.class);
        }
        return null;
    }

    public void saveIpAddress(final Context context, final String address) {
        getEditor(context).putString(CURRENT_IPADDRESS, ServiceConfig.assembleServiceAddress(address, ServiceConfig.PORT)).apply();
    }

    public String getIpAddress(final Context context) {
        if (getPreferences(context).contains(CURRENT_IPADDRESS)) {
            return getPreferences(context).getString(CURRENT_IPADDRESS, EMPTY_VALUE);
        } else {
            saveIpAddress(context, ServiceConfig.DEFAULT_SERVICE_ADDRESS);
        }
        return getIpAddress(context);
    }

    public void deleteCurrentUser(final Context context) {
        if (getPreferences(context).contains(CURRENT_USER)) {
            getEditor(context).remove(CURRENT_USER).apply();
        }
    }

    public void clearCache(final Context context) {
        getEditor(context).clear().apply();
    }
}

