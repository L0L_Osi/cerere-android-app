package com.app.cerere.cerereapp.domain.model.event;

import lombok.Getter;
import lombok.Setter;

public class BloodLoss extends AbstractEvent {

    public static final String NAME = "bloodLoss";

    @Getter @Setter private Integer quantity;

    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }

    public static boolean validateQuantity(final Integer quantity) {
        return quantity != null && (quantity >= 0 && quantity <= 3000);
    }

}
