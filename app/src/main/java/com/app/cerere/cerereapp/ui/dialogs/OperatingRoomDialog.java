package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.android.SessionManager;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.OperatingRoom;
import com.app.cerere.cerereapp.domain.model.event.PostoperativeAnalgesia;
import com.app.cerere.cerereapp.domain.model.event.Recognition;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.domain.util.EnumUtils;
import com.app.cerere.cerereapp.ui.component.MySpinner;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class OperatingRoomDialog extends AbstractDialog
        implements PartialDataListener<Object> {

    private MySpinner<OperatingRoom.Operation> spinnerOperation;
    private CheckBox cbMD;
    private CheckBox cbDD;
    private CheckBox cbCA;
    private CheckBox cbIF;
    private CheckBox cbFB;
    private CheckBox cbPA;
    private CheckBox cbLT3;
    private CheckBox cbOther;
    private EditText etOtherReason;

    private EditText etAnesthetist;
    private EditText etGynecologist;

    private Switch switchCodered;
    private Switch switchFasting;
    private EditText etlastPeriduralBolusTC;
    private Button btnLastPeriduralBolusTC;
    private Button btnAnesthetics;
    private EditText etAnestTime;
    private Button btnAnestTime;
    private EditText etIncisTime;
    private Button btnIncisTime;
    private Switch switchEffectivePeridural;
    private Switch switchHardIOT;
    private EditText etBloodLoss;
    private Switch switchPDPH;
    private Button btnPostoperativeAnalgesia;
    private Button btnSubmit;
    private Button btnCancel;

    private PostoperativeAnalgesia tmpPostOperative;
    private List<Anesthetic> anesthetics;

    private final Report report;

    public OperatingRoomDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_operatingroom, "Sala Operatoria");
        initView();
        initViewData();
        btnCancel.setOnClickListener(e -> dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
        btnAnesthetics.setOnClickListener(e -> {
            AnestheticsOperatingRoomDialog dialog = new AnestheticsOperatingRoomDialog(getContext(), anesthetics, this);
            dialog.show();
        });
        btnPostoperativeAnalgesia.setOnClickListener(e -> new PostoperativeAnalgesiaDialog(getContext(), tmpPostOperative, this).show());
    }

    private void initView() {
        spinnerOperation = new MySpinner<>(getView(), R.id.op_operation, EnumUtils.getEnumAsList(OperatingRoom.Operation.class));
        cbMD = findViewById(R.id.op_reason_mechanicaldystocia);
        cbDD = findViewById(R.id.op_reason_dynamicdystocia);
        cbCA = findViewById(R.id.op_reason_ctgalteration);
        cbIF = findViewById(R.id.op_reason_inductionfailure);
        cbFB= findViewById(R.id.op_reason_fetalbradycardia);
        cbPA = findViewById(R.id.op_reason_placentationanomaly);
        cbLT3 = findViewById(R.id.op_reason_liquidotinto3);
        cbOther = findViewById(R.id.op_reason_other);
        cbOther.setOnCheckedChangeListener((e, x) -> etOtherReason.setVisibility(cbOther.isChecked()? View.VISIBLE: View.GONE));
        etOtherReason = findViewById(R.id.op_reason_et);
        etOtherReason.setVisibility(View.GONE);
        etAnesthetist = findViewById(R.id.op_anesthetist);
        etGynecologist = findViewById(R.id.op_gynecologist);

        switchCodered = findViewById(R.id.op_switch_codered);
        switchFasting = findViewById(R.id.op_switch_fasting);
        etlastPeriduralBolusTC = findViewById(R.id.op_interval);
        btnLastPeriduralBolusTC = findViewById(R.id.op_btn_interval);

        btnLastPeriduralBolusTC.setOnClickListener(e -> {
            if(!UtilsView.isEmpty(etAnestTime) &&
                    (report.getRecognitions() != null &&
                        report.getRecognitions().stream()
                            .anyMatch(x -> x.getRecognType() == Recognition.RecognType.A))) {

                Optional<Recognition> temp = report.getRecognitions().stream()
                        .filter(x -> x.getRecognType() == Recognition.RecognType.A)
                        .max((x, y) -> DateUtil.timeDifferenceInMin(x.getDate(), x.getTime(), y.getDate(), y.getTime()));
                if(temp.isPresent()) {
                    int difference = DateUtil.timeDifferenceInMin(
                        DateUtil.getCurrentDate(),
                        UtilsView.getValueAsString(etAnestTime),
                        temp.get().getDate(),
                        temp.get().getTime());
                    if(difference >= 0) {
                        etlastPeriduralBolusTC.setText(Integer.toString(difference));
                        return;
                    }
                } else {
                    Toast.makeText(getContext(), "Errore nel parsing degli orari",Toast.LENGTH_SHORT).show();
                    etlastPeriduralBolusTC.setText("");
                }
            } else {
                Toast.makeText(getContext(), "Orario Ultimo Bolo mancante",Toast.LENGTH_SHORT).show();
                etlastPeriduralBolusTC.setText("");
            }
        });

        btnAnesthetics = findViewById(R.id.op_btn_anesthetics);

        etAnestTime = findViewById(R.id.op_anestTime);
        btnAnestTime = findViewById(R.id.op_anestTime_btn);
        btnAnestTime.setOnClickListener(e -> etAnestTime.setText(DateUtil.getCurrentTime()));

        etIncisTime = findViewById(R.id.op_incisionTime);
        btnIncisTime = findViewById(R.id.op_incisionTime_btn);
        btnIncisTime.setOnClickListener(e -> etIncisTime.setText(DateUtil.getCurrentTime()));

        switchEffectivePeridural = findViewById(R.id.op_switch_effectivePeridural);
        switchHardIOT = findViewById(R.id.op_switch_hardIOT);
        etBloodLoss = findViewById(R.id.op_bloodloss);
        switchPDPH = findViewById(R.id.op_switch_pdph);

        btnPostoperativeAnalgesia = findViewById(R.id.op_btn_postoperativeAnalgesia);

        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
    }

    private void initViewData() {
        if (report == null || !report.getOperatingRoom().isPresent()) {
            etAnesthetist.setText(UtilsView.emptyIfNull(SessionManager.get().getCurrentUser().getUsername()));
            return;
        } else {
            OperatingRoom op = report.getOperatingRoom().get();
            spinnerOperation.setItemSelected(op.getOperation());

            cbMD.setChecked(op.getReason().contains(OperatingRoom.Reason.MECHANICALDYSTOCIA));
            cbDD.setChecked(op.getReason().contains(OperatingRoom.Reason.DYNAMICDYSTOCIA));
            cbCA.setChecked(op.getReason().contains(OperatingRoom.Reason.CTGALTERATION));
            cbIF .setChecked(op.getReason().contains(OperatingRoom.Reason.INDUCTIONFAILURE));
            cbFB.setChecked(op.getReason().contains(OperatingRoom.Reason.FETALBRADYCARDIA));
            cbPA.setChecked(op.getReason().contains(OperatingRoom.Reason.PLACENTATIONANOMALY));
            cbLT3.setChecked(op.getReason().contains(OperatingRoom.Reason.LIQUIDOTINTO3));
            cbOther.setChecked(op.getReason().contains(OperatingRoom.Reason.OTHER));
            if(cbOther.isChecked()) {
                etOtherReason.setText(UtilsView.emptyIfNull(op.getOtherReason()));
                etOtherReason.setVisibility(View.VISIBLE);
            }
            etAnesthetist.setText(UtilsView.emptyIfNull(op.getAnesthetist()));
            etGynecologist.setText(UtilsView.emptyIfNull(op.getGynecologist()));

            switchCodered.setChecked(op.isCodeRed());
            switchFasting.setChecked(op.isFasting());
            etlastPeriduralBolusTC.setText(UtilsView.emptyIfNull(op.getLastPeriduralBolusTC()));

            if(op.getAnesthetics() != null) {
                this.anesthetics = op.getAnesthetics();
            }

            etAnestTime.setText(UtilsView.emptyIfNull(op.getAnestheticTime()));
            etIncisTime.setText(UtilsView.emptyIfNull(op.getIncisionTime()));
            switchEffectivePeridural.setChecked(op.isEffectivePeridural());
            switchHardIOT.setChecked(op.isHardIOT());
            etBloodLoss.setText(UtilsView.emptyIfNull(op.getBloodloss()));
            switchPDPH.setChecked(op.isPdphTreatment());

            tmpPostOperative = op.getPostoperativeAnalgesia();
        }
    }

    private void onSubmit() {
        final OperatingRoom op = validateData();
        if(op != null) {
            report.setOperatingRoom(op);
            DataRepository.get().updateReport(report, op, OperatingRoom.NAME);
            this.dismiss();
        }
    }

    private OperatingRoom validateData() {
        if (!UtilsView.isEmpty(etAnestTime) && !DateUtil.validateTime(UtilsView.getValueAsString(etAnestTime)) ||
                !UtilsView.isEmpty(etIncisTime) && !DateUtil.validateTime(UtilsView.getValueAsString(etIncisTime))) {
            showError("Orario non valido");
            return null;
        } else if(spinnerOperation.getItemSelected() == null) {
            showError("Intervento non selezionato");
            return null;
        } else if((!UtilsView.hasOneTrue(cbCA, cbDD, cbFB, cbIF, cbLT3, cbMD, cbPA, cbOther)) ||
                (cbOther.isChecked() &&
                        UtilsView.isEmpty(etOtherReason))) {
            showError("Motivo non valido");
            return null;
        }

        final OperatingRoom op = new OperatingRoom();
        op.setOperation(spinnerOperation.getItemSelected());

        List<OperatingRoom.Reason> list = new LinkedList<>();

        if (cbMD.isChecked()) {
            list.add(OperatingRoom.Reason.MECHANICALDYSTOCIA);
        }
        if (cbDD.isChecked()) {
            list.add(OperatingRoom.Reason.DYNAMICDYSTOCIA);
        }
        if (cbLT3.isChecked()) {
            list.add(OperatingRoom.Reason.LIQUIDOTINTO3);
        }
        if (cbPA.isChecked()) {
            list.add(OperatingRoom.Reason.PLACENTATIONANOMALY);
        }
        if (cbFB.isChecked()) {
            list.add(OperatingRoom.Reason.FETALBRADYCARDIA);
        }
        if (cbIF.isChecked()) {
            list.add(OperatingRoom.Reason.INDUCTIONFAILURE);
        }
        if (cbCA.isChecked()) {
            list.add(OperatingRoom.Reason.CTGALTERATION);
        }

        if (cbOther.isChecked()) {
            list.add(OperatingRoom.Reason.OTHER);
            op.setOtherReason(UtilsView.getValueAsString(etOtherReason));
        } else {
            op.setOtherReason(null);
        }
        op.setReason(list);
        op.setAnesthetist(UtilsView.getValueAsString(etAnesthetist));
        op.setGynecologist(UtilsView.getValueAsString(etGynecologist));

        op.setCodeRed(switchCodered.isChecked());
        op.setFasting(switchFasting.isChecked());

        op.setLastPeriduralBolusTC(UtilsView.getValueAsInteger(etlastPeriduralBolusTC));

        if(anesthetics == null || anesthetics.isEmpty()) {
            showError("Anestetico non valido");
            return null;
        }
        op.setAnesthetics(anesthetics);

        op.setAnestheticTime(UtilsView.getValueAsString(etAnestTime));
        op.setIncisionTime(UtilsView.getValueAsString(etIncisTime));
        op.setEffectivePeridural(switchEffectivePeridural.isChecked());
        op.setHardIOT(switchHardIOT.isChecked());
        op.setBloodloss(UtilsView.getValueAsInteger(etBloodLoss));
        op.setPdphTreatment(switchPDPH.isChecked());
        op.setPostoperativeAnalgesia(tmpPostOperative);
        return op;
    }


    public void setData(Object obj) {
        if(obj instanceof PostoperativeAnalgesia) {
            tmpPostOperative = (PostoperativeAnalgesia) obj;
        } else if (obj instanceof List) {
            List list = (List) obj;
            if(!list.isEmpty() && list.stream().allMatch(x -> x instanceof  Anesthetic)) {
                anesthetics = (List<Anesthetic>) list;
            }
        }
    }
}
