package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Baby;
import com.app.cerere.cerereapp.domain.model.event.Birth;
import com.app.cerere.cerereapp.domain.types.BirthResult;
import com.app.cerere.cerereapp.domain.types.Genders;
import com.app.cerere.cerereapp.domain.util.EnumUtils;
import com.app.cerere.cerereapp.ui.component.MySpinner;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.Optional;

public class BabyDialog extends AbstractDialog {

    private EditText etWeight;
    private EditText etApgar1;
    private EditText etApgar5;
    private EditText etPh;
    private Switch switchResuscitation;
    private Button btnSubmit;
    private Button btnCancel;

    private final Baby baby;
    private final PartialDataListener<Object> listener;
    private final Integer index;
    private MySpinner<Genders> spinnerGender;
    private MySpinner<BirthResult> spinnerResult;

    public BabyDialog(@NonNull final Context context, PartialDataListener<Object> listener, final Integer index, final Optional<Baby> baby) {
        super(context);
        this.listener = listener;
        this.baby = baby.isPresent()? baby.get(): null;
        this.index = index;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_baby, "Informazioni neonato");

        initView();
        initViewData();
        btnSubmit.setOnClickListener(e -> onSubmit());
        btnCancel.setOnClickListener(e -> dismiss());
    }

    private void initView() {
        switchResuscitation = findViewById(R.id.baby_switch_resuscitation);
        etWeight = findViewById( R.id.baby_weight );
        etApgar1 = findViewById( R.id.baby_apgar_1 );
        etApgar5 = findViewById( R.id.baby_apgar_5 );
        etPh = findViewById( R.id.baby_ph );
        btnSubmit = findViewById( R.id.btn_submit);
        btnCancel = findViewById( R.id.btn_cancel);
        spinnerGender = new MySpinner<>(getView(), R.id.baby_gender, EnumUtils.getEnumAsList(Genders.class));
        spinnerResult = new MySpinner<>(getView(), R.id.baby_result, EnumUtils.getEnumAsList(BirthResult.class));
    }

    private void initViewData() {
        if (baby == null) {
            return;
        }

        spinnerGender.setItemSelected(baby.getGender());
        spinnerResult.setItemSelected(baby.getResult());
        switchResuscitation.setChecked(baby.isResuscitation());
        etWeight.setText(UtilsView.emptyIfNull(baby.getWeight()));
        etApgar1.setText(UtilsView.emptyIfNull(baby.getApgar1()));
        etApgar5.setText(UtilsView.emptyIfNull(baby.getApgar5()));
        etPh.setText(UtilsView.emptyIfNull(baby.getPh()));
    }

    private void onSubmit() {
        final Baby b = validateData();
        if (b != null) {
            b.setId(index);
            listener.setData(b);
            this.dismiss();
        }
    }

    private Baby validateData() {
        if (!UtilsView.isEmpty(etApgar1) && !Baby.validateApgar(UtilsView.getValueAsInteger(etApgar1))) {
            showError("Apgar 1 non valido!");
            return null;
        }
        if (!Baby.validateWeight(UtilsView.getValueAsInteger(etWeight))) {
            showError("Peso non valido!");
            return null;
        }
        if (!UtilsView.isEmpty(etApgar5) && !Baby.validateApgar(UtilsView.getValueAsInteger(etApgar5))) {
            showError("Apgar 5 non valido!");
            return null;
        }
        final Baby b = new Baby();
        b.setApgar1(UtilsView.getValueAsInteger(etApgar1));
        b.setApgar5(UtilsView.getValueAsInteger(etApgar5));
        b.setWeight(UtilsView.getValueAsInteger(etWeight));
        b.setGender(spinnerGender.getItemSelected());
        b.setResult(spinnerResult.getItemSelected());
        b.setResuscitation(switchResuscitation.isChecked());
        b.setPh(UtilsView.getValueAsDouble(etPh));
        return b;
    }
}


