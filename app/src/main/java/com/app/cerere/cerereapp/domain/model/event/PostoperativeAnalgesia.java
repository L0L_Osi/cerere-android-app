package com.app.cerere.cerereapp.domain.model.event;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class PostoperativeAnalgesia implements Serializable {

    public static final String NAME = "postoperativeAnalgesia";

    @Getter @Setter private String protocolType;
    @Getter @Setter private String variation;
    @Getter @Setter private String notes;
}
