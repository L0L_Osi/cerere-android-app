package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic.DrugDetails;
import com.app.cerere.cerereapp.domain.types.Drugs;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedHashMap;

public class AnestheticNotesDialog extends AbstractDialog {

    private EditText etNotes;
    private Button btnSubmit;
    private Button btnCancel;

    private PartialDataListener<Object> listener;
    private DrugsVia via;


    public AnestheticNotesDialog(@NonNull final Context context, final PartialDataListener<Object> listener, DrugsVia via) {
        super(context);
        this.via = via;
        this.listener = listener;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_anesthetic_notes, "Aggiungi Anestetico " + via.getPrintName());
        initView();
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }


    private void onSubmit() {
        final Anesthetic anesthetic = validateData();
        if (anesthetic == null)
            return;
        listener.setData(anesthetic);
        this.dismiss();
    }

    private Anesthetic validateData() {
        final Anesthetic a = new Anesthetic();
        if (!valuesPresent()) {
            showError("Valori mancanti");
            return null;
        } else {
            a.setDrug(Drugs.OTHERS);
            a.setVia(via);
            a.setNotes(UtilsView.getValueAsString(etNotes));
            LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
            a.setDetails(details);
            return a;
        }
    }

    private boolean valuesPresent() {
        return !UtilsView.isEmpty(etNotes);
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        etNotes = findViewById( R.id.an_et_notes);
    }
}

