package com.app.cerere.cerereapp.ui.rooms;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.android.SessionManager;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.dialogs.AbstractDialog;
import com.app.cerere.cerereapp.ui.main.ReportMainActivity;
import com.app.cerere.cerereapp.ui.util.UtilsView;

public class NewReportDialog extends AbstractDialog {

    private EditText etDate;
    private EditText etTime;
    private Button btnTime;
    private Button btnDate;
    private Button btnCancel;
    private Button btnSubmit;

    private Room currentRoom;

    public NewReportDialog(@NonNull final Context context, final Room room) {
        super(context);
        this.currentRoom = room;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_new_report, "Nuovo Report");

        initView();
        btnDate.setOnClickListener(e -> etDate.setText(DateUtil.getCurrentDate()));
        btnTime.setOnClickListener(e -> etTime.setText(DateUtil.getCurrentTime()));
        btnCancel.setOnClickListener(e -> this.dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private Report formatData() {
        if (UtilsView.isEmpty(etDate) || !DateUtil.validateDate(UtilsView.getValueAsString(etDate)) ||
                UtilsView.isEmpty(etTime) || !DateUtil.validateTime(UtilsView.getValueAsString(etTime))) {
            showDefaultError();
            return null;
        }
        if (!SessionManager.get().isLogged()) {
            showError("Errore nella sessione, nessun utente trovato");
            return null;
        }
        final Report r = new Report();
        r.setDate(etDate.getText().toString());
        r.setTime(etTime.getText().toString());
        r.setRoom(currentRoom);
        r.setOperator(SessionManager.get().getCurrentUser().getName());
        return r;
    }

    private void onSubmit() {
        final Report report = formatData();
        if (report == null) {
            return;
        }
        DataRepository.get().createReport(report);
        this.dismiss();
        ReportMainActivity.startActivity(getContext(), currentRoom);
    }

    private void initView() {
        etDate = findViewById(R.id.init_et_date);
        etTime = findViewById(R.id.init_et_time);
        btnTime = findViewById(R.id.init_btn_time);
        btnDate = findViewById(R.id.init_btn_date);
        btnCancel = findViewById(R.id.btn_cancel);
        btnSubmit = findViewById(R.id.btn_submit);
    }
}
