package com.app.cerere.cerereapp.domain.model.event;

import java.util.Arrays;
import java.util.Objects;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode
public abstract class AbstractEvent implements Event {

    @Getter @Setter private String date;
    @Getter @Setter private String time;
    @Getter @Setter private String operator;

    protected Status calculateStatus(final int count, final int valueExpected) {
        if (count == 0) {
            return Status.EMPTY;
        }
        else if (count == valueExpected) {
            return Status.COMPLETE;
        }
        else {
            return Status.PARTIAL;
        }
    }

    protected int countStringValues(final String... strings) {
        return (int) Arrays.asList(strings).stream()
                .filter(Objects::nonNull)
                .filter(s -> !s.isEmpty())
                .count();
    }
}
