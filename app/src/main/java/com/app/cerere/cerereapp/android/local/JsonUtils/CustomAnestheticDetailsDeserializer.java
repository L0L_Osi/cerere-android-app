package com.app.cerere.cerereapp.android.local.JsonUtils;

import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.types.DrugTipology;
import com.app.cerere.cerereapp.domain.types.Drugs;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;

public class CustomAnestheticDetailsDeserializer implements JsonDeserializer<LinkedHashMap<Anesthetic.DrugDetails, Double>> {

    public LinkedHashMap<Anesthetic.DrugDetails, Double> deserialize(JsonElement e, Type typeOfSrc, JsonDeserializationContext context) {
        LinkedHashMap<Anesthetic.DrugDetails, Double> details = new LinkedHashMap<>();
        try {
            JsonObject jsonDetails = e.getAsJsonObject();

            if(jsonDetails != null) {
                for(String key: jsonDetails.keySet()) {
                    details.put(Anesthetic.DrugDetails.valueOf(key.toUpperCase()), jsonDetails.get(key).getAsDouble());
                }
            }
            return details;
        } catch(Exception err) {
            err.printStackTrace();
            return null;
        }
    }

}
