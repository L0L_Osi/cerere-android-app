package com.app.cerere.cerereapp;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.android.SyncService;
import com.app.cerere.cerereapp.android.local.JsonUtils.JsonUtils;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.service.WsMessage;
import com.app.cerere.cerereapp.domain.model.Report;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Optional;

public class App extends Application {

    private static App instance;
    /*
       Variable to toggle the synchronisation of the data with a websocket
    */
    public static final boolean SYNC = true;

    public static App get() { return instance; }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (SYNC) {
            final Intent i = new Intent(this, SyncService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                i.setAction(C.START_FOREGROUND);
                startForegroundService(i);
            } else {
                startService(i);
            }
        }
        DataRepository.get().initRuntimeData();
    }






}
