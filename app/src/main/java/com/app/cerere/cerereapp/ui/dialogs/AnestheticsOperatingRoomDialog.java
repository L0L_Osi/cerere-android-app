package com.app.cerere.cerereapp.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Recognition;
import com.app.cerere.cerereapp.domain.types.DrugsVia;

import java.util.LinkedList;
import java.util.List;

public class AnestheticsOperatingRoomDialog extends AnestheticsDialog implements PartialDataListener<Object>{

    private Button btnEpidural;
    private Button btnGeneral;
    private Button btnSpinal;
    private TableLayout tableEpidural;
    private TableLayout tableGeneral;
    private TableLayout tableSpinal;

    public AnestheticsOperatingRoomDialog(@NonNull final Context context, final List<Anesthetic> anesthetics, final PartialDataListener<Object> listener) {
        super(context, anesthetics, listener);
    }

    public void setData(final Object obj) {
        if(obj instanceof Anesthetic) {
            Anesthetic a = (Anesthetic) obj;
            anesthetics.add(a);
            switch (a.getVia()) {
                case EPIDURAL: {
                    insertRow(a, tableEpidural);
                    break;
                }
                case GENERAL: {
                    insertRow(a, tableGeneral);
                    break;
                }
                case SPINAL: {
                    insertRow(a, tableSpinal);
                    break;
                }
                default:
                    break;
            }
        }
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_anestheticsoperatingroom, "Anestetici");

        btnEpidural.setOnClickListener(e -> {
           EpiduralDialog dialog = new EpiduralDialog(getContext(), this);
           dialog.show();
        });
        btnGeneral.setOnClickListener(e -> {
            AnestheticNotesDialog dialog = new AnestheticNotesDialog(getContext(), this, DrugsVia.GENERAL);
            dialog.show();
        });
        btnSpinal.setOnClickListener(e -> {
            AnestheticNotesDialog dialog = new AnestheticNotesDialog(getContext(), this, DrugsVia.SPINAL);
            dialog.show();
        });
    }

    protected void initViewData() {
        if(anesthetics == null || anesthetics.stream().allMatch(x -> x == null)) {
            return;
        }
        for(Anesthetic a: anesthetics) {
            switch(a.getVia()) {
                case EPIDURAL: {
                    insertRow(a, tableEpidural);
                    break;
                }
                case GENERAL: {
                    insertRow(a, tableGeneral);
                    break;
                }
                case SPINAL: {
                    insertRow(a, tableSpinal);
                    break;
                }
                default: break;
            }
        }
    }

    protected void initView() {
        super.initView();

        btnEpidural = findViewById(R.id.an_btn_epidural);
        btnGeneral = findViewById(R.id.an_btn_general);
        btnSpinal = findViewById(R.id.an_btn_spinal);
        tableEpidural = findViewById(R.id.an_epidural_table);
        tableGeneral = findViewById(R.id.an_general_table);
        tableSpinal = findViewById(R.id.an_spinal_table);
    }
}
