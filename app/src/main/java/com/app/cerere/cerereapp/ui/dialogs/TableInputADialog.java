package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Recognition;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedList;
import java.util.List;

public class TableInputADialog extends AbstractDialog implements PartialDataListener<Object> {

    private Button btnSubmit;
    private Button btnCancel;
    private EditText etTime;
    private Button btnTime;
    private EditText etDate;
    private Button btnDate;
    private Button btnAnesthetics;
    private EditText etFcfoetal;
    private Spinner spinnerCtg;
    private EditText etVasBefore;
    private EditText etVasAfter;
    private EditText etBromage;
    private EditText etFcMaternal;
    private EditText etPaMaternalA;
    private EditText etPaMaternalB;
    private EditText etSpo2;
    private EditText etTemp;
    private Switch switchOxytocin;
    private Switch switchSuspended;
    private EditText etOxytocin;

    private Group oxytocinGroup;

    private Report report;
    private Recognition.CtgType ctgType;
    private Recognition recognition;
    private List<Anesthetic> anesthetics = new LinkedList<>();

    public TableInputADialog(@NonNull final Context context, final Report report, final Recognition recognition) {
        super(context);
        this.report = report;
        this.recognition = recognition;
    }

    public TableInputADialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_table_input_a, "Iniezione Dose di Anestetico");

        initView();
        initViewData();
        btnTime.setOnClickListener(e -> etTime.setText(DateUtil.getCurrentTime()));
        btnDate.setOnClickListener(e -> etDate.setText(DateUtil.getCurrentDate()));
        btnAnesthetics.setOnClickListener(e -> {
            AnestheticsRecognitionsDialog dialog = new AnestheticsRecognitionsDialog(getContext(), anesthetics, this);
            dialog.show();
        });
        btnSubmit.setOnClickListener(e -> onSubmit());
        btnCancel.setOnClickListener(e -> dismiss());
        switchOxytocin.setOnCheckedChangeListener((e, x) ->
                oxytocinGroup.setVisibility(switchOxytocin.isChecked()? View.VISIBLE: View.GONE));
    }

    private void initViewData() {
        spinnerCtg.setOnItemSelectedListener(new CtgListener());
        if (recognition == null) {
            return;
        }
        etTime.setText(UtilsView.emptyIfNull(recognition.getTime()));
        etDate.setText(UtilsView.emptyIfNull(recognition.getDate()));

        etFcfoetal.setText(UtilsView.emptyIfNull(recognition.getFcFoetal()));
        spinnerCtg.setSelection(getCtgSpinnerPosition(recognition.getCtg()));
        if( recognition.getAnesthetics() != null) {
            anesthetics = recognition.getAnesthetics();
        }
        etVasBefore.setText(UtilsView.emptyIfNull(recognition.getVasBefore()));
        etVasAfter.setText(UtilsView.emptyIfNull(recognition.getVasAfter()));
        etBromage.setText(UtilsView.emptyIfNull(recognition.getBromage()));
        etFcMaternal.setText(UtilsView.emptyIfNull(recognition.getFcMaternal()));
        etPaMaternalA.setText(UtilsView.emptyIfNull(recognition.getPaMaternalA()));
        etPaMaternalB.setText(UtilsView.emptyIfNull(recognition.getPaMaternalB()));
        etSpo2.setText(UtilsView.emptyIfNull(recognition.getSpO2Maternal()));
        etTemp.setText(UtilsView.emptyIfNull(recognition.getTemp()));
        if(recognition.getOxytocin()) {
            oxytocinGroup.setVisibility(View.VISIBLE);
            switchSuspended.setChecked(recognition.getOxytocinSuspended().booleanValue());
        }
        switchOxytocin.setChecked(recognition.getOxytocin().booleanValue());
        etOxytocin.setText(UtilsView.emptyIfNull(recognition.getOxytocinSpeed()));
    }

    private void onSubmit() {
        final Recognition r = validateData();
        if (r == null) {
            return;
        }
        if (recognition != null) {
            //need to handle date differently?
            //need to handle booleans differently
            editColumn(report.getRecognitions(), r);
            r.setDate(this.recognition.getDate());
        } else {
            addNewColumn(report.getRecognitions(), r);
            r.setDate(DateUtil.getCurrentDate());
        }
        DataRepository.get().updateReport(report, r, Recognition.NAME);
        this.dismiss();
    }

    private void addNewColumn(final List<Recognition> list, final Recognition r) {
        list.add(r);
    }

    private void editColumn(final List<Recognition> list, final Recognition r) {
        list.set(list.indexOf(recognition), r);
    }

    private Recognition validateData() {
        final Recognition r = new Recognition();
        if (UtilsView.isEmpty(etTime)) {
            showError("Ora mancante");
            return null;
        } else if (!DateUtil.validateTime(UtilsView.getValueAsString(etTime))) {
            showError("Formato dell'ora errato");
            return null;
        } else if (UtilsView.isEmpty(etDate)) {
            showError("Data mancante");
            return null;
        } else if (!DateUtil.validateDate(UtilsView.getValueAsString(etDate))) {
            showError("Formato della data errato");
            return null;
        }
        if (!UtilsView.isEmpty(etPaMaternalA) && UtilsView.isEmpty(etPaMaternalB) ||
                UtilsView.isEmpty(etPaMaternalA) && !UtilsView.isEmpty(etPaMaternalB)
        ) {
            showError("PA materna non valida");
            return null;
        }
        if (switchOxytocin.isChecked() && UtilsView.isEmpty(etOxytocin) && !switchSuspended.isChecked()) {
            showError("Inserire velocità ossitocina");
            return null;
        } else {
            r.setOxytocinSpeed(UtilsView.getValueAsInteger(etOxytocin));
        }

        if (!UtilsView.isEmpty(etVasBefore) && !Recognition.validateVas(UtilsView.getValueAsInteger(etVasBefore))) {
            showError("VAS prima non valido");
            return null;
        }
        if (!UtilsView.isEmpty(etVasAfter) && !Recognition.validateVas(UtilsView.getValueAsInteger(etVasAfter))) {
            showError("VAS dopo non valido");
            return null;
        }
        if (!UtilsView.isEmpty(etBromage) && !Recognition.validateBromage(UtilsView.getValueAsInteger(etBromage))) {
            showError("Bromage non valido");
            return null;
        }
        if (!UtilsView.isEmpty(etTemp) &&  !Recognition.validateTemperature(UtilsView.getValueAsDouble(etTemp))) {
            showError("Temperatura non valida");
            return null;
        }
        r.setTemp(UtilsView.getValueAsDouble(etTemp));
        r.setPaMaternalA(UtilsView.getValueAsInteger(etPaMaternalA));
        r.setPaMaternalB(UtilsView.getValueAsInteger(etPaMaternalB));
        r.setBromage(UtilsView.getValueAsInteger(etBromage));
        r.setVasBefore(UtilsView.getValueAsInteger(etVasBefore));
        r.setVasAfter(UtilsView.getValueAsInteger(etVasAfter));
        r.setTime(UtilsView.getValueAsString(etTime));
        r.setDate(UtilsView.getValueAsString(etDate));
        r.setCtg(ctgType);
        r.setFcMaternal(UtilsView.getValueAsInteger(etFcMaternal));
        r.setFcFoetal(UtilsView.getValueAsInteger(etFcfoetal));
        r.setSpO2Maternal(UtilsView.getValueAsInteger(etSpo2));
        r.setOxytocin(switchOxytocin.isChecked());
        if(switchOxytocin.isChecked()) {
            r.setOxytocinSuspended(switchSuspended.isChecked());
            r.setOxytocinSpeed(UtilsView.getValueAsInteger(etOxytocin));
        }
        final Integer position = recognition == null ? report.getRecognitions().size() :
                report.getRecognitions().indexOf(recognition);
        r.setPosition(position);
        r.setRecognType(Recognition.RecognType.A);
        r.setAnesthetics(anesthetics);
        return r;
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        etDate = findViewById(R.id.table_et_date);
        btnDate = findViewById(R.id.table_btn_date);
        etTime = findViewById(R.id.table_et_time);
        btnTime = findViewById(R.id.table_btn_time);
        etFcfoetal = findViewById(R.id.table_et_fcfetal);
        spinnerCtg = findViewById(R.id.table_ctg);
        btnAnesthetics = findViewById(R.id.table_btn_anesthetics);
        etVasBefore = findViewById(R.id.table_et_vasBefore);
        etVasAfter = findViewById(R.id.table_et_vasAfter);
        etBromage = findViewById(R.id.table_et_bromage);
        etFcMaternal = findViewById(R.id.table_et_fcmaternal);
        etPaMaternalA = findViewById(R.id.table_et_pamaternala);
        etPaMaternalB = findViewById(R.id.table_et_pamaternalb);
        etSpo2 = findViewById(R.id.table_et_spo2);
        etTemp = findViewById(R.id.table_et_temp);
        switchOxytocin = findViewById(R.id.table_switch_oxytocin);
        switchSuspended = findViewById(R.id.table_switch_oxytocin_suspended);
        etOxytocin = findViewById(R.id.table_et_oxytocin);
        initSpinner(spinnerCtg, R.array.ctg_types);

        oxytocinGroup = findViewById(R.id.table_group_oxitocin);
        oxytocinGroup.setVisibility(View.GONE);
    }

    private int getCtgSpinnerPosition(final Recognition.CtgType ctgType) {
        if (ctgType == Recognition.CtgType.CATI) return 1;
        else if (ctgType == Recognition.CtgType.CATII) return 2;
        else if (ctgType == Recognition.CtgType.CATIII) return 3;
        else return 0;
    }

    @Override
    public void setData(final Object obj) {
        if (obj instanceof List) {
            List list = (List) obj;
            if (!list.isEmpty() && list.stream().allMatch(x -> x instanceof Anesthetic)) {
                this.anesthetics = (List<Anesthetic>) list;
            }
        }
    }

    /**
     * Listener for the CTG spinner input
     */
    private class CtgListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
            if (position == 0) ctgType = null;
            else if (position == 1) ctgType = Recognition.CtgType.CATI;
            else if (position == 2) ctgType = Recognition.CtgType.CATII;
            else if (position == 3) ctgType = Recognition.CtgType.CATIII;
        }

        @Override
        public void onNothingSelected(final AdapterView<?> parent) {
            ctgType = null;
        }
    }
}
