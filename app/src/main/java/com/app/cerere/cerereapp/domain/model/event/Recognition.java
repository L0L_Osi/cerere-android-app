package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Recognition extends AbstractEvent {

    public static final String NAME = "recognitions";

    public enum NeckType {

        @SerializedName("raccorciato")
        RACCORCIATO("raccorciato"),

        @SerializedName("appianato")
        APPIANATO("appianato"),

        @SerializedName("scomparso")
        SCOMPARSO("scomparso");

        @Getter private final String name;

        NeckType(final String name) {
            this.name = name;
        }
    }

    public enum CtgType {
        @SerializedName("catI")
        CATI("categoria I"),
        @SerializedName("catII")
        CATII("categoria II"),
        @SerializedName("catIII")
        CATIII("categoria III");

        @Getter private final String name;

        CtgType(final String name) {
            this.name = name;
        }
    }

    public enum RecognType {
        @SerializedName("VO")
        VO("Visita Ostetrica"),
        @SerializedName("A")
        A("Anestetico");

        @Getter private final String name;

        RecognType(final String name) {
            this.name = name;
        }
    }

    @Getter @Setter private RecognType recognType;

    //Visita Ostetrica
    @Getter @Setter private Integer position;
    @Getter @Setter private NeckType neck;
    @Getter @Setter private Integer dilation;

    //Anestetico
    @Getter @Setter private Integer lvHead;
    @Getter @Setter private Integer fcFoetal;
    @Getter @Setter private CtgType ctg;
    @Getter @Setter private List<Anesthetic> anesthetics;
    @Getter @Setter private Integer vasBefore;
    @Getter @Setter private Integer vasAfter;
    @Getter @Setter private Integer bromage;
    @Getter @Setter private Integer fcMaternal;
    @Getter @Setter private Integer paMaternalA;
    @Getter @Setter private Integer paMaternalB;
    @Getter @Setter private Integer spO2Maternal;
    @Getter @Setter private Double temp;
    @Getter @Setter private Boolean oxytocin;
    @Getter @Setter private Boolean oxytocinSuspended;
    @Getter @Setter private Integer oxytocinSpeed;

    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }

    public static boolean validateDilation(final Integer dilation) {
        return dilation != null && (dilation >= 1 && dilation <= 10);
    }

    public static boolean validateLvHead(final Integer lvHead) {
        return lvHead != null && (lvHead >= -3 && lvHead <= 3);
    }

    public static boolean validateVas(final Integer vas) {
        return vas != null && (vas >= 0 && vas <= 10);
    }

    public static boolean validateBromage(final Integer bromage) {
        return bromage != null && (bromage >= 0 && bromage <= 3);
    }

    public static boolean validateTemperature(final Double temp) {
        return temp != null && ( temp >= 0 && temp <= 100);
    }
}
