package com.app.cerere.cerereapp.ui.login;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.SessionManager;
import com.app.cerere.cerereapp.domain.model.User;
import com.app.cerere.cerereapp.ui.SettingActivity;
import com.app.cerere.cerereapp.ui.rooms.RoomsActivity;

public class LoginActivity extends SettingActivity {

    private Button btnLogin;
    private EditText etName;
    private EditText etPswd;


    public static void startActivity(final Context context) {
        final Intent i  = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_login);

        if (SessionManager.get().isLogged()) {
            showRooms();
        }

        initView();
        btnLogin.setOnClickListener(e -> fakeLogin());
    }

    private void initView() {
        etName = findViewById(R.id.login_et_name);
        etPswd = findViewById(R.id.login_et_pswd);
        btnLogin = findViewById(R.id.login_button);
    }

    /*
        Test only
     */
    private void fakeLogin() {
        final User user = formatData();
        if (user.getUsername().equals("fakelogin")) {
            user.setName("admin");
            SessionManager.get().fakeLogIn(this, user, () -> this.showRooms());
        }
        else {
            login();
        }
    }

    private void login() {
        final User user = formatData();
        SessionManager.get().logIn(this, user,
                () -> this.showRooms(),
                () -> Toast.makeText(this, "Dati non validi", Toast.LENGTH_SHORT).show());
    }

    private void showRooms() {
        finish();
        RoomsActivity.startActivity(this);
    }

    private User formatData() {
        final String name = etName.getText().toString();
        final String pswd = etPswd.getText().toString();
        return new User(name, pswd);
    }
}
