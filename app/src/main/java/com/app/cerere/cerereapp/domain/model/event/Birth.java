package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Birth extends AbstractEvent {

    public static final String NAME = "births";

    public enum Type {
        @SerializedName("spontaneo")
        SPONTANEO("Spontaneo"),

        @SerializedName("ventosa")
        VENTOSA("Ventosa"),

        @SerializedName("kristeller")
        KRISTELLER("Kristeller"),

        @SerializedName("TC")
        TC("TC");

        @Getter private final String printName;

        Type(final String printName) {
            this.printName = printName;
        }

        @Override
        public String toString() {
            return getPrintName();
        }
    }

    @Getter @Setter private List<Type> type = new ArrayList<>();
    @Getter @Setter private boolean episiotomia;
    @Getter @Setter private String note;
    @Getter @Setter private Integer id;

    @Override
    public Status isComplete() {
        return Status.EMPTY;
    }

    public String toString() {
        String s = "";
        for(Birth.Type type: type) {
            s = s.concat(type.getPrintName() + ", ");
        }
        if(s.length() > 0) {
            return s.substring(0, s.length() - 2);
        } else return s;
    }
}
