package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.LaborComplications;
import com.app.cerere.cerereapp.domain.model.event.ThirdStage;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedList;
import java.util.List;

public class LaborComplicationsDialog extends AbstractDialog {

    private final Report report;

    private RadioButton radioNone;
    private CheckBox cbDuralPuncture;
    private CheckBox cbLateralizedCatheter;
    private CheckBox cbVesselCannulation;
    private CheckBox cbInefficientCatheter;
    private CheckBox cbRepositioning;
    private CheckBox cbOther;
    private EditText etOtherType;
    private Button btnSubmit;
    private Button btnCancel;

    public LaborComplicationsDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_labor_complications, "Complicanze");
        initView();
        initViewData();


        radioNone.setOnClickListener(e -> onExclusiveRadioClick());
        cbDuralPuncture.setOnClickListener(e -> onChechBoxClick());
        cbLateralizedCatheter.setOnClickListener(e -> onChechBoxClick());
        cbVesselCannulation.setOnClickListener(e -> onChechBoxClick());
        cbInefficientCatheter.setOnClickListener(e -> onChechBoxClick());
        cbRepositioning.setOnClickListener(e -> onChechBoxClick());
        cbOther.setOnClickListener(e -> onChechBoxClick());
        btnCancel.setOnClickListener(e -> this.dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);

        radioNone = findViewById(R.id.lc_none_rb);
        cbDuralPuncture = findViewById(R.id.lc_duralpuncture_checkbox);
        cbLateralizedCatheter = findViewById(R.id.lc_lateralizedcatheter_checkbox);
        cbVesselCannulation = findViewById(R.id.lc_vesselcannulation_checkbox);
        cbInefficientCatheter = findViewById(R.id.lc_inefficientcatheter_checkbox);
        cbRepositioning = findViewById(R.id.lc_repositioning_checkbox);
        cbOther = findViewById(R.id.lc_other_checkbox);

        cbOther.setOnCheckedChangeListener((e, x) ->
            etOtherType.setVisibility(cbOther.isChecked()? View.VISIBLE: View.GONE));
        etOtherType = findViewById(R.id.lc_other_et);

        etOtherType.setVisibility(View.GONE);
    }

    private void onExclusiveRadioClick() {
        cbOther.setChecked(false);
        cbRepositioning.setChecked(false);
        cbInefficientCatheter.setChecked(false);
        cbVesselCannulation.setChecked(false);
        cbLateralizedCatheter.setChecked(false);
        cbDuralPuncture.setChecked(false);

        UtilsView.setStrikeTextFlags(cbOther, cbRepositioning, cbInefficientCatheter, cbVesselCannulation, cbLateralizedCatheter, cbDuralPuncture);
        UtilsView.setNotStrikeTextFlags(radioNone);
    }

    private void onChechBoxClick() {
        radioNone.setChecked(false);
        UtilsView.setStrikeTextFlags(radioNone);
        UtilsView.setNotStrikeTextFlags(cbOther, cbRepositioning, cbInefficientCatheter, cbVesselCannulation, cbLateralizedCatheter, cbDuralPuncture);
    }

    private void initViewData() {
        if (report.getLaborComplications().isPresent()) {
            LaborComplications lc = report.getLaborComplications().get();
            if(lc.getType().contains(LaborComplications.Type.NONE))
                radioNone.setChecked(true);
            else {
                cbDuralPuncture.setChecked(lc.getType().contains(LaborComplications.Type.DURAL_PUNCTURE));
                cbLateralizedCatheter.setChecked(lc.getType().contains(LaborComplications.Type.LATERALIZED_CATHETER));
                cbVesselCannulation.setChecked(lc.getType().contains(LaborComplications.Type.VESSEL_CANNULATION));
                cbInefficientCatheter.setChecked(lc.getType().contains(LaborComplications.Type.INEFFICIENT_CATHETER));
                cbRepositioning.setChecked(lc.getType().contains(LaborComplications.Type.REPOSITIONING));
                if (lc.getType().contains(LaborComplications.Type.OTHER)) {
                    cbOther.setChecked(true);
                    etOtherType.setVisibility(View.VISIBLE);
                    etOtherType.setText(lc.getOther());
                } else {
                    etOtherType.setVisibility(View.GONE);
                }
            }
        }
    }

    private void onSubmit() {
        final LaborComplications lc = validateData();
        if(lc != null) {
            report.setLaborComplications(lc);
            DataRepository.get().updateReport(report, lc, LaborComplications.NAME);
            this.dismiss();
        }
    }

    private LaborComplications validateData() {
        if(!(radioNone.isChecked() || cbDuralPuncture.isChecked() || cbLateralizedCatheter.isChecked() || cbVesselCannulation.isChecked() ||
                cbInefficientCatheter.isChecked() || cbRepositioning.isChecked() || cbOther.isChecked())) {
            showError("Nessuna complicanza specificata");
            return null;
        } else if(cbOther.isChecked() && UtilsView.isEmpty(etOtherType)) {
            showError("Campo 'Altro' vuoto");
            return null;
        } else {
            LaborComplications lc = new LaborComplications();

            List<LaborComplications.Type> list = new LinkedList<>();
            if(radioNone.isChecked())
                list.add(LaborComplications.Type.NONE);
            if(cbDuralPuncture.isChecked())
                list.add(LaborComplications.Type.DURAL_PUNCTURE);
            if(cbLateralizedCatheter.isChecked())
                list.add(LaborComplications.Type.LATERALIZED_CATHETER);
            if(cbVesselCannulation.isChecked())
                list.add(LaborComplications.Type.VESSEL_CANNULATION);
            if(cbInefficientCatheter.isChecked())
                list.add(LaborComplications.Type.INEFFICIENT_CATHETER);
            if(cbRepositioning.isChecked())
                list.add(LaborComplications.Type.REPOSITIONING);
            if(cbOther.isChecked()) {
                list.add(LaborComplications.Type.OTHER);
                lc.setOther(UtilsView.getValueAsString(etOtherType));
            }

            lc.setType(list);
            return lc;
        }
    }
}
