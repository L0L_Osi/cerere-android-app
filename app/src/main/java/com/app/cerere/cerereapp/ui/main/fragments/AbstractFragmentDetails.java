package com.app.cerere.cerereapp.ui.main.fragments;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.app.cerere.cerereapp.ui.DataChangeListener;
import com.app.cerere.cerereapp.ui.dialogs.AbstractDialog;


public abstract class AbstractFragmentDetails extends Fragment implements DataChangeListener {

    protected static final String SELECTED_REPORT = "selectedReport";


    protected <X extends AbstractFragmentDetails, Y extends AbstractDialog> void showDialogAndReload(final X x, final Y y) {
        y.setOnDismissListener(d -> x.reloadData());
        y.show();
    }

    protected void showErrorMissingReport() {
        Toast.makeText(getContext(), "Report non trovato, errore nei dati", Toast.LENGTH_LONG).show();
    }

    protected void reloadParentData() {
        final Fragment parent = getParentFragment();
        parent.getFragmentManager().beginTransaction().detach(parent).attach(parent).commit();
    }
}
