package com.app.cerere.cerereapp.domain.model.event;

import lombok.Getter;
import lombok.Setter;

public class ThirdStage extends AbstractEvent {

    public static final String NAME = "thirdStage";
    @Getter @Setter private boolean therapy;

    @Override
    public Status isComplete() {
        return null;
    }
}
