package com.app.cerere.cerereapp.ui.main.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.ui.dialogs.InfoReportDialog;
import com.app.cerere.cerereapp.ui.dialogs.PatientDialog;
import com.app.cerere.cerereapp.ui.util.UtilsView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class FragmentDetailsInfo extends AbstractFragmentDetails {

    private EditText etSdo;
    private Button btnInfo;
    private Button btnPatient;
    private Button btnScan;
    private Report report;

    public static FragmentDetailsInfo newInstance(final Report report) {
        final FragmentDetailsInfo fragment = new FragmentDetailsInfo();
        final Bundle b = new Bundle();
        b.putSerializable(SELECTED_REPORT, report);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle b = getArguments();
        if (b == null || !b.containsKey(SELECTED_REPORT)) {
            return;
        }
        this.report = (Report) b.getSerializable(SELECTED_REPORT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v =  inflater.inflate(R.layout.fragment_details_info, container, false);
        initView(v);
        btnInfo.setOnClickListener(e -> showDialogAndReload(this, new InfoReportDialog(getContext(), report)));
        btnPatient.setOnClickListener(e -> showDialogAndReload(this, new PatientDialog(getContext(), report)));
        btnScan.setOnClickListener(e -> this.scanCode());
        return v;
    }

    @Override
    public void reloadData() {
        reloadParentData();
    }

    private void initView(final View v) {
        etSdo = v.findViewById(R.id.f_info_et_sdo);
        btnInfo = v.findViewById( R.id.f_info_btn_info );
        btnPatient = v.findViewById( R.id.f_info_btn_patient );
        btnScan = v.findViewById(R.id.f_info_btn_sdo);

        if (report.getSdo() != null) {
            etSdo.setText(report.getSdo());
        }

        etSdo.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (s != null && (report.getSdo() == null || !report.getSdo().equals(s))) {
                    report.setSdo(s.toString());
                    DataRepository.get().updateReport(report, s.toString(), "sdo");
                }
            }
        });
    }

    private void scanCode() {
        IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
        integrator.setOrientationLocked(true);
        integrator.initiateScan();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(getContext(), "Codice non trovato!", Toast.LENGTH_LONG).show();
            } else {
                Log.d("SCAN BAR CODE RESULT", result.getContents());
                etSdo.setText(result.getContents());
            }
        }
    }
}

