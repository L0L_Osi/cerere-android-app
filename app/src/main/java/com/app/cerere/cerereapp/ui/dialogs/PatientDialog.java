package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Patient;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PatientDialog extends AbstractDialog {

    private EditText etName;
    private EditText etAge;
    private EditText patientEtGestAge;
    private EditText patientEtWeightPre;
    private EditText patientEtWeightCurr;
    private EditText patientEtHeight;
    private Button btnSubmit;
    private Button btnCancel;
    private Switch switchPMA;
    private RadioButton radioPrimipara;
    private RadioButton radioPluripara;
    private RadioButton radioNullipara;
    private TableLayout tableBirths;
    private Button btnAddBirth;
    private Button btnRemoveBirth;
    private Group groupBtnsTable;

    private final Report report;
    private int birthCount = 0;
    private final List<TableRow> rows = new ArrayList<>();

    public PatientDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_patient, "Informazioni paziente");

        initView();
        initViewData();
        radioNullipara.setOnClickListener(e -> {
            tableBirths.setVisibility(View.GONE);
            groupBtnsTable.setVisibility(View.GONE);
        });
        radioPrimipara.setOnClickListener(e -> {
            addFirstBirth();
            tableBirths.setVisibility(View.VISIBLE);
            groupBtnsTable.setVisibility(View.GONE);
        });
        radioPluripara.setOnClickListener(e -> {
            addFirstBirth();
            tableBirths.setVisibility(View.VISIBLE);
            groupBtnsTable.setVisibility(View.VISIBLE);
        });
        btnAddBirth.setOnClickListener(e -> addBirth());
        btnRemoveBirth.setOnClickListener(e -> removeLastBirth());
        btnSubmit.setOnClickListener(e -> this.onSubmit());
        btnCancel.setOnClickListener(e -> this.dismiss());
    }

    private void initView() {
        etName = findViewById( R.id.patient_et_name );
        etAge = findViewById( R.id.patient_et_age );
        patientEtGestAge = findViewById( R.id.patient_et_gestAge );
        patientEtWeightPre = findViewById( R.id.patient_et_weightPre );
        patientEtWeightCurr = findViewById( R.id.patient_et_weightCurr );
        patientEtHeight = findViewById( R.id.patient_et_height );
        radioPrimipara = findViewById( R.id.patient_radio_primipara);
        radioPluripara = findViewById( R.id.patient_radio_pluripara);
        radioNullipara = findViewById( R.id.patient_radio_nullipara);
        tableBirths = (TableLayout) findViewById(R.id.patient_table);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        switchPMA = findViewById(R.id.patient_switch_pma);
        btnAddBirth = (Button) findViewById(R.id.patient_btn_addbirth);
        btnRemoveBirth = (Button) findViewById(R.id.patient_btn_removebirth);
        groupBtnsTable = (Group) findViewById(R.id.patient_group_btntable);
    }

    private void initViewData() {
        tableBirths.setVisibility(View.GONE);
        groupBtnsTable.setVisibility(View.GONE);
        if (report == null || !report.getPatient().isPresent()) {
            return;
        }
        final Patient p = report.getPatient().get();
        etName.setText(UtilsView.emptyIfNull(p.getName()));
        etAge.setText(UtilsView.emptyIfNull(p.getAge()));
        patientEtGestAge.setText(UtilsView.emptyIfNull(p.getGestAge()));
        patientEtHeight.setText(UtilsView.emptyIfNull(p.getHeight()));
        patientEtWeightCurr.setText(UtilsView.emptyIfNull(p.getWeightCurrent()));
        patientEtWeightPre.setText(UtilsView.emptyIfNull(p.getWeightPre()));
        switchPMA.setChecked(p.isPma());

        if (p.getBirthsHistory() == Patient.BirthsHistory.NULLIPARA) {
            radioNullipara.setChecked(true);
        } else if (p.getBirthsHistory() == Patient.BirthsHistory.PRIMIPARA) {
            radioPrimipara.setChecked(true);
            setPrevBirths(p);
            tableBirths.setVisibility(View.VISIBLE);
        } else if (p.getBirthsHistory() == Patient.BirthsHistory.PLURIPARA) {
            radioPluripara.setChecked(true);
            setPrevBirths(p);
            tableBirths.setVisibility(View.VISIBLE);
            groupBtnsTable.setVisibility(View.VISIBLE);
        }
    }

    private void onSubmit() {
        final Patient p = validateData();
        if (p != null)  {
            report.setPatient(p);
            DataRepository.get().updateReport(report, p, Patient.NAME);
            this.dismiss();
        }
    }

    private Patient validateData() {
        if(!UtilsView.isEmpty(etAge) && !Patient.validateAge(UtilsView.getValueAsInteger(etAge))) {
            showError("Età non valida");
            return null;
        } else if(!UtilsView.isEmpty(patientEtGestAge) && !Patient.validateGestAge(UtilsView.getValueAsInteger(patientEtGestAge))) {
            showError("Periodo di gestazione non valido");
            return null;
        } else if(!UtilsView.isEmpty(patientEtHeight) && !Patient.validateHeight(UtilsView.getValueAsInteger(patientEtHeight))) {
            showError("Altezza non valida");
            return null;
        } else if(!UtilsView.isEmpty(patientEtWeightPre) && !Patient.validateWeight(UtilsView.getValueAsDouble(patientEtWeightPre)) ||
                !UtilsView.isEmpty(patientEtWeightCurr) && !Patient.validateWeight(UtilsView.getValueAsDouble(patientEtWeightCurr))) {
            showError("Peso non valida");
            return null;
        }
        final Patient p = new Patient();
        p.setName(UtilsView.getValueAsString(etName));
        p.setAge(UtilsView.getValueAsInteger(etAge));
        p.setGestAge(UtilsView.getValueAsInteger(patientEtGestAge));
        p.setWeightCurrent(UtilsView.getValueAsDouble(patientEtWeightCurr));
        p.setWeightPre(UtilsView.getValueAsDouble(patientEtWeightPre));
        p.setHeight(UtilsView.getValueAsInteger(patientEtHeight));
        p.setPma(switchPMA.isChecked());

        if (radioNullipara.isChecked()) {
            p.setBirthsHistory(Patient.BirthsHistory.NULLIPARA);
        } else if (radioPrimipara.isChecked()) {
            p.setBirthsHistory(Patient.BirthsHistory.PRIMIPARA);
            p.setPreviousBirths(getSpinnersValues());
        } else if (radioPluripara.isChecked()) {
            p.setBirthsHistory(Patient.BirthsHistory.PLURIPARA);
            p.setPreviousBirths(getSpinnersValues());
        }
        return p;
    }

    private void addFirstBirth() {
        tableBirths.removeAllViews();
        rows.clear();
        birthCount = 0;
        addBirth();
    }

    private void addBirth() {
        final Context ctx = getContext();
        final TableRow row = getRowWithParams(ctx);
        final TextView tv = getTvWithParams(ctx);
        final Spinner spinner = getSpinnerWithParams(ctx);
        tv.setText("Parto n." + (birthCount + 1));
        row.addView(tv);
        row.addView(spinner);
        rows.add(row);
        tableBirths.addView(row);
        birthCount++;
    }

    private void removeLastBirth() {
        if(birthCount > 0) {
            tableBirths.removeView(rows.remove(rows.size() - 1));
            birthCount--;
        }
    }

    private void setPrevBirths(final Patient patient) {
        final List<String> births = patient.getPreviousBirths();
        if (births.size() == 0 && (patient.getBirthsHistory() == Patient.BirthsHistory.PRIMIPARA
                                || patient.getBirthsHistory() == Patient.BirthsHistory.PLURIPARA)) {
            addBirth();
        } else {
            final List<String> values = UtilsView.getStringListFromRes(getContext(), R.array.patient_previous_births);
            for (int i = 0; i < births.size(); i++) {
                addBirth();
                ((Spinner) rows.get(i).getChildAt(1)).setSelection(values.indexOf(births.get(i)));
            }
        }
    }

    private List<String> getSpinnersValues() {
        return rows.stream()
                .map(r -> (Spinner) r.getChildAt(1))
                .map(s -> (String) s.getSelectedItem())
                .filter(string -> !string.isEmpty())
                .collect(Collectors.toList());
    }

    private TableRow getRowWithParams(final Context ctx) {
        final TableRow row = new TableRow(ctx);
        row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT, 4.0f));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setPadding(0, 10, 0, 10);
        return row;
    }

    private TextView getTvWithParams(final Context ctx) {
        final TextView tv = new TextView(ctx);
        tv.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.MATCH_PARENT, 1.0f));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER);
        return tv;
    }

    private Spinner getSpinnerWithParams(final Context ctx) {
        final Spinner spinner = new Spinner(ctx);
        spinner.setLayoutParams(new TableRow.LayoutParams(0,
                        TableRow.LayoutParams.MATCH_PARENT, 3.0f));
        spinner.setGravity(Gravity.CENTER_VERTICAL);
        myInitSpinner(spinner, UtilsView.getStringListFromRes(getContext(),
                        R.array.patient_previous_births));
        return spinner;
    }

    private void myInitSpinner(final Spinner spinner, List<String> values) {
        ArrayAdapter adapter = new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, values);
        spinner.setAdapter(adapter);
    }
}
