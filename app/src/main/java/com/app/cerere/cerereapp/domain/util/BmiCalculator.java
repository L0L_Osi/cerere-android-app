package com.app.cerere.cerereapp.domain.util;

public class BmiCalculator {

    public static double getBmiByCm(final double weight, final int heightInCm) {
        return getBmiByM(weight, (double) heightInCm / 100);
    }

    public static double getBmiByM(final double weight, final double heightInMeters) {
        double r =  weight / (heightInMeters*heightInMeters);
        return (double) Math.round(r * 100) / 100;
    }
}
