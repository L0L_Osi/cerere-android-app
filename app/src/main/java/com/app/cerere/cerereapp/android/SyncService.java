package com.app.cerere.cerereapp.android;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.C;
import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.local.JsonUtils.JsonUtils;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.service.WifiListener;
import com.app.cerere.cerereapp.android.service.WsMessage;
import com.app.cerere.cerereapp.domain.model.Report;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;

public class SyncService extends Service implements Observer {

    public static final String CHANNEL_ID = "cerereNotification";
    private Optional<MySocket> ws = Optional.empty();

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        if (intent != null && intent.getAction() != null && intent.getAction().equals(C.START_FOREGROUND)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                initNotification();
            }
        }
        return SyncService.START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ws = Optional.of(new MySocket());
        new Thread(() -> ws.get().connect()).start();
        WifiListener.get().addObserver(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi (api = Build.VERSION_CODES.O)
    private void initNotification() {
        NotificationChannel nc = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_DEFAULT);
        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.createNotificationChannel(nc);
        Notification notification = new Notification.Builder(this, CHANNEL_ID)
                .setContentTitle("Cerere")
                .setContentText("Servizio attivo")
                .setBadgeIconType(R.drawable.child_notification)
                .setSmallIcon(R.drawable.child_notification)
                .setChannelId(CHANNEL_ID)
                .build();

        startForeground(999, notification);
    }

    @Override
    public void update(final Observable o, final Object arg) {
        if(ws.isPresent()) {
            ws.get().reconnect();
        }
    }


    public void initWebSocketClient() {
        if(!ws.isPresent()) {

        }
    }

    public void closeSocket() {
        if(ws.isPresent()) {
            ws.get().close();
            ws = Optional.empty();
        }
    }



    public class MySocket extends WebSocketClient {
        private static final String WS_ADDRESS =  "/cerere/api/ws/subscribe";

        private MySocket() {
            super(URI.create(LocalDataCacher.get().getIpAddress(App.get().getApplicationContext()) + WS_ADDRESS));
        }

        @Override
        public void onOpen(final ServerHandshake handshakedata) {
            Log.d("SOCKET", "connected");
        }

        @Override
        public void onMessage(final String message) {
            Log.d("SOCKET MSG", message);
            final WsMessage msg = JsonUtils.jsonStringToObj(message, WsMessage.class);
            final WsMessage.WsOperation operation = msg.getOperation();
            if (operation == WsMessage.WsOperation.UPDATE) {
                DataRepository.get().reloadReport(msg.getId());
            } else if (operation == WsMessage.WsOperation.CREATE) {
                DataRepository.get().reloadReport(msg.getId());
            } else if (operation == WsMessage.WsOperation.DELETE) {
                final Report report = DataRepository.get().getLocalReportById(msg.getId());
                if (report != null) {
                    DataRepository.get().deleteReport(report.getRoom());
                }
            }
        }

        @Override
        public void onClose(final int code, final String reason, final boolean remote) {
            Log.d("SOCKET", "closed");
        }

        @Override
        public void onError(final Exception ex) {
        }
    }
}
