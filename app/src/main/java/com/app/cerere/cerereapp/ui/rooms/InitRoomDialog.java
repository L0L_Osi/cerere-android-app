package com.app.cerere.cerereapp.ui.rooms;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.ui.dialogs.AbstractDialog;
import com.app.cerere.cerereapp.ui.dialogs.TableInputADialog;
import com.app.cerere.cerereapp.ui.dialogs.TableInputVODialog;

public class InitRoomDialog extends AbstractDialog {


    private Button btnCancel;
    private Button btnNew;
    private Button btnOld;

    private Room room;
    private RoomsActivity activity;

    public InitRoomDialog(@NonNull final Context context, final Room room, final RoomsActivity activity) {
        super(context);
        this.room = room;
        this.activity = activity;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_init_report, "");

        initView();
        btnNew.setOnClickListener(e -> {
            dismiss();
           new NewReportDialog(getContext(), this.room).show();
        });
        btnOld.setOnClickListener(e -> {
            dismiss();
            new OldReportDialog(getContext(), this.room, this.activity).show();
        });
        btnCancel.setOnClickListener(e -> dismiss());
    }

    private void initView() {
        btnCancel = findViewById(R.id.btn_init_cancel);
        btnNew = findViewById(R.id.btn_new);
        btnOld = findViewById(R.id.btn_old);
    }
}
