package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.BloodLoss;
import com.app.cerere.cerereapp.domain.model.event.Pph;
import com.app.cerere.cerereapp.ui.util.UtilsView;

public class BloodLossDialog extends AbstractDialog {

    private EditText bloodEtQuantity;
    private Button btnSubmit;
    private Button btnCancel;

    private final Report report;

    public BloodLossDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_blood_loss, "Perdite ematiche");

        initView();
        initViewData();
        btnCancel.setOnClickListener(e -> dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private void initView() {
        bloodEtQuantity = findViewById(R.id.blood_et_quantity);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
    }

    private void initViewData() {
        if (report.getBloodLoss().isPresent()) {
            int quantity = report.getBloodLoss().get().getQuantity();
            bloodEtQuantity.setText(String.valueOf(quantity));
        }
    }

    private void onSubmit() {
        if (!BloodLoss.validateQuantity(UtilsView.getValueAsInteger(bloodEtQuantity))) {
            showError("Quantità non valida");
        } else {
            final BloodLoss b = new BloodLoss();
            b.setQuantity(UtilsView.getValueAsInteger(bloodEtQuantity));
            report.setBloodLoss(b);
            DataRepository.get().updateReport(report, b, BloodLoss.NAME);
            this.dismiss();
        }
    }
}
