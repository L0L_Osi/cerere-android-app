package com.app.cerere.cerereapp.domain.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String TIME_FORMAT = "HH:mm";
    private static final String DATETIME_FORMAT = "yyyy-MM-dd HH:mm";
    private DateUtil() { }

    /**
     * @return the current date in the format yyyy-MM-dd
     */
    public static String getCurrentDate() {
        final DateFormat dt = new SimpleDateFormat(DATE_FORMAT);
        return dt.format(new Date());
    }

    /**
     * @return the current time in the format HH:mm
     */
    public static String getCurrentTime() {
        final DateFormat dt = new SimpleDateFormat(TIME_FORMAT);
        return dt.format(new Date());
    }

    public static boolean validateDate(final String date) {
        final DateFormat dt = new SimpleDateFormat(DATE_FORMAT);
        dt.setLenient(true);
        try {
            Date d = dt.parse(date);
            return true;
        } catch (ParseException e) {
           return false;
        }
    }

    public static boolean validateTime(final String time) {
  /*      final DateFormat dt = new SimpleDateFormat(TIME_FORMAT);
        dt.setLenient(true);
        try {
            Date d = dt.parse(time);
            return true;
        } catch (ParseException e) {
            return false;
        } */
        return time.matches("([01]?[0-9]|2[0-3]):[0-5][0-9]");
    }

    public static int timeDifferenceInMin(String date1, String time1, String date2, String time2) {
            final DateFormat dt = new SimpleDateFormat(DATETIME_FORMAT);
            if(validateTime(time1) && validateTime(time2) &&
            validateDate(date1) && validateDate(date2)) {
                dt.setLenient(true);
                try {
                    Date d1 = dt.parse(date1 + " " + time1);
                    Date d2 = dt.parse(date2 + " " + time2);
                    long difference = d1.getTime() - d2.getTime();
                    return (int) (difference / (60 * 1000));
                } catch (ParseException e) {
                    return -1;
                }
            } else return -1;
    }
}
