package com.app.cerere.cerereapp.ui.component;

import android.content.Context;
import android.text.Layout;
import android.view.Gravity;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Getter;

public class MyTable {

    public interface TableClickListener {
        void itemClicked(int row, int column);
    }

    private static final int ROW_HEIGHT = 40;
    private static final int MINIMUM_COLUMN_WIDTH = 200;

    private final Context ctx;
    private final View view;
    private int columnCounter;
    private final TableLayout tableLayout;

    private List<String> names;
    private final TableLayout tableNamesLayout;
    private final TableClickListener listener;

    @Getter
    final List<TableRow> rows = new ArrayList<>();

    @Getter List<TableRow> rowsWithNames = new ArrayList<>();

    /**
     * Your view must contain the component_table.xml otherwise a exception will be thrown.
     * @param ctx context
     * @param view the view where is R.id.table_layout
     * @param listener
     */
    public MyTable(final Context ctx, final View view, final TableClickListener listener) {
        this.ctx = ctx;
        this.view = view;
        this.listener = listener;
        this.columnCounter = 0;
        this.tableLayout = (TableLayout) view.findViewById(R.id.table_layout);
        this.tableNamesLayout = (TableLayout) view.findViewById(R.id.table_names_layout);
        if (tableLayout == null)
            throw  new IllegalArgumentException("Impossibile trovare R.id.table_layout nella view");
        if (tableNamesLayout == null)
            throw  new IllegalArgumentException("Impossibile trovare R.id.table_names_layout nella view");
    }

    public void init(final List<String> names) {
        if (names == null) {
            return;
        }
        names.forEach(s -> {
            final TableRow row = createRow();
            row.addView(createTv(s));
            tableNamesLayout.addView(row);
            rowsWithNames.add(row);
        });
        setUpColumn(rows, names.size());
        rows.forEach(tableLayout::addView);
        this.names = names;
        UtilsView.alternateColorColumn(rowsWithNames, 0);
    }

    public void addColumn(final List<String> values) {
        final int column = columnCounter;
        for (int i = 0; i < values.size(); i++) {
            final int row = i;
            final TextView tv = createTv(values.get(i));
            tv.setOnClickListener(l -> listener.itemClicked(row, column));
            rows.get(i).addView(tv);
        }
        UtilsView.alternateColorColumn(rows, column);
        columnCounter++;
    }

    public void reloadColumn(final List<String> strings, final int columnNum) {
        final List<TextView> tvs = rows.stream()
                .map(r -> (TextView) r.getChildAt(columnNum))
                .collect(Collectors.toList());
        putTexts(tvs, strings);
    }

    public void eraseAll() {
        tableLayout.removeAllViews();
        tableNamesLayout.removeAllViews();
        rows.clear();
        columnCounter = 0;
        init(names);
    }

    private void putTexts(final List<TextView> tvs, final List<String> texts) {
        if (tvs.size() != texts.size())
            throw new IllegalStateException("Il numero di TextView e il numero di valori non corrispondono");

        for (int i = 0; i < tvs.size(); i++) {
            tvs.get(i).setText(texts.get(i));
        }
    }

    private void setUpColumn(final List<TableRow> rowList, final int size) {
        for (int i = 0; i < size; i++) {
            final TableRow row = new TableRow(ctx);
            row.setMinimumHeight(ROW_HEIGHT);
            rowList.add(row);
        }
    }

    private TextView createTv(final String text) {
        final TextView tv = new TextView(ctx);
        tv.setText(text);
        applyTextViewParams(tv);
        return tv;
    }

    private TableRow createRow() {
        final TableRow row = new TableRow(ctx);
        row.setMinimumHeight(ROW_HEIGHT);
        return row;
    }

    private void applyTextViewParams(final TextView tv) {
        tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.MATCH_PARENT, 1.0f));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER);
        tv.setMinimumWidth(MINIMUM_COLUMN_WIDTH);
    }
}
