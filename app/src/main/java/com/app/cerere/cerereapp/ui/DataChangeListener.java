package com.app.cerere.cerereapp.ui;

public interface DataChangeListener {

    void reloadData();
}
