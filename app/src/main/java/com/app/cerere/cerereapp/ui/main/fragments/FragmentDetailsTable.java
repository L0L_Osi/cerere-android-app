package com.app.cerere.cerereapp.ui.main.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Recognition;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.app.cerere.cerereapp.ui.component.MyTable;
import com.app.cerere.cerereapp.ui.dialogs.PartialDataListener;
import com.app.cerere.cerereapp.ui.dialogs.TableInputADialog;
import com.app.cerere.cerereapp.ui.dialogs.TableInputDialog;
import com.app.cerere.cerereapp.ui.dialogs.TableInputVODialog;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FragmentDetailsTable extends AbstractFragmentDetails implements MyTable.TableClickListener, PartialDataListener<Dialog> {

    private static final int NONE = -1;
    private int columnSelected = NONE;

    private View view;
    private Button btnAdd;
    private Button btnEdit;
    private Button btnCancel;
    private Report report;
    private MyTable table;

    public static FragmentDetailsTable newInstance(final Report report) {
        final FragmentDetailsTable fragment = new FragmentDetailsTable();
        final Bundle b = new Bundle();
        b.putSerializable(SELECTED_REPORT, report);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle b = getArguments();
        if (b == null || !b.containsKey(SELECTED_REPORT)) {
            return;
        }
        this.report = (Report) b.getSerializable(SELECTED_REPORT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final View v =  inflater.inflate(R.layout.fragment_details_table, container, false);
        initView(v);
        initViewData();
        btnAdd.setOnClickListener(e -> onAdd());
        btnEdit.setOnClickListener(e -> onEdit());
        btnCancel.setOnClickListener(e -> onErase());
        this.view = v;
        return v;
    }

    @Override
    public void reloadData() {
    }

    @Override
    public void setData(Dialog dialog) {
        final int prevSize = report.getRecognitions().size();
        dialog.setOnDismissListener(l -> {
            if (report.getRecognitions().size() > prevSize) {
                final List<String> strings = getColumnTexts(report.getRecognitions().get(prevSize));
                table.addColumn(strings);
                UtilsView.alternateColorColumn(table.getRows(), prevSize);
            }

            if (columnSelected != NONE) {
                UtilsView.alternateColorColumn(table.getRows(), columnSelected);
                this.columnSelected = NONE;
            }

        });
        dialog.show();
    }

    private void onEdit() {
        if (columnSelected == NONE) {
            Toast.makeText(getContext(), "Nessuna colonna selezionata!", Toast.LENGTH_SHORT).show();
            return;
        }
        final Recognition re = report.getRecognitions().get(columnSelected);
        Dialog t = null;
        switch(re.getRecognType()) {
            case A:
                t = new TableInputADialog(getContext(), report, re);
                break;
            case VO:
                t = new TableInputVODialog(getContext(), report, re);
                break;
        }

        if(t != null) {
            t.setOnDismissListener(l ->
                    table.reloadColumn(getColumnTexts(report.getRecognitions().get(columnSelected)), columnSelected));
            t.show();
        }
    }

    private void onAdd() {
        final TableInputDialog tbd = new TableInputDialog(getContext(), report, this);
        tbd.show();
    }

    private void onErase() {
        if(columnSelected != NONE) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    // Add the buttons
            builder.setTitle("ATTENZIONE");
            builder.setMessage("Sicuro di voler eliminare questo Recognition?");
            builder.setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    report.getRecognitions().remove(columnSelected);
                    DataRepository.get().updateReport(report, null, Recognition.NAME);
                    table.eraseAll();
                    for (int i = 0; i < report.getRecognitions().size(); i++) {
                        final Recognition r = report.getRecognitions().get(i);
                        table.addColumn(getColumnTexts(r));
                    }
                    columnSelected = NONE;
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(this.getContext(), "Nessuna colonna selezionata!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void itemClicked(final int row, final int column) {
        Log.d("COLUMN CLICKED", String.valueOf(column));
        if (columnSelected != NONE) {
            UtilsView.alternateColorColumn(table.getRows(), columnSelected);
        }
        this.columnSelected = column;
        UtilsView.colorColumn(table.getRows(), column, Color.YELLOW);
    }

    private void initView(final View v) {
        table = new MyTable(getContext(), v, this);
        btnAdd = v.findViewById(R.id.btn_add);
        btnCancel = v.findViewById(R.id.btn_cancel);
        btnEdit = v.findViewById(R.id.btn_edit);
    }

    private void initViewData() {
        if (report == null || report.getRecognitions() == null) {
            return;
        }
        table.init(UtilsView.getStringListFromRes(getContext(), R.array.recognition_rows));
        for (int i = 0; i < report.getRecognitions().size(); i++) {
            final Recognition r = report.getRecognitions().get(i);
            table.addColumn(getColumnTexts(r));
        }
    }

    private List<String> getColumnTexts(final Recognition recognition) {
        final List<String> values = new ArrayList<>();
        values.add(UtilsView.emptyIfNull(recognition.getDate()));
        values.add(UtilsView.emptyIfNull(recognition.getTime()));
        values.add(recognition.getNeck() != null ? recognition.getNeck().getName() : UtilsView.EMPTY_VALUE);
        values.add(UtilsView.emptyIfNull(recognition.getDilation()));
        values.add(UtilsView.emptyIfNull(recognition.getLvHead()));
        values.add(UtilsView.emptyIfNull(recognition.getFcFoetal()));
        values.add(recognition.getCtg() != null ? recognition.getCtg().getName() : UtilsView.EMPTY_VALUE);
        final List<Anesthetic> anesthetics = recognition.getAnesthetics();

        if (anesthetics == null || anesthetics.stream().allMatch(x -> x == null)) {
            Log.d("Errore", "dati danneggiati");
            values.add(UtilsView.EMPTY_VALUE);
            values.add(UtilsView.EMPTY_VALUE);
            values.add(UtilsView.EMPTY_VALUE);
        } else {
            final StringBuilder cell1 = new StringBuilder();
            anesthetics.stream().filter(x -> x != null && x.getVia() == DrugsVia.SUBARACHNOID)
                    .forEach(x -> cell1.append(x.toString() + " "));
            values.add(cell1.toString());
            final StringBuilder cell2 = new StringBuilder();
            anesthetics.stream().filter(x -> x != null && x.getVia() == DrugsVia.EPIDURAL)
                    .forEach(x -> cell2.append(x.toString() + " "));
            values.add(cell2.toString());
            final StringBuilder cell3 = new StringBuilder();
            anesthetics.stream().filter(x -> x != null && x.getVia() == DrugsVia.PIEB)
                    .forEach(x -> cell3.append(x.toString() + " "));
            values.add(cell3.toString());
        }

        values.add(UtilsView.emptyIfNull(recognition.getVasBefore()));
        values.add(UtilsView.emptyIfNull(recognition.getVasAfter()));
        values.add(UtilsView.emptyIfNull(recognition.getBromage()));
        values.add(UtilsView.emptyIfNull(recognition.getFcMaternal()));
        values.add(UtilsView.emptyIfNull(recognition.getPaMaternalA())
                + "/" + UtilsView.emptyIfNull(recognition.getPaMaternalB()));
        values.add(UtilsView.emptyIfNull(recognition.getSpO2Maternal()));
        values.add(UtilsView.emptyIfNull(recognition.getTemp()));
        values.add(recognition.getOxytocin() != null? UtilsView.boolYesOrNo(recognition.getOxytocin()): "");
        values.add(recognition.getOxytocinSuspended() != null? UtilsView.boolYesOrNo(recognition.getOxytocinSuspended()): "");
        values.add(UtilsView.emptyIfNull(recognition.getOxytocinSpeed()));
        return values;
    }
}
