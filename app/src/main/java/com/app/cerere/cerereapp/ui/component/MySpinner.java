package com.app.cerere.cerereapp.ui.component;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.util.EnumUtils;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class MySpinner<X extends Enum> {

    @Getter private final Spinner spinner;
    private ArrayAdapter<String> adapter;
    @Getter private final List<X> values = new ArrayList<>();
    private List<String> strings = new ArrayList<>();

    public MySpinner(final View view, final int spinner,final List<X> values) {
        this.spinner = view.findViewById(spinner);
        this.values.add(null);
        this.strings.add("");
        this.values.addAll(values);
        this.strings.addAll(EnumUtils.enumListToStringList(values));
        this.adapter = new ArrayAdapter<>(view.getContext(), R.layout.support_simple_spinner_dropdown_item, strings);
        this.adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.spinner.setAdapter(adapter);
    }

    public void setItemSelected(final X x) {
        spinner.setSelection(values.indexOf(x));
    }

    public X getItemSelected() {
        return values.get(spinner.getSelectedItemPosition());
    }

    public void setOnItemClickListener(AdapterView.OnItemSelectedListener listener) {
        spinner.setOnItemSelectedListener(listener);
    }
}
