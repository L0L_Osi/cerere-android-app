package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Secondamento;

public class SecondamentoDialog extends AbstractDialog {

    private RadioGroup secRadiogroup;
    private RadioButton secRadioSpontaneo;
    private RadioButton secRadioManuale;
    private RadioButton secRadioRcu;
    private Button btnSubmit;
    private Button btnCancel;

    private final Report report;


    public SecondamentoDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_secondamento, "Secondamento");

        initView();
        initViewData();
        btnCancel.setOnClickListener(e -> dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private void initView() {
        secRadiogroup = findViewById( R.id.sec_radiogroup );
        secRadioSpontaneo = findViewById( R.id.sec_radio_spontaneo );
        secRadioManuale = findViewById( R.id.sec_radio_manuale );
        secRadioRcu = findViewById( R.id.sec_radio_rcu );
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
    }

    private void initViewData() {
        if (report != null && report.getSecondamento().isPresent()) {
            switch (report.getSecondamento().get().getType()) {
                case SPONTANEO:
                    secRadioSpontaneo.setChecked(true);
                    break;
                case MANUALE:
                    secRadioManuale.setChecked(true);
                    break;
                case RCU:
                    secRadioRcu.setChecked(true);
                    break;
            }
        }
    }

    private boolean validateData() {
        return secRadioSpontaneo.isChecked()
                || secRadioManuale.isChecked()
                || secRadioRcu.isChecked();
    }

    private void onSubmit() {
        final Secondamento s = new Secondamento();
        if (validateData()) {
            if (secRadioSpontaneo.isChecked())
                s.setType(Secondamento.Type.SPONTANEO);
            else if (secRadioManuale.isChecked())
                s.setType(Secondamento.Type.MANUALE);
            else if (secRadioRcu.isChecked())
                s.setType(Secondamento.Type.RCU);
            report.setSecondamento(s);
            DataRepository.get().updateReport(report, s, Secondamento.NAME);
            this.dismiss();
        }  else {
            showError("Tipo non selezionato");
        }
    }

}
