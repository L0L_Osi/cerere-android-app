package com.app.cerere.cerereapp.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Recognition;

import java.util.LinkedList;
import java.util.List;

public abstract class AnestheticsDialog extends AbstractDialog implements PartialDataListener<Object>{

    private PartialDataListener<Object> listener;
    protected List<Anesthetic> anesthetics = new LinkedList<>();

    private Button btnSubmit;
    private Button btnCancel;

    public AnestheticsDialog(@NonNull final Context context, final List<Anesthetic> anesthetics, final PartialDataListener<Object> listener) {
        super(context);
        this.listener = listener;
        if(anesthetics != null) {
            this.anesthetics.addAll(anesthetics);
        }
    }

    public abstract void setData(final Object obj);

    protected void onCreate(final Bundle savedInstanceState, int layout, String title) {
        super.onCreate(savedInstanceState, layout, title);

        initView();
        initViewData();
        btnSubmit.setOnClickListener(e -> onSubmit());
        btnCancel.setOnClickListener(e -> dismiss());
    }

    protected abstract void initViewData();

    private void onSubmit() {
        listener.setData(anesthetics);
        this.dismiss();
    }

    protected void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
    }

    protected void insertRow(Anesthetic a, TableLayout table) {
        final Context ctx = getContext();
        final TableRow row = getRowWithParams(ctx);
        final TextView tv = getTvWithParams(ctx);
        final Button btn = getButtonWithParams(ctx, "rimuovi");

        btn.setOnClickListener(e -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            builder.setTitle("ATTENZIONE");
            builder.setMessage("Sicuro di voler eliminare questo Anestetico?");

            builder.setPositiveButton(R.string.submit, new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    table.removeView(row);
                    anesthetics.remove(a);
                }
            });

            builder.setNegativeButton(R.string.cancel, new OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

        tv.setText(a.toString());
        tv.setTextSize(16);
        row.addView(tv);
        row.addView(btn);
        table.addView(row);
    }

    private TextView getTvWithParams(final Context ctx) {
        final TextView tv = new TextView(ctx);
        tv.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.MATCH_PARENT, 3.0f));
        tv.setPadding(5, 0, 5, 0);
        tv.setGravity(Gravity.CENTER);
        return tv;
    }

    private Button getButtonWithParams(final Context ctx, final String text) {
        final Button btn = new Button(ctx);
        btn.setLayoutParams(new TableRow.LayoutParams(0,
                TableRow.LayoutParams.WRAP_CONTENT, 1.0f));
        btn.setText(text);
        btn.setGravity(Gravity.CENTER);
        btn.setPadding(5, 0, 5, 0);
        return btn;
    }

    private TableRow getRowWithParams(final Context ctx) {
        final TableRow row = new TableRow(ctx);
        row.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT, 4));
        row.setGravity(Gravity.CENTER_VERTICAL);
        row.setPadding(0, 10, 0, 10);
        return row;
    }
}
