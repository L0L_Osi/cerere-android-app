package com.app.cerere.cerereapp.ui.dialogs;

import android.app.Service;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.android.SessionManager;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.android.service.ServiceConfig;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.ui.main.ReportMainActivity;

public class SettingDialog extends AbstractDialog {

    private EditText set_ipaddress;
    private Button btnCancel;
    private Button btnSubmit;

    public SettingDialog(@NonNull final Context context) {
        super(context);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_settings, "");

        initView();
        set_ipaddress.setText(LocalDataCacher.get().getIpAddress(App.get().getApplicationContext())
                .substring(LocalDataCacher.get().getIpAddress(App.get().getApplicationContext()).lastIndexOf("/") + 1,
                        LocalDataCacher.get().getIpAddress(App.get().getApplicationContext()).lastIndexOf(":")));
        btnCancel.setOnClickListener(e -> this.dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private void onSubmit() {
        LocalDataCacher.get().saveIpAddress(App.get().getApplicationContext(), set_ipaddress.getText().toString());
        this.dismiss();
    }

    private void initView() {
        set_ipaddress = findViewById(R.id.set_ipaddress);
        btnCancel = findViewById(R.id.btn_cancel);
        btnSubmit = findViewById(R.id.btn_submit);
    }
}
