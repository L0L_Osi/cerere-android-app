package com.app.cerere.cerereapp.ui.main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.cerere.cerereapp.C;
import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Patient;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.domain.util.BmiCalculator;
import com.app.cerere.cerereapp.ui.DataChangeListener;
import com.app.cerere.cerereapp.ui.GenericPagerAdapter;
import com.app.cerere.cerereapp.ui.main.fragments.FragmentDetailsBirth;
import com.app.cerere.cerereapp.ui.main.fragments.FragmentDetailsInfo;
import com.app.cerere.cerereapp.ui.main.fragments.FragmentDetailsLabor;
import com.app.cerere.cerereapp.ui.main.fragments.FragmentDetailsTable;
import com.app.cerere.cerereapp.ui.rooms.RoomsActivity;
import com.app.cerere.cerereapp.ui.util.UtilsView;


public class MainContainerFragment extends Fragment implements DataChangeListener {

    private Room room;

    private TextView mainTvNameValue;
    private TextView mainTvWeightValue;
    private TextView mainTvBmiValue;
    private TextView mainTvAgeValue;
    private TextView mainTvGestAgeValue;
    private Button btnSetComplete;

    public static MainContainerFragment newInstance(final Room room) {
        final MainContainerFragment fragment = new MainContainerFragment();
        final Bundle args = new Bundle();
        args.putSerializable(C.SELECTED_SECTION, room);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.room = (Room) getArguments().getSerializable(C.SELECTED_SECTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main_container, container, false);
        final Report report = DataRepository.get().getLocalReportByRoom(room);
        if (report != null) {
            final ViewPager viewPager = view.findViewById(R.id.main_bot_content);
            final TabLayout tabs = view.findViewById(R.id.main_section_tabs);
            initView(view);
            if (report.getPatient().isPresent()) {
                initViewData(report.getPatient().get());
            }
            initViewPager(viewPager, report);
            tabs.setupWithViewPager(viewPager);
            btnSetComplete.setOnClickListener(e -> onCompleteReport());
        }
        return view;
    }

    private void initView(final View v) {
        mainTvNameValue = v.findViewById( R.id.main_tv_name_value );
        mainTvWeightValue = v.findViewById( R.id.main_tv_weight_value );
        mainTvBmiValue = v.findViewById( R.id.main_tv_bmi_value );
        mainTvAgeValue = v.findViewById( R.id.main_tv_age_value );
        mainTvGestAgeValue = v.findViewById( R.id.main_tv_gest_age_value );
        btnSetComplete = v.findViewById(R.id.main_btn_complete);
    }

    private void initViewData(final Patient p) {
        mainTvNameValue.setText(UtilsView.emptyIfNull(p.getName()));
        mainTvWeightValue.setText(UtilsView.emptyIfNull(p.getWeightCurrent()));
        mainTvAgeValue.setText(UtilsView.emptyIfNull(p.getAge()));
        mainTvGestAgeValue.setText(UtilsView.emptyIfNull(p.getGestAge()));
        if (p.getWeightCurrent() != null && p.getHeight() != null) {
            mainTvBmiValue.setText(String.valueOf(BmiCalculator.getBmiByCm(p.getWeightCurrent(), p.getHeight())));
        }
    }

    private void initViewPager(final ViewPager vp, final Report r) {
        final GenericPagerAdapter adapter = new GenericPagerAdapter(getChildFragmentManager());
        adapter.addFragment(FragmentDetailsInfo.newInstance(r), "Info");
        adapter.addFragment(FragmentDetailsLabor.newInstance(r), "Travaglio");
        adapter.addFragment(FragmentDetailsTable.newInstance(r), "Tabella");
        adapter.addFragment(FragmentDetailsBirth.newInstance(r), "Parto");
        vp.setAdapter(adapter);
    }


    @Override
    public void reloadData() {
        final Report report = DataRepository.get().getLocalReportByRoom(room);
        if (report != null && report.getPatient().isPresent()) {
            initViewData(report.getPatient().get());
        }
    }

    private void onCompleteReport() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Conferma completamento");
        alert.setMessage("Sei sicuro di voler terminare questo report?");
        alert.setPositiveButton("Sì", (dialog, which) -> {
            Report report = DataRepository.get().getLocalReportByRoom(room);

            if(report.getSdo() == null ||
                    report.getSdo().length() <= 0) {

                AlertDialog.Builder alert2 = new AlertDialog.Builder(getContext());
                alert2.setTitle("Attenzione");
                alert2.setMessage("Non hai inserito lo SDO del report. Vuoi comunque terminare questo report?");

                alert2.setPositiveButton("Sì", (dialog2, which2) -> {
                    DataRepository.get().completeReport(room);
                    dialog2.dismiss();
                    RoomsActivity.startActivity(getContext());
                });

                alert2.setNegativeButton("No", (dialog2, which2) -> {
                    dialog2.dismiss();
                    dialog.dismiss();
                });
                alert2.show();
            } else {
                DataRepository.get().completeReport(room);
                dialog.dismiss();
                RoomsActivity.startActivity(getContext());
            }
        });
        alert.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        alert.show();
    }
}
