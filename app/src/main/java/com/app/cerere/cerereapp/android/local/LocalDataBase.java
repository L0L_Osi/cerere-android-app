package com.app.cerere.cerereapp.android.local;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.android.local.JsonUtils.JsonUtils;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.User;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class LocalDataBase {

    private static final String TAG = "MSG";
    private static final String REPORTS_DB = "cererereports.txt";

    private static final LocalDataBase INSTANCE = new LocalDataBase();

    private final Context ctx;
    private final LocalFileHandler db_reports;

    private LocalDataBase() {
        this.ctx = App.get().getApplicationContext();
        db_reports = new LocalFileHandler(REPORTS_DB);
    }

    public static LocalDataBase get() { return INSTANCE; }

    public void removeReportList() {
        db_reports.deleteFile();
    }

    private void saveReportList(final List<Report> reports) {
        String jsonDataString = JsonUtils.objToJsonString(reports);
        db_reports.writeFile(jsonDataString);
        Toast.makeText(ctx, "Dati Aggiornati", Toast.LENGTH_SHORT).show();
    }

    public void removeReport(final Report report) {
        new Thread(() -> {
            List<Report> reports = loadReports();
            final Integer index = findReportInList(report, reports);

            if(index != null && reports.remove(reports.get(index))) {
                this.saveReportList(reports);
            }
        }).start();
    }

    public void completeReport(Report update) {
        new Thread(() -> {
            List<Report> reports = loadReports();
            final Integer index = findReportInList(update, reports);

            if (index == null) {
                reports.add(update);
                this.saveReportList(reports);
            }
        }).start();
    }

    public List<Report> loadReports() {
        if(db_reports.isUsable()) {
            Log.d(TAG, "file exists");
            return JsonUtils.jsonArrayStringToReportList(db_reports.readFile());
        } else return new ArrayList<>();
    }

    private Integer findReportInList(Report report, List<Report> list) {
        Optional<Report> match = list.stream().filter(x -> x.equals(report)).findAny();
        if(match.isPresent()) {
            return list.indexOf(match.get());
        }
        return null;
    }
}

