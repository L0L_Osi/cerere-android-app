package com.app.cerere.cerereapp.android.local.JsonUtils;

import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.User;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

public class JsonUtils {

    private static GsonCustomConverter converter = new GsonCustomConverter();

    public static JSONObject objToJSON(final String containerName, final Object obj) {
        return createJsonIntoAContainer(containerName,
                obj instanceof String? obj.toString(): objToJsonString((obj)));
    }

    public static JSONObject objToJSON(final Object obj) {
        try {
            return new JSONObject(converter.getCustomGson().toJson(obj));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String objToJsonString(final Object obj) {
        return converter.getCustomGson().toJson(obj);
    }

    public static JSONArray objToJSONArray(final Object obj) {
        try {
            return new JSONArray(converter.getCustomGson().toJson(obj));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param jsonArrayString the JsonArray converted to string
     * @return the list of reports
     */
    public static List<Report> jsonArrayStringToReportList(final String jsonArrayString) {
        Type type = new TypeToken<List<Report>>() {}.getType();
        return converter.getCustomGson().fromJson(jsonArrayString, type);
    }

    /**
     * @param jsonArrayString the JsonArray converted to string
     * @return the list of users
     */
    public static List<User> jsonArrayStringToUserList(final String jsonArrayString) {
        Type type = new TypeToken<List<User>>() {
        }.getType();
        return converter.getCustomGson().fromJson(jsonArrayString, type);
    }

    public static <X> X jsonStringToObj(final String jsonString, final Class<X> x) {
        return converter.getCustomGson().fromJson(jsonString, x);
    }

    public static List<JSONObject> jsonStringToJSONList(final String jsonString) {
        Type type = new TypeToken<JSONArray>() {}.getType();
        List<JSONObject> list = converter.getCustomGson().fromJson(jsonString, type);
        return list;
    }

    public static JSONArray jsonStringToJSONArray(final String jsonString) {
        Type type = new TypeToken<JSONArray>() {}.getType();
        JSONArray array = converter.getCustomGson().fromJson(jsonString, type);
        return array;
    }

    private static JSONObject createJsonIntoAContainer(final String containerName, final String jsonString) {
        JSONObject container = null;
        try {
            if(jsonString.startsWith("[")) {
                container = new JSONObject().put(containerName, new JSONArray(jsonString));
            } else if(jsonString.startsWith("{")){
                container = new JSONObject().put(containerName, new JSONObject(jsonString));
            } else {
                container = new JSONObject().put(containerName, jsonString);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return container;
    }
}
