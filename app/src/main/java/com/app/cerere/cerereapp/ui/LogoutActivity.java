package com.app.cerere.cerereapp.ui;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.android.SessionManager;
import com.app.cerere.cerereapp.android.local.cache.LocalDataCacher;
import com.app.cerere.cerereapp.ui.login.LoginActivity;
import com.app.cerere.cerereapp.ui.rooms.RoomsActivity;

import java.io.File;

public class LogoutActivity extends AppCompatActivity {


    protected void onCreate(@Nullable final Bundle savedInstanceState, @LayoutRes int layout) {
        super.onCreate(savedInstanceState);
        enableExport();
        setContentView(layout);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.menu_logout) {
            SessionManager.get().logOut(this);
            this.finish();
            LoginActivity.startActivity(this);
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            return true;
        } else if(item.getItemId() == R.id.menu_share) {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            String myFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/cerere-app/cererereports.txt";
            File fileWithinMyDir = new File(myFilePath);

            if(fileWithinMyDir.exists()) {
                intentShareFile.setType("application/pdf");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + myFilePath));

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        "Sharing File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");

                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            } else {
                Toast.makeText(App.get().getApplicationContext(), "File dati vuoto", Toast.LENGTH_LONG).show();
            }

            return true;
        } else if(item.getItemId() == R.id.menu_clear_data) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Attenzione");
            alert.setMessage("Vuoi veramente eliminare tutti i dati nella cache?");

            alert.setPositiveButton("Sì", (dialog2, which2) -> {
                LocalDataCacher.get().clearCache(App.get().getApplicationContext());
                DataRepository.get().initRuntimeData();
            });

            alert.setNegativeButton("No", (dialog2, which2) -> {

            });
            alert.show();

            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private  boolean enableExport() {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            return false;
        }
    }

    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            DataRepository.get().initRuntimeData();
        } else {
            Toast.makeText(this, "Abilita la modifica ai dati per recuperare i report!", Toast.LENGTH_SHORT).show();
        }
    }
}
