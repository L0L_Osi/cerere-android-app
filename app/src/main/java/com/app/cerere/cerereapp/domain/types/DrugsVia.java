package com.app.cerere.cerereapp.domain.types;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public enum DrugsVia {
    @SerializedName("subarachnoid")
    SUBARACHNOID("Subaracnoidea"),

    @SerializedName("epidural")
    EPIDURAL("Epidurale"),

    @SerializedName("pieb")
    PIEB("PIEB"),

    @SerializedName("spinal")
    SPINAL("Spinale"),

    @SerializedName("general")
    GENERAL("Generale");

    @Getter private final String printName;

    DrugsVia(final String printName) {
        this.printName = printName;
    }
}
