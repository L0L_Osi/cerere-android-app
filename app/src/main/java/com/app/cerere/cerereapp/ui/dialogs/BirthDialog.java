package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Pair;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Birth;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.Optional;

public class BirthDialog extends AbstractDialog {

    private RadioButton radioSpontaneo;
    private CheckBox cbKristeller;
    private CheckBox cbVentosa;
    private CheckBox cbTc;
    private EditText etNote;
    private Switch switchEpisiotomia;
    private EditText etDate;
    private EditText etTime;
    private Button btnTime;
    private Button btnDate;
    private Button btnSubmit;
    private Button btnCancel;

    private final Birth birth;
    private final PartialDataListener<Object> listener;
    private final Integer index;

    public BirthDialog(@NonNull final Context context, PartialDataListener<Object> listener, final Integer index, final Optional<Birth> birth) {
        super(context);
        this.listener = listener;
        this.birth = birth.isPresent()? birth.get(): null;
        this.index = index;
    }

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_birth, "Informazioni parto");

        initView();
        initViewData();

        btnDate.setOnClickListener(e -> etDate.setText(DateUtil.getCurrentDate()));
        btnTime.setOnClickListener(e -> etTime.setText(DateUtil.getCurrentTime()));
        radioSpontaneo.setOnClickListener(e -> onExclusiveRadioClick());
        cbVentosa.setOnClickListener(e -> onChechBoxClick());
        cbKristeller.setOnClickListener(e -> onChechBoxClick());
        cbTc.setOnClickListener(e -> onChechBoxClick());
        btnCancel.setOnClickListener(e -> dismiss());
        btnSubmit.setOnClickListener(e -> onSubmit());
    }

    private void initView() {
        etDate = findViewById(R.id.birth_et_date);
        etTime = findViewById(R.id.birth_et_time);
        btnTime = findViewById(R.id.birth_btn_time);
        btnDate = findViewById(R.id.birth_btn_date);
        radioSpontaneo = findViewById( R.id.birth_radio_spontaneo );
        cbKristeller = findViewById( R.id.birth_cb_kristeller );
        cbTc = findViewById( R.id.birth_cb_tc );
        cbVentosa = findViewById( R.id.birth_cb_ventosa );
        etNote = findViewById( R.id.birth_et_presentation );
        switchEpisiotomia = findViewById(R.id.birth_switch_episiotomia);
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
    }

    private void initViewData() {
        if (birth == null) {
            return;
        }

        for (final Birth.Type type : birth.getType()) {
            if (type == Birth.Type.SPONTANEO)
                radioSpontaneo.setChecked(true);
            else {
                if (type == Birth.Type.VENTOSA)
                    cbVentosa.setChecked(true);
                if (type == Birth.Type.KRISTELLER)
                    cbKristeller.setChecked(true);
                if (type == Birth.Type.TC)
                    cbTc.setChecked(true);
            }
        }
        switchEpisiotomia.setChecked(birth.isEpisiotomia());
        etNote.setText(birth.getNote());

        etDate.setText(UtilsView.emptyIfNull(birth.getDate()));
        etTime.setText(UtilsView.emptyIfNull(birth.getTime()));
    }

    private void onExclusiveRadioClick() {
        cbTc.setChecked(false);
        cbKristeller.setChecked(false);
        cbVentosa.setChecked(false);

        UtilsView.setStrikeTextFlags(cbTc, cbKristeller, cbVentosa);
        UtilsView.setNotStrikeTextFlags(radioSpontaneo);
    }

    private void onChechBoxClick() {
        radioSpontaneo.setChecked(false);
        UtilsView.setStrikeTextFlags(radioSpontaneo);
        UtilsView.setNotStrikeTextFlags(cbTc, cbKristeller, cbVentosa);
    }

    private void onSubmit() {
        final Birth b = validateData();
        if (b != null) {
            b.setId(index);
            listener.setData(b);
            this.dismiss();
        }
    }

    private Birth validateData() {
        final Birth b = new Birth();

        if (UtilsView.isEmpty(etDate) || !DateUtil.validateDate(UtilsView.getValueAsString(etDate))) {
            showError("Data non valida");
            return null;
        }
        if (UtilsView.isEmpty(etTime) || !DateUtil.validateTime(UtilsView.getValueAsString(etTime))) {
            showError("Orario non valida");
            return null;
        }

        if(!setTypes(b)) {
            showError("Nessuna tipologia selezionata");
            return null;
        }
        b.setNote(UtilsView.getValueAsString(etNote));
        b.setEpisiotomia(switchEpisiotomia.isChecked());
        b.setDate(UtilsView.getValueAsString(etDate));
        b.setTime(UtilsView.getValueAsString(etTime));
        return b;
    }

    private boolean setTypes(final Birth b) {
        if (radioSpontaneo.isChecked()) {
            b.getType().add(Birth.Type.SPONTANEO);
        } else if(cbVentosa.isChecked() || cbKristeller.isChecked() || cbTc.isChecked()) {
            if (cbVentosa.isChecked())
                b.getType().add(Birth.Type.VENTOSA);
            if (cbKristeller.isChecked())
                b.getType().add(Birth.Type.KRISTELLER);
            if (cbTc.isChecked())
                b.getType().add(Birth.Type.TC);
        } else {
            return false;
        }
        return true;
    }
}
