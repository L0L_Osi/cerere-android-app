package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.PostoperativeAnalgesia;
import com.app.cerere.cerereapp.ui.util.UtilsView;

public class PostoperativeAnalgesiaDialog extends AbstractDialog {

    private PostoperativeAnalgesia postoperativeAnalgesia;
    private PartialDataListener<Object> listener;

    private EditText etProtocol;
    private EditText etVariation;
    private EditText etNotes;

    private Button btnSubmit;
    private Button btnCancel;


    public PostoperativeAnalgesiaDialog(@NonNull final Context context, final PostoperativeAnalgesia postoperativeAnalgesia, final PartialDataListener<Object> listener) {
        super(context);
        this.postoperativeAnalgesia = postoperativeAnalgesia;
        this.listener = listener;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_postoperativeanalgesia, "Analgesia Postoperatoria");
        initView();
        initViewData();
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }

    private void initViewData() {
        if(postoperativeAnalgesia == null)
            return;
        else {
            etProtocol.setText(UtilsView.emptyIfNull(postoperativeAnalgesia.getProtocolType()));
            etVariation.setText(UtilsView.emptyIfNull(postoperativeAnalgesia.getVariation()));
            etNotes.setText(UtilsView.emptyIfNull(postoperativeAnalgesia.getNotes()));
        }
    }

    private void onSubmit() {
        postoperativeAnalgesia = validateData();
        if (postoperativeAnalgesia == null)
            return;
        listener.setData(postoperativeAnalgesia);
        this.dismiss();
    }

    private PostoperativeAnalgesia validateData() {
        final PostoperativeAnalgesia post = new PostoperativeAnalgesia();
        if (UtilsView.isEmpty(etProtocol)) {
            showError("Tipo non specificato");
            return null;
        } else {
            post.setProtocolType(UtilsView.getValueAsString(etProtocol));
            post.setVariation(UtilsView.getValueAsString(etVariation));
            post.setNotes(UtilsView.getValueAsString(etNotes));
            return post;
        }
    }


    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);

        etProtocol = findViewById(R.id.post_protocol);
        etVariation = findViewById(R.id.post_variation);
        etNotes = findViewById(R.id.post_notes);
    }
}
