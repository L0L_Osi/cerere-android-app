package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic.DrugDetails;
import com.app.cerere.cerereapp.domain.types.Drugs;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedHashMap;

public class PiebDialog extends AbstractDialog {

    private EditText etSolutionType;
    private EditText etSpeed;
    private EditText etBoli;
    private EditText etLockout;
    private EditText etInterval;
    private Button btnSubmit;
    private Button btnCancel;

    private PartialDataListener<Object> listener;


    public PiebDialog(@NonNull final Context context, final PartialDataListener<Object> listener) {
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_pieb, "Via Pieb");
        initView();
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }


    private void onSubmit() {
        final Anesthetic anesthetic = validateData();
        if (anesthetic == null)
            return;
        listener.setData(anesthetic);
        this.dismiss();
    }

    private Anesthetic validateData() {
        final Anesthetic a = new Anesthetic();
        if (!valuesPresent()) {
            showError("Valori mancanti");
            return null;
        } else {
            a.setDrug(Drugs.OTHERS);
            a.setVia(DrugsVia.PIEB);
            a.setSolution(UtilsView.getValueAsString(etSolutionType));
            LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
            details.put(DrugDetails.BASALSPEED, UtilsView.getValueAsDouble(etSpeed));
            details.put(DrugDetails.BOLI, UtilsView.getValueAsDouble(etBoli));
            details.put(DrugDetails.LOCKOUT, UtilsView.getValueAsDouble(etLockout));
            details.put(DrugDetails.INTERVAL, UtilsView.getValueAsDouble(etInterval));
            a.setDetails(details);
            return a;
        }
    }

    private boolean valuesPresent() {
        return (!UtilsView.isEmpty(etSolutionType) &&
                !UtilsView.isEmpty(etSpeed) &&
                !UtilsView.isEmpty(etBoli) &&
                !UtilsView.isEmpty(etLockout) &&
                !UtilsView.isEmpty(etInterval));
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        etSolutionType = findViewById( R.id.pieb_et_solution);
        etSpeed = findViewById( R.id.pieb_et_speed);
        etBoli = findViewById( R.id.pieb_et_boli);
        etLockout = findViewById( R.id.pieb_et_lockout);
        etInterval = findViewById( R.id.pieb_et_interval);
    }
}
