package com.app.cerere.cerereapp.domain.types;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public enum BirthResult {

    @SerializedName ("reparto")
    REPARTO("Reparto"),

    @SerializedName("tin")
    TIN("Tin"),

    @SerializedName("deceduto")
    DECEDUTO("Deceduto");

    @Getter
    private final String printName;

    BirthResult(final String printName) {
        this.printName = printName;
    }

    @NonNull
    @Override
    public String toString() {
        return getPrintName();
    }
}
