package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.MembraneRupture;
import com.app.cerere.cerereapp.domain.util.DateUtil;
import com.app.cerere.cerereapp.ui.util.UtilsView;

public class MembraneRuptureDialog extends AbstractDialog {

    private final Report report;
    private Button btnSubmit;
    private Button btnCancel;
    private Button btnDate;
    private EditText etDate;
    private EditText etTime;
    private Button btnTime;
    private RadioButton radioNatural;
    private RadioButton radioAmnioressi;
    private RadioButton radioClear;
    private RadioButton radioDyedNS;
    private RadioButton radioDyedS;


    public MembraneRuptureDialog(@NonNull final Context context, final Report report) {
        super(context);
        this.report = report;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_membrane, "Rottura membrane");

        initView();
        initViewData();
        btnDate.setOnClickListener(e -> etDate.setText(DateUtil.getCurrentDate()));
        btnTime.setOnClickListener(e -> etTime.setText(DateUtil.getCurrentTime()));
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        btnDate = findViewById( R.id.membrane_btn_date );
        etDate = findViewById( R.id.membrane_et_date );
        etTime = findViewById( R.id.membrane_et_time );
        btnTime = findViewById( R.id.membrane_btn_time );
        radioNatural = findViewById( R.id.membrane_radio_natural );
        radioAmnioressi = findViewById( R.id.membrane_radio_amnioressi );
        radioClear = findViewById( R.id.membrane_radio_clear );
        radioDyedNS = findViewById( R.id.membrane_radio_dyednotsignificant);
        radioDyedS = findViewById( R.id.membrane_radio_dyedsignificant );
    }

    private void initViewData() {
        if (report != null && report.getMembraneRupture().isPresent()) {
            final MembraneRupture mr = report.getMembraneRupture().get();
            etDate.setText(UtilsView.emptyIfNull(mr.getDate()));
            etTime.setText(UtilsView.emptyIfNull(mr.getTime()));

            if (mr.getType() == MembraneRupture.Type.AMNIORESSI)
                radioAmnioressi.setChecked(true);
            else if (mr.getType() == MembraneRupture.Type.SPONTANEA)
                radioNatural.setChecked(true);

            if (mr.getFluidType() == MembraneRupture.FluidType.LIMPIDO)
                radioClear.setChecked(true);
            else if (mr.getFluidType() == MembraneRupture.FluidType.TINTO_S) {
                radioDyedS.setChecked(true);
            } else if (mr.getFluidType() == MembraneRupture.FluidType.TINTO_NS) {
                radioDyedNS.setChecked(true);
            }
        }
    }

    private MembraneRupture validateData() {
        final MembraneRupture mr = new MembraneRupture();
        if (UtilsView.isEmpty(etDate) || !DateUtil.validateDate(UtilsView.getValueAsString(etDate))) {
            showError("Data non valida");
            return null;
        }
        if (UtilsView.isEmpty(etTime) || !DateUtil.validateTime(UtilsView.getValueAsString(etTime))) {
            showError("Orario non valido");
            return null;
        }
        mr.setDate(etDate.getText().toString());
        mr.setTime(etTime.getText().toString());


        if (radioNatural.isChecked())
            mr.setType(MembraneRupture.Type.SPONTANEA);
        else if (radioAmnioressi.isChecked())
            mr.setType(MembraneRupture.Type.AMNIORESSI);
        else {
            showError("Tipo non selezionato");
            return null;
        }

        if (radioClear.isChecked())
            mr.setFluidType(MembraneRupture.FluidType.LIMPIDO);
        else if (radioDyedNS.isChecked()) {
            mr.setFluidType(MembraneRupture.FluidType.TINTO_NS);
        } else if (radioDyedS.isChecked()) {
            mr.setFluidType(MembraneRupture.FluidType.TINTO_S);
        } else {
            showError("Tipo del liquido non selezionato");
            return null;
        }
        return mr;
    }

    private void onSubmit() {
        final MembraneRupture mr = validateData();
        if (mr != null) {
            report.setMembraneRupture(mr);
            DataRepository.get().updateReport(report, mr, MembraneRupture.NAME);
            this.dismiss();
        }
    }
}
