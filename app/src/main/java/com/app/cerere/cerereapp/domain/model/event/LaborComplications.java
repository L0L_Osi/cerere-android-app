package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class LaborComplications extends AbstractEvent {

    public static final String NAME = "laborComplications";

    public enum Type {
        @SerializedName("none")
        NONE("Nessuna"),

        @SerializedName("dural puncture")
        DURAL_PUNCTURE("Puntura Durale"),

        @SerializedName("lateralized catheter")
        LATERALIZED_CATHETER("Catetere Lateralizzato"),

        @SerializedName("vessel cannulation")
        VESSEL_CANNULATION("Incannulamento Vaso"),

        @SerializedName("inefficient catheter")
        INEFFICIENT_CATHETER("Catetere Inefficace"),

        @SerializedName("repositioning")
        REPOSITIONING("Catetere Lateralizzato"),

        @SerializedName("other")
        OTHER("Altro");

        @Getter
        private final String printName;

        Type(final String printName) {
            this.printName = printName;
        }

        @Override
        public String toString() {
            return getPrintName();
        }
    }

    @Getter @Setter private List<Type> type;
    @Getter @Setter private String other;

    @Override
    public Status isComplete() {
        return null;
    }

}
