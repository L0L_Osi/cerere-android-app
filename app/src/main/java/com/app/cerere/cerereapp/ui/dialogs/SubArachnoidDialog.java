package com.app.cerere.cerereapp.ui.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.Group;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic.DrugDetails;
import com.app.cerere.cerereapp.domain.types.DrugTipology;
import com.app.cerere.cerereapp.domain.types.Drugs;
import com.app.cerere.cerereapp.domain.types.DrugsVia;
import com.app.cerere.cerereapp.ui.util.UtilsView;

import java.util.LinkedHashMap;


public class SubArachnoidDialog extends AbstractDialog {

    private final PartialDataListener<Object> listener;
    private RadioGroup radioGroupType;
    private RadioButton radioLocal;
    private RadioButton radioOpioid;
    private EditText etRopivacainaMg;
    private EditText etRopivacainaMl;
    private EditText etBupivacainaMg;
    private EditText etBupivacainaMl;
    private EditText etSufentanil;
    private Group groupLocal;
    private Group groupOpioid;
    private Button btnSubmit;
    private Button btnCancel;


    public SubArachnoidDialog(@NonNull final Context context, final PartialDataListener<Object> listener) {
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_subarachnoid, "Via subaracnoidea");
        initView();
        radioLocal.setOnClickListener(e -> {
            groupLocal.setVisibility(View.VISIBLE);
            groupOpioid.setVisibility(View.GONE);
        });
        radioOpioid.setOnClickListener(e -> {
            groupLocal.setVisibility(View.GONE);
            groupOpioid.setVisibility(View.VISIBLE);
        });
        btnCancel.setOnClickListener(e-> dismiss());
        btnSubmit.setOnClickListener(e-> onSubmit());
    }

    private void onSubmit() {
        final Anesthetic anesthetic = validateData();
        if (anesthetic == null)
            return;
        listener.setData(anesthetic);
        this.dismiss();
    }

    private Anesthetic validateData() {
        final Anesthetic a = new Anesthetic();
        boolean result = false;
        if (radioLocal.isChecked())
            result = local(a);
        else if (radioOpioid.isChecked())
            result = opioid(a);
        if (!result) {
            sendError();
            return null;
        }
        a.setVia(DrugsVia.SUBARACHNOID);
        return a;
    }

    private boolean local(final Anesthetic a) {
        a.setTipology(DrugTipology.LOCAL);
        LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
        if (!UtilsView.isEmpty(etRopivacainaMg) && !UtilsView.isEmpty(etRopivacainaMl) &&
                UtilsView.isEmpty(etBupivacainaMg) && UtilsView.isEmpty(etBupivacainaMl)) {
            a.setDrug(Drugs.ROPIVACAINA);
            details.put(DrugDetails.MG, UtilsView.getValueAsDouble(etRopivacainaMg));
            details.put(DrugDetails.ML, UtilsView.getValueAsDouble(etRopivacainaMl));
            a.setDetails(details);
            return true;
        } else if (UtilsView.isEmpty(etRopivacainaMg) && UtilsView.isEmpty(etRopivacainaMl) &&
                !UtilsView.isEmpty(etBupivacainaMg) && !UtilsView.isEmpty(etBupivacainaMl)) {
            a.setDrug(Drugs.BUPIVACAINA);
            details.put(DrugDetails.MG, UtilsView.getValueAsDouble(etBupivacainaMg));
            details.put(DrugDetails.ML, UtilsView.getValueAsDouble(etBupivacainaMl));
            a.setDetails(details);
            return true;
        } else {
            return false;
        }
    }

    private boolean opioid(final Anesthetic a) {
        a.setTipology(DrugTipology.OPIOID);
        if (UtilsView.isEmpty(etSufentanil))
            return false;
        a.setDrug(Drugs.SUFENTANIL);
        LinkedHashMap<DrugDetails, Double> details = new LinkedHashMap<>();
        details.put(DrugDetails.MGC, UtilsView.getValueAsDouble(etSufentanil));
        a.setDetails(details);
        return true;
    }

    private void initView() {
        btnSubmit = findViewById(R.id.btn_submit);
        btnCancel = findViewById(R.id.btn_cancel);
        radioGroupType = findViewById( R.id.sub_radiogroup_type);
        radioLocal = findViewById( R.id.sub_radio_local );
        radioOpioid = findViewById( R.id.sub_radio_opioid );
        etRopivacainaMg = findViewById( R.id.sub_et_ropivacainamg);
        etRopivacainaMl = findViewById( R.id.sub_et_ropivacainaml);
        etBupivacainaMg = findViewById( R.id.sub_et_bupivacainamg);
        etBupivacainaMl = findViewById( R.id.sub_et_bupivacainaml);
        etSufentanil = findViewById( R.id.sub_et_sufentanil );
        groupLocal = findViewById( R.id.sub_group_local);
        groupOpioid = findViewById( R.id.sub_group_opioid);

        groupLocal.setVisibility(View.GONE);
        groupOpioid.setVisibility(View.GONE);
    }

    private void sendError() {
        if (radioLocal.isChecked() &&
                !UtilsView.isEmpty(etRopivacainaMg) && !UtilsView.isEmpty(etRopivacainaMl) &&
                !UtilsView.isEmpty(etBupivacainaMg) && !UtilsView.isEmpty(etBupivacainaMl)) {
            showError("Inserire dati per solo un anestetico!");
        } else {
            showDefaultError();
        }
    }
}
