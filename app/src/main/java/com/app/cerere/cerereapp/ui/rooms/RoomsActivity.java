package com.app.cerere.cerereapp.ui.rooms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.app.cerere.cerereapp.App;
import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.android.DataRepository;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.types.Room;
import com.app.cerere.cerereapp.ui.LogoutActivity;
import com.app.cerere.cerereapp.ui.dialogs.PartialDataListener;
import com.app.cerere.cerereapp.ui.main.ReportMainActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class RoomsActivity extends LogoutActivity {

    private PartialDataListener<Object> listener;

    public static void startActivity(final Context context) {
        context.startActivity(new Intent(context, RoomsActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_rooms);

        final Button btnRoom1 = findViewById(R.id.btn_room1);
        final Button btnRoom2 = findViewById(R.id.btn_room2);
        final Button btnRoom3 = findViewById(R.id.btn_room3);
        final Button btnRoom4 = findViewById(R.id.btn_room4);

        btnRoom1.setOnClickListener(e -> onRoomPicked(Room.ONE));
        btnRoom2.setOnClickListener(e -> onRoomPicked(Room.TWO));
        btnRoom3.setOnClickListener(e -> onRoomPicked(Room.THREE));
        btnRoom4.setOnClickListener(e -> onRoomPicked(Room.FOUR));
    }

    private void onRoomPicked(final Room room) {
        final Report report = DataRepository.get().getLocalReportByRoom(room);
        if (report == null || report.getStatus() == Report.Status.COMPLETED) {
            new InitRoomDialog(this, room, this).show();
        } else {
            showSelectedReport(room);
        }
    }


    private void showSelectedReport(final Room room) {
        ReportMainActivity.startActivity(this, room);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(App.get().getApplicationContext(), "Codice non trovato!", Toast.LENGTH_LONG).show();
            } else {
                Log.d("SCAN BAR CODE RESULT", result.getContents());
                listener.setData(result.getContents());
            }
        }
    }

    public void setListener(PartialDataListener<Object> listener) {
        this.listener = listener;
    }

}
