package com.app.cerere.cerereapp.domain.types;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

public enum DrugTipology {
    @SerializedName ("local")
    LOCAL("Anestetico locale"),

    @SerializedName("opioid")
    OPIOID("Oppioide");

    @Getter
    private final String printName;

    DrugTipology(final String printName) {
        this.printName = printName;
    }
}
