package com.app.cerere.cerereapp.domain.model;

import com.app.cerere.cerereapp.domain.model.event.Baby;
import com.app.cerere.cerereapp.domain.model.event.Birth;
import com.app.cerere.cerereapp.domain.model.event.BloodLoss;
import com.app.cerere.cerereapp.domain.model.event.InfoReport;
import com.app.cerere.cerereapp.domain.model.event.Labor;
import com.app.cerere.cerereapp.domain.model.event.LaborComplications;
import com.app.cerere.cerereapp.domain.model.event.MembraneRupture;
import com.app.cerere.cerereapp.domain.model.event.OperatingRoom;
import com.app.cerere.cerereapp.domain.model.event.Patient;
import com.app.cerere.cerereapp.domain.model.event.Pph;
import com.app.cerere.cerereapp.domain.model.event.Recognition;
import com.app.cerere.cerereapp.domain.model.event.Secondamento;
import com.app.cerere.cerereapp.domain.model.event.ThirdStage;
import com.app.cerere.cerereapp.domain.types.Room;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Report implements Serializable {

    public enum Status implements Serializable{

        ACTIVE("active"),
        COMPLETED("completed");

        @Getter private String name;

        Status(final String name) {
            this.name = name;
        }
    }

    @SerializedName("_id")
    @Getter @Setter private String id;
    @Getter @Setter private String date;
    @Getter @Setter private String time;
    @Getter @Setter private Status status;
    @Getter @Setter private String operator;
    @Getter @Setter private Room room;
    @Getter @Setter private Integer version;
    @Getter @Setter private String sdo;


    @Setter private Patient patient;
    @Setter private InfoReport info;
    @Setter private List<Baby> babies;
    @Setter private List<Birth> births;
    @Setter private Secondamento secondamento;
    @Setter private BloodLoss bloodLoss;
    @Setter private Pph pph;
    @Setter private OperatingRoom operatingRoom;
    @Setter private Labor labor;
    @Setter private LaborComplications laborComplications;
    @Setter private MembraneRupture membraneRupture;
    @Setter private ThirdStage thirdStage;
    @Getter @Setter private List<Recognition> recognitions = new ArrayList<>();

    public Report() {
        this.version = 0;
        this.status = Status.ACTIVE;
    }

    public void increaseVersion() { this.version++; }

    public Optional<Patient> getPatient() {
        return Optional.ofNullable(patient);
    }

    public Optional<InfoReport> getInfo() {
        return Optional.ofNullable(info);
    }

    public Optional<List<Baby>> getBabies() { return Optional.ofNullable(babies); }

    public Optional<List<Birth>> getBirths() { return Optional.ofNullable(births); }

    public Optional<BloodLoss> getBloodLoss() { return Optional.ofNullable(bloodLoss); }

    public Optional<Pph> getPph() { return Optional.ofNullable(pph); }

    public Optional<OperatingRoom> getOperatingRoom() { return Optional.ofNullable(operatingRoom); }

    public Optional<Labor> getLabor() { return Optional.ofNullable(labor); }

    public Optional<LaborComplications> getLaborComplications() { return Optional.ofNullable(laborComplications); }

    public Optional<MembraneRupture> getMembraneRupture() { return Optional.ofNullable(membraneRupture); }

    public Optional<Secondamento> getSecondamento() { return Optional.ofNullable(secondamento); }

    public Optional<ThirdStage> getThirdStage() { return Optional.ofNullable(thirdStage); }

    public boolean equals(Report report) {
        String sdo = report.getSdo();

        return (this.getId() != null && this.getId().equals(report.getId())) ||
                (this.getSdo() != null && this.getSdo().equals(sdo));
    }
}
