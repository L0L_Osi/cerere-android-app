package com.app.cerere.cerereapp.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.Button;

import com.app.cerere.cerereapp.R;
import com.app.cerere.cerereapp.domain.model.Report;
import com.app.cerere.cerereapp.domain.model.event.Anesthetic;
import com.app.cerere.cerereapp.ui.component.MyTable;

import java.util.List;

public class TableInputDialog extends AbstractDialog {


    private Button btnCancel;
    private Button btnVO;
    private Button btnA;

    private Report report;
    private PartialDataListener<Dialog> listener;

    public TableInputDialog(@NonNull final Context context, final Report report, PartialDataListener<Dialog> listener) {
        super(context);
        this.report = report;
        this.listener = listener;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.dialog_table_input, "Inserimento rilevazione");

        initView();
        btnVO.setOnClickListener(e -> {
            dismiss();
            listener.setData(new TableInputVODialog(getContext(), this.report));
        });
        btnA.setOnClickListener(e -> {
            dismiss();
            listener.setData(new TableInputADialog(getContext(), this.report));
        });
        btnCancel.setOnClickListener(e -> dismiss());
    }

    private void initView() {
        btnCancel = findViewById(R.id.btn_table_cancel);
        btnVO = findViewById(R.id.btn_table_vo);
        btnA = findViewById(R.id.btn_table_a);
    }
}
