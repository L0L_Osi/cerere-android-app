package com.app.cerere.cerereapp.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.cerere.cerereapp.R;

import java.util.List;

public abstract class AbstractDialog extends Dialog {

    private TextView tvError;
    private TextView tvTitle;
    private View view;

    public AbstractDialog(@NonNull final Context context) {
        super(context);
        this.setCancelable(false);
    }

    /**
     * @param savedInstanceState
     * @param layout the layout resource to display
     * @param title the string title of the dialog
     */
    protected void onCreate(Bundle savedInstanceState, @LayoutRes int layout, String title) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.view = getLayoutInflater().inflate(layout, null);
//        setContentView(layout);
        setContentView(view);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        super.onCreate(savedInstanceState);

        tvError = findViewById(R.id.tv_error);
        tvTitle = findViewById(R.id.tv_title);
        if (tvTitle != null)
            tvTitle.setText(title);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    protected void initSpinner(final Spinner spinner, final int resources) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                resources, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    protected<X extends Enum> void initSpinner(final Spinner spinner, final List<X> values) {
        spinner.setAdapter(new ArrayAdapter<X>(getContext(),
                R.layout.support_simple_spinner_dropdown_item, values));
    }

    protected void showError(final String msg) {
        if (tvError != null) {
            tvError.setText(msg);
            tvError.setVisibility(View.VISIBLE);
        }
    }

    protected void showDefaultError() {
        if (tvError != null) {
            tvError.setText("Errore nei dati!");
            tvError.setVisibility(View.VISIBLE);
        }
    }

    protected View getView() {
        return this.view;
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this.getContext(), "Temporaneamente disabilitato", Toast.LENGTH_SHORT).show();
    }
}
