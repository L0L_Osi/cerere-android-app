package com.app.cerere.cerereapp.domain.model.event;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Labor extends AbstractEvent {

    public static final String NAME = "labor";

    public enum Type {
        @SerializedName("spontaneo")
        SPONTANEO("Spontaneo"),

        @SerializedName("indotto")
        INDOTTO("Indotto");

        @Getter private final String printName;

        Type(final String printName) {
            this.printName = printName;
        }

        @Override
        public String toString() {
            return getPrintName();
        }
    }

    public enum IndottoType {
        @SerializedName("crb")
        CRB("CRB"),

        @SerializedName("propess")
        PROPESS("Propess"),

        @SerializedName("cytotec")
        CYTOTEC("Cytotec"),

        @SerializedName("ossitocina")
        OSSITOCINA("Ossitocina");

        @Getter private final String printName;

        IndottoType(final String printName) {
            this.printName = printName;
        }

        @Override
        public String toString() {
            return getPrintName();
        }
    }

    @Getter @Setter private Type type;
    @Getter @Setter private List<IndottoType> indottoType;
    @Getter @Setter private boolean tolac;

    @Override
    public Status isComplete() {
        return null;
    }

    public String getIndottoTypeToString() {
        String s = "";
        for(IndottoType type: indottoType) {
            s = s.concat(type.getPrintName() + ", ");
        }
        if(s.length() > 0) {
            return s.substring(0, s.length() - 2);
        } else return s;
    }

}
