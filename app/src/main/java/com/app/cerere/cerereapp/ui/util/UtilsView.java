package com.app.cerere.cerereapp.ui.util;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.app.cerere.cerereapp.domain.model.event.Event;

import java.util.Arrays;
import java.util.List;

public class UtilsView {

    public static final String VALUE_PRESENT = "Presente";
    public static final String VALUE_ABSENT = "Assente";
    public static final String VALUE_POSITIVE = "Sì";
    public static final String VALUE_NEGATIVE= "No";
    public static final String EMPTY_VALUE = "";

    public static boolean hasOneEmpty(final EditText ... ets) {
        for (final EditText et : ets) {
            if (et.getText().length() == 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasOneTrue(final CheckBox ... cbs) {
        for (final CheckBox cb : cbs) {
            if (cb.isChecked()) {
                return true;
            }
        }
        return false;
    }

    public static int getColor(final Event.Status status) {
        switch (status) {
            case EMPTY: return Color.BLACK;
            case PARTIAL: return Color.YELLOW;
            case COMPLETE: return Color.GREEN;
            default: return Color.BLACK;
        }
    }

    public static String boolYesOrNo(final boolean value) {
        return value ? VALUE_POSITIVE : VALUE_NEGATIVE;
    }

    public static String boolIsPresent(final boolean isPresent) {
        return isPresent ? VALUE_PRESENT : VALUE_ABSENT;
    }

    public static String capitalizeFirst(final String input) {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public static void setStrikeTextFlags(final TextView... tvs) {
        for (final TextView tv : tvs) {
            tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    public static void setNotStrikeTextFlags(final TextView... tvs) {
        for (final TextView tv : tvs) {
            tv.setPaintFlags(tv.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }

    public static String printStringList(final List<String> list) {
        if (list == null || list.isEmpty())
            return EMPTY_VALUE;
        String s = EMPTY_VALUE;
        int i = 0;
        for (; i < list.size()-1; i++) {
            s += list.get(i) + ", ";
        }
        s += list.get(i);
        return s;
    }

    public static void colorColumn(final List<TableRow> rows, final int columnNum, final int color) {
        rows.forEach(r -> r.getChildAt(columnNum).setBackgroundColor(color));
    }

    public static void alternateColorColumn(final List<TableRow> rows, final int columnNum) {
        for (int i = 0; i < rows.size(); i++) {
            rows.get(i).getChildAt(columnNum).setBackgroundColor(i % 2 == 0 ? Color.WHITE : Color.LTGRAY);
        }
    }

    public static String emptyIfNull(final String tmp) {
        return tmp != null ? tmp : EMPTY_VALUE;
    }

    public static String emptyIfNull(final Integer value) {
        return value != null ? String.valueOf(value) : EMPTY_VALUE;
    }

    public static String emptyIfNull(final Double value) {
        return value != null ? String.valueOf(value) : EMPTY_VALUE;
    }

    /**
     * @param et the EditText with the requested value
     * @return the parsed Integer or null if the text is not valid
     */
    public static Integer getValueAsInteger(final EditText et) {
        if (et == null) return null;
        if (et.getText().length() == 0) return null;
        try {
            return Integer.parseInt(et.getText().toString().trim());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static String getValueAsString(final EditText et) {
        if (et == null) return null;
        if (et.getText().length() == 0) return null;
        return et.getText().toString();
    }

    /**
     * @param et the editable text
     * @return the double value. Be sure the text is not empty
     */
    public static Double getValueAsDouble(final EditText et) {
        if (et == null) return null;
        if (et.getText().length() == 0) return null;
        try {
            return Double.parseDouble(et.getText().toString().trim());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static List<String> getStringListFromRes(final Context ctx, final int resourcesId) {
        return Arrays.asList(ctx.getResources().getStringArray(resourcesId));
    }

    public static String getStringFromRes(final Context ctx, final int resourcesId) {
        return ctx.getResources().getString(resourcesId);
    }

    public static boolean isEmpty(final EditText et) {
        return et.getText().length() == 0;
    }
}

